﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace EWO
{
    public partial class FormFoldery : Form
    {
        bool Edycja;

        public FormFoldery(bool edycja)
        {
            InitializeComponent();
            Edycja = edycja;
        }

        private void FormFoldery_Load(object sender, EventArgs e)
        {
            this.Icon = EWO.Ikonka;

            if (Edycja == true)
            {
                TextBoxFolderPDF.Text = EWO.ZalacznikiOdczyt("PDF").Katalog.ToString();
                TextBoxFolderXLSX.Text = EWO.ZalacznikiOdczyt("XLSX").Katalog.ToString();
            }
            else
            {
                TextBoxFolderPDF.Text = EWO.tmpFolderPDF;
                TextBoxFolderXLSX.Text = EWO.tmpFolderXLSX;
            }

            ToolTipFolderPDF.SetToolTip(TextBoxFolderPDF, TextBoxFolderPDF.Text);
            ToolTipFolderXLSX.SetToolTip(TextBoxFolderXLSX, TextBoxFolderXLSX.Text);

        }

        private void ButtonFolderPDF_Click(object sender, EventArgs e)
        {
            DialogResult DlgResult = FolderBrowserDialog.ShowDialog();

            if (DlgResult == DialogResult.OK)
            {
                TextBoxFolderPDF.Text = FolderBrowserDialog.SelectedPath;
                ToolTipFolderPDF.SetToolTip(TextBoxFolderPDF, TextBoxFolderPDF.Text);
            }

        }

        private void ButtonZapisz_Click(object sender, EventArgs e)
        {
            EWO.tmpFolderPDF = TextBoxFolderPDF.Text;
            EWO.tmpFolderXLSX = TextBoxFolderXLSX.Text;
            
            if (Edycja == true)
            {
                EWO.KatalogZalacznikaZmien("PDF", EWO.tmpFolderPDF);
                EWO.KatalogZalacznikaZmien("XLSX", EWO.tmpFolderXLSX);
            }

            MessageBox.Show("Dane zapisane...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void ButtonFolderXLSX_Click(object sender, EventArgs e)
        {
            DialogResult DlgResult = FolderBrowserDialog.ShowDialog();

            if (DlgResult == DialogResult.OK)
            {
                TextBoxFolderXLSX.Text = FolderBrowserDialog.SelectedPath;
                ToolTipFolderXLSX.SetToolTip(TextBoxFolderXLSX, TextBoxFolderXLSX.Text);
            }

        }

    }
}
