﻿namespace EWO
{
    partial class FormStatystyki
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataGridStatystyki = new System.Windows.Forms.DataGridView();
            this.ComboBoxListaLat = new System.Windows.Forms.ComboBox();
            this.RadioButtonW = new System.Windows.Forms.RadioButton();
            this.RadioButtonP = new System.Windows.Forms.RadioButton();
            this.GroupBoxOperacje = new System.Windows.Forms.GroupBox();
            this.ComboBoxListaFunkcji = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStatystyki)).BeginInit();
            this.GroupBoxOperacje.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataGridStatystyki
            // 
            this.DataGridStatystyki.AllowUserToAddRows = false;
            this.DataGridStatystyki.AllowUserToDeleteRows = false;
            this.DataGridStatystyki.AllowUserToResizeColumns = false;
            this.DataGridStatystyki.AllowUserToResizeRows = false;
            this.DataGridStatystyki.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridStatystyki.Location = new System.Drawing.Point(12, 59);
            this.DataGridStatystyki.Name = "DataGridStatystyki";
            this.DataGridStatystyki.ReadOnly = true;
            this.DataGridStatystyki.RowHeadersVisible = false;
            this.DataGridStatystyki.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DataGridStatystyki.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridStatystyki.Size = new System.Drawing.Size(1150, 281);
            this.DataGridStatystyki.TabIndex = 0;
            // 
            // ComboBoxListaLat
            // 
            this.ComboBoxListaLat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxListaLat.FormattingEnabled = true;
            this.ComboBoxListaLat.Location = new System.Drawing.Point(1041, 23);
            this.ComboBoxListaLat.Name = "ComboBoxListaLat";
            this.ComboBoxListaLat.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxListaLat.TabIndex = 1;
            this.ComboBoxListaLat.SelectedIndexChanged += new System.EventHandler(this.OdswiezDane);
            // 
            // RadioButtonW
            // 
            this.RadioButtonW.AutoSize = true;
            this.RadioButtonW.Location = new System.Drawing.Point(17, 19);
            this.RadioButtonW.Name = "RadioButtonW";
            this.RadioButtonW.Size = new System.Drawing.Size(64, 17);
            this.RadioButtonW.TabIndex = 2;
            this.RadioButtonW.TabStop = true;
            this.RadioButtonW.Text = "Wydatki";
            this.RadioButtonW.UseVisualStyleBackColor = true;
            this.RadioButtonW.CheckedChanged += new System.EventHandler(this.OdswiezDane);
            // 
            // RadioButtonP
            // 
            this.RadioButtonP.AutoSize = true;
            this.RadioButtonP.Location = new System.Drawing.Point(98, 19);
            this.RadioButtonP.Name = "RadioButtonP";
            this.RadioButtonP.Size = new System.Drawing.Size(74, 17);
            this.RadioButtonP.TabIndex = 3;
            this.RadioButtonP.TabStop = true;
            this.RadioButtonP.Text = "Przychody";
            this.RadioButtonP.UseVisualStyleBackColor = true;
            this.RadioButtonP.CheckedChanged += new System.EventHandler(this.OdswiezDane);
            // 
            // GroupBoxOperacje
            // 
            this.GroupBoxOperacje.Controls.Add(this.RadioButtonP);
            this.GroupBoxOperacje.Controls.Add(this.RadioButtonW);
            this.GroupBoxOperacje.Location = new System.Drawing.Point(12, 5);
            this.GroupBoxOperacje.Name = "GroupBoxOperacje";
            this.GroupBoxOperacje.Size = new System.Drawing.Size(178, 48);
            this.GroupBoxOperacje.TabIndex = 4;
            this.GroupBoxOperacje.TabStop = false;
            this.GroupBoxOperacje.Text = "Operacje";
            // 
            // ComboBoxListaFunkcji
            // 
            this.ComboBoxListaFunkcji.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxListaFunkcji.FormattingEnabled = true;
            this.ComboBoxListaFunkcji.Location = new System.Drawing.Point(889, 23);
            this.ComboBoxListaFunkcji.Name = "ComboBoxListaFunkcji";
            this.ComboBoxListaFunkcji.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxListaFunkcji.TabIndex = 5;
            this.ComboBoxListaFunkcji.SelectedIndexChanged += new System.EventHandler(this.OdswiezDane);
            // 
            // FormStatystyki
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1174, 352);
            this.Controls.Add(this.ComboBoxListaFunkcji);
            this.Controls.Add(this.ComboBoxListaLat);
            this.Controls.Add(this.DataGridStatystyki);
            this.Controls.Add(this.GroupBoxOperacje);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1180, 380);
            this.Name = "FormStatystyki";
            this.Text = "EWO";
            this.Load += new System.EventHandler(this.FormStatystyki_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStatystyki)).EndInit();
            this.GroupBoxOperacje.ResumeLayout(false);
            this.GroupBoxOperacje.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridStatystyki;
        private System.Windows.Forms.ComboBox ComboBoxListaLat;
        private System.Windows.Forms.RadioButton RadioButtonW;
        private System.Windows.Forms.RadioButton RadioButtonP;
        private System.Windows.Forms.GroupBox GroupBoxOperacje;
        private System.Windows.Forms.ComboBox ComboBoxListaFunkcji;
    }
}