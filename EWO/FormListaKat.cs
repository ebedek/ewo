﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EWO
{
    public partial class FormListaKat : Form
    {
        bool CzyWprowadzono = false;
        TypyDzialanT TypDzialania;

        public FormListaKat(int wybor, string operacja)
        {
            InitializeComponent();

            if (operacja == "wydatek")
            {
                TypDzialania = new TypyDzialanT("wydatek");
                EWO.TowaryWczytaj(TypDzialania);

                for (int kwi = 0; kwi < EWO.TowaryIlosc(); kwi++)
                {
                    ComboBoxWybWariant.Items.Add(EWO.TowarOdczyt(kwi).Towar);
                }
            }

            if (operacja == "przychód")
            {
                TypDzialania = new TypyDzialanT("przychód");
                EWO.TowaryWczytaj(TypDzialania);

                for (int rpi = 0; rpi < EWO.TowaryIlosc(); rpi++)
                {
                    ComboBoxWybWariant.Items.Add(EWO.TowarOdczyt(rpi).Towar);
                }
            }
        }

        private void FormListaKat_Load(object sender, EventArgs e)
        {
            this.Icon = EWO.Ikonka;
        }

        private void ButtonWybWariant_Click(object sender, EventArgs e)
        {
            
            if (ComboBoxWybWariant.Text.Length == 0)
                MessageBox.Show("Proszę wybrać pozycję z listy...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
            {
                EWO.ImportWyborWariantu = ComboBoxWybWariant.Text;
                CzyWprowadzono = true;
                MessageBox.Show("Wariant wybrany...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }

        private void FormListaKat_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (CzyWprowadzono == false)
                EWO.ImportWyborWariantu = "nie importuj";

        }

        }

}
