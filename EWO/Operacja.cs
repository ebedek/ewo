﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;

namespace EWO
{
    public class OperacjaT
    {
        
        public int Id { get; set; }
        public TypyDzialanT TypDzialania { get; set; }
        public TowaryT Towar { get; set;}
        public double Kwota { get; set; }
        public string Data { get; set; }
        
        public OperacjaT (int id, TypyDzialanT a,TowaryT b, double c, string d)
        {
            Id = id;
            TypDzialania = a;
            Towar = b;
            Kwota = c;
            Data = d;
        }

        public static string KwotaSprawdz(string kwota)
        {
            double formatkwota;
            formatkwota = double.Parse(kwota);
            return string.Format("{0:0.00}", formatkwota);
        }
    }
}
