﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWO
{
    public class TowaryT
    {
        public string Towar { get; set; }
        public string TypTowaru { get; set; }

        public TowaryT(string towar, string typtowaru)
        {
            Towar = towar;
            TypTowaru = typtowaru;
        }
    }
}
