﻿namespace EWO
{
    partial class FormFoldery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.TextBoxFolderPDF = new System.Windows.Forms.TextBox();
            this.ButtonFolderPDF = new System.Windows.Forms.Button();
            this.ButtonZapisz = new System.Windows.Forms.Button();
            this.TextBoxFolderXLSX = new System.Windows.Forms.TextBox();
            this.ButtonFolderXLSX = new System.Windows.Forms.Button();
            this.LabelPDF = new System.Windows.Forms.Label();
            this.LabelXLSX = new System.Windows.Forms.Label();
            this.ToolTipFolderPDF = new System.Windows.Forms.ToolTip(this.components);
            this.ToolTipFolderXLSX = new System.Windows.Forms.ToolTip(this.components);
            this.GroupBoxFoldery = new System.Windows.Forms.GroupBox();
            this.GroupBoxFoldery.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextBoxFolderPDF
            // 
            this.TextBoxFolderPDF.Location = new System.Drawing.Point(9, 47);
            this.TextBoxFolderPDF.Name = "TextBoxFolderPDF";
            this.TextBoxFolderPDF.ReadOnly = true;
            this.TextBoxFolderPDF.Size = new System.Drawing.Size(353, 20);
            this.TextBoxFolderPDF.TabIndex = 0;
            // 
            // ButtonFolderPDF
            // 
            this.ButtonFolderPDF.Location = new System.Drawing.Point(379, 44);
            this.ButtonFolderPDF.Name = "ButtonFolderPDF";
            this.ButtonFolderPDF.Size = new System.Drawing.Size(75, 23);
            this.ButtonFolderPDF.TabIndex = 1;
            this.ButtonFolderPDF.Text = "Wybierz";
            this.ButtonFolderPDF.UseVisualStyleBackColor = true;
            this.ButtonFolderPDF.Click += new System.EventHandler(this.ButtonFolderPDF_Click);
            // 
            // ButtonZapisz
            // 
            this.ButtonZapisz.Location = new System.Drawing.Point(12, 135);
            this.ButtonZapisz.Name = "ButtonZapisz";
            this.ButtonZapisz.Size = new System.Drawing.Size(75, 23);
            this.ButtonZapisz.TabIndex = 4;
            this.ButtonZapisz.Text = "Zapisz";
            this.ButtonZapisz.UseVisualStyleBackColor = true;
            this.ButtonZapisz.Click += new System.EventHandler(this.ButtonZapisz_Click);
            // 
            // TextBoxFolderXLSX
            // 
            this.TextBoxFolderXLSX.Location = new System.Drawing.Point(9, 86);
            this.TextBoxFolderXLSX.Name = "TextBoxFolderXLSX";
            this.TextBoxFolderXLSX.ReadOnly = true;
            this.TextBoxFolderXLSX.Size = new System.Drawing.Size(353, 20);
            this.TextBoxFolderXLSX.TabIndex = 2;
            // 
            // ButtonFolderXLSX
            // 
            this.ButtonFolderXLSX.Location = new System.Drawing.Point(379, 83);
            this.ButtonFolderXLSX.Name = "ButtonFolderXLSX";
            this.ButtonFolderXLSX.Size = new System.Drawing.Size(75, 23);
            this.ButtonFolderXLSX.TabIndex = 3;
            this.ButtonFolderXLSX.Text = "Wybierz";
            this.ButtonFolderXLSX.UseVisualStyleBackColor = true;
            this.ButtonFolderXLSX.Click += new System.EventHandler(this.ButtonFolderXLSX_Click);
            // 
            // LabelPDF
            // 
            this.LabelPDF.AutoSize = true;
            this.LabelPDF.Location = new System.Drawing.Point(9, 31);
            this.LabelPDF.Name = "LabelPDF";
            this.LabelPDF.Size = new System.Drawing.Size(63, 13);
            this.LabelPDF.TabIndex = 5;
            this.LabelPDF.Text = "Raport PDF";
            // 
            // LabelXLSX
            // 
            this.LabelXLSX.AutoSize = true;
            this.LabelXLSX.Location = new System.Drawing.Point(9, 70);
            this.LabelXLSX.Name = "LabelXLSX";
            this.LabelXLSX.Size = new System.Drawing.Size(73, 13);
            this.LabelXLSX.TabIndex = 6;
            this.LabelXLSX.Text = "Eksport XLSX";
            // 
            // GroupBoxFoldery
            // 
            this.GroupBoxFoldery.Controls.Add(this.ButtonFolderXLSX);
            this.GroupBoxFoldery.Controls.Add(this.ButtonFolderPDF);
            this.GroupBoxFoldery.Controls.Add(this.LabelPDF);
            this.GroupBoxFoldery.Controls.Add(this.LabelXLSX);
            this.GroupBoxFoldery.Controls.Add(this.TextBoxFolderXLSX);
            this.GroupBoxFoldery.Controls.Add(this.TextBoxFolderPDF);
            this.GroupBoxFoldery.Location = new System.Drawing.Point(12, 4);
            this.GroupBoxFoldery.Name = "GroupBoxFoldery";
            this.GroupBoxFoldery.Size = new System.Drawing.Size(469, 125);
            this.GroupBoxFoldery.TabIndex = 7;
            this.GroupBoxFoldery.TabStop = false;
            this.GroupBoxFoldery.Text = "Ustawienia folderów";
            // 
            // FormFoldery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 170);
            this.Controls.Add(this.ButtonZapisz);
            this.Controls.Add(this.GroupBoxFoldery);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormFoldery";
            this.Text = "EWO";
            this.Load += new System.EventHandler(this.FormFoldery_Load);
            this.GroupBoxFoldery.ResumeLayout(false);
            this.GroupBoxFoldery.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog FolderBrowserDialog;
        private System.Windows.Forms.TextBox TextBoxFolderPDF;
        private System.Windows.Forms.Button ButtonFolderPDF;
        private System.Windows.Forms.Button ButtonZapisz;
        private System.Windows.Forms.TextBox TextBoxFolderXLSX;
        private System.Windows.Forms.Button ButtonFolderXLSX;
        private System.Windows.Forms.Label LabelPDF;
        private System.Windows.Forms.Label LabelXLSX;
        private System.Windows.Forms.ToolTip ToolTipFolderPDF;
        private System.Windows.Forms.ToolTip ToolTipFolderXLSX;
        private System.Windows.Forms.GroupBox GroupBoxFoldery;

    }
}