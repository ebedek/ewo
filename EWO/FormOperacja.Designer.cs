﻿namespace EWO
{
    partial class FormOperacja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBoxOperacjaTyp = new System.Windows.Forms.TextBox();
            this.TextBoxOperacjaKwota = new System.Windows.Forms.TextBox();
            this.ComboBoxOperacjaTowar = new System.Windows.Forms.ComboBox();
            this.DatePickerOperacjaData = new System.Windows.Forms.DateTimePicker();
            this.ButtonOperacja = new System.Windows.Forms.Button();
            this.GroupBoxOperacja = new System.Windows.Forms.GroupBox();
            this.LabelOperacjaDzialanie = new System.Windows.Forms.Label();
            this.ComboBoxOperacjaDzialanie = new System.Windows.Forms.ComboBox();
            this.LabelOperacjaData = new System.Windows.Forms.Label();
            this.LabelOperacjaTowar = new System.Windows.Forms.Label();
            this.LabelOperacjaKwota = new System.Windows.Forms.Label();
            this.LabelOperacjaTyp = new System.Windows.Forms.Label();
            this.GroupBoxOperacja.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextBoxOperacjaTyp
            // 
            this.TextBoxOperacjaTyp.Enabled = false;
            this.TextBoxOperacjaTyp.Location = new System.Drawing.Point(105, 133);
            this.TextBoxOperacjaTyp.Name = "TextBoxOperacjaTyp";
            this.TextBoxOperacjaTyp.Size = new System.Drawing.Size(100, 20);
            this.TextBoxOperacjaTyp.TabIndex = 2;
            // 
            // TextBoxOperacjaKwota
            // 
            this.TextBoxOperacjaKwota.Location = new System.Drawing.Point(105, 61);
            this.TextBoxOperacjaKwota.Name = "TextBoxOperacjaKwota";
            this.TextBoxOperacjaKwota.Size = new System.Drawing.Size(100, 20);
            this.TextBoxOperacjaKwota.TabIndex = 1;
            this.TextBoxOperacjaKwota.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TextBoxOperacjaKwota.Leave += new System.EventHandler(this.TextBoxOperacjaKwota_Leave);
            // 
            // ComboBoxOperacjaTowar
            // 
            this.ComboBoxOperacjaTowar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxOperacjaTowar.FormattingEnabled = true;
            this.ComboBoxOperacjaTowar.Location = new System.Drawing.Point(105, 96);
            this.ComboBoxOperacjaTowar.Name = "ComboBoxOperacjaTowar";
            this.ComboBoxOperacjaTowar.Size = new System.Drawing.Size(100, 21);
            this.ComboBoxOperacjaTowar.TabIndex = 3;
            this.ComboBoxOperacjaTowar.SelectedIndexChanged += new System.EventHandler(this.ComboBoxOperacjaTowar_SelectedIndexChanged);
            // 
            // DatePickerOperacjaData
            // 
            this.DatePickerOperacjaData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DatePickerOperacjaData.Location = new System.Drawing.Point(105, 168);
            this.DatePickerOperacjaData.Name = "DatePickerOperacjaData";
            this.DatePickerOperacjaData.Size = new System.Drawing.Size(146, 20);
            this.DatePickerOperacjaData.TabIndex = 4;
            // 
            // ButtonOperacja
            // 
            this.ButtonOperacja.Location = new System.Drawing.Point(12, 237);
            this.ButtonOperacja.Name = "ButtonOperacja";
            this.ButtonOperacja.Size = new System.Drawing.Size(116, 23);
            this.ButtonOperacja.TabIndex = 5;
            this.ButtonOperacja.Text = "Zarejestruj operację";
            this.ButtonOperacja.UseVisualStyleBackColor = true;
            this.ButtonOperacja.Click += new System.EventHandler(this.ButtonOperacja_Click);
            // 
            // GroupBoxOperacja
            // 
            this.GroupBoxOperacja.Controls.Add(this.LabelOperacjaDzialanie);
            this.GroupBoxOperacja.Controls.Add(this.ComboBoxOperacjaDzialanie);
            this.GroupBoxOperacja.Controls.Add(this.LabelOperacjaData);
            this.GroupBoxOperacja.Controls.Add(this.LabelOperacjaTowar);
            this.GroupBoxOperacja.Controls.Add(this.LabelOperacjaKwota);
            this.GroupBoxOperacja.Controls.Add(this.LabelOperacjaTyp);
            this.GroupBoxOperacja.Controls.Add(this.TextBoxOperacjaTyp);
            this.GroupBoxOperacja.Controls.Add(this.TextBoxOperacjaKwota);
            this.GroupBoxOperacja.Controls.Add(this.DatePickerOperacjaData);
            this.GroupBoxOperacja.Controls.Add(this.ComboBoxOperacjaTowar);
            this.GroupBoxOperacja.Location = new System.Drawing.Point(12, 12);
            this.GroupBoxOperacja.Name = "GroupBoxOperacja";
            this.GroupBoxOperacja.Size = new System.Drawing.Size(285, 207);
            this.GroupBoxOperacja.TabIndex = 5;
            this.GroupBoxOperacja.TabStop = false;
            this.GroupBoxOperacja.Text = "Rejestrowanie operacji";
            // 
            // LabelOperacjaDzialanie
            // 
            this.LabelOperacjaDzialanie.AutoSize = true;
            this.LabelOperacjaDzialanie.Location = new System.Drawing.Point(18, 25);
            this.LabelOperacjaDzialanie.Name = "LabelOperacjaDzialanie";
            this.LabelOperacjaDzialanie.Size = new System.Drawing.Size(52, 13);
            this.LabelOperacjaDzialanie.TabIndex = 8;
            this.LabelOperacjaDzialanie.Text = "Działanie";
            // 
            // ComboBoxOperacjaDzialanie
            // 
            this.ComboBoxOperacjaDzialanie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxOperacjaDzialanie.FormattingEnabled = true;
            this.ComboBoxOperacjaDzialanie.Location = new System.Drawing.Point(105, 22);
            this.ComboBoxOperacjaDzialanie.Name = "ComboBoxOperacjaDzialanie";
            this.ComboBoxOperacjaDzialanie.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxOperacjaDzialanie.TabIndex = 0;
            this.ComboBoxOperacjaDzialanie.SelectedIndexChanged += new System.EventHandler(this.ComboBoxOperacjaDzialanie_SelectedIndexChanged);
            // 
            // LabelOperacjaData
            // 
            this.LabelOperacjaData.AutoSize = true;
            this.LabelOperacjaData.Location = new System.Drawing.Point(18, 174);
            this.LabelOperacjaData.Name = "LabelOperacjaData";
            this.LabelOperacjaData.Size = new System.Drawing.Size(30, 13);
            this.LabelOperacjaData.TabIndex = 7;
            this.LabelOperacjaData.Text = "Data";
            // 
            // LabelOperacjaTowar
            // 
            this.LabelOperacjaTowar.AutoSize = true;
            this.LabelOperacjaTowar.Location = new System.Drawing.Point(18, 99);
            this.LabelOperacjaTowar.Name = "LabelOperacjaTowar";
            this.LabelOperacjaTowar.Size = new System.Drawing.Size(37, 13);
            this.LabelOperacjaTowar.TabIndex = 6;
            this.LabelOperacjaTowar.Text = "Towar";
            // 
            // LabelOperacjaKwota
            // 
            this.LabelOperacjaKwota.AutoSize = true;
            this.LabelOperacjaKwota.Location = new System.Drawing.Point(18, 68);
            this.LabelOperacjaKwota.Name = "LabelOperacjaKwota";
            this.LabelOperacjaKwota.Size = new System.Drawing.Size(37, 13);
            this.LabelOperacjaKwota.TabIndex = 5;
            this.LabelOperacjaKwota.Text = "Kwota";
            // 
            // LabelOperacjaTyp
            // 
            this.LabelOperacjaTyp.AutoSize = true;
            this.LabelOperacjaTyp.Location = new System.Drawing.Point(18, 136);
            this.LabelOperacjaTyp.Name = "LabelOperacjaTyp";
            this.LabelOperacjaTyp.Size = new System.Drawing.Size(25, 13);
            this.LabelOperacjaTyp.TabIndex = 4;
            this.LabelOperacjaTyp.Text = "Typ";
            // 
            // FormOperacja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 278);
            this.Controls.Add(this.GroupBoxOperacja);
            this.Controls.Add(this.ButtonOperacja);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormOperacja";
            this.Text = "EWO";
            this.Load += new System.EventHandler(this.FormOperacja_Load);
            this.GroupBoxOperacja.ResumeLayout(false);
            this.GroupBoxOperacja.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxOperacjaTyp;
        private System.Windows.Forms.TextBox TextBoxOperacjaKwota;
        private System.Windows.Forms.ComboBox ComboBoxOperacjaTowar;
        private System.Windows.Forms.DateTimePicker DatePickerOperacjaData;
        private System.Windows.Forms.Button ButtonOperacja;
        private System.Windows.Forms.GroupBox GroupBoxOperacja;
        private System.Windows.Forms.Label LabelOperacjaData;
        private System.Windows.Forms.Label LabelOperacjaTowar;
        private System.Windows.Forms.Label LabelOperacjaKwota;
        private System.Windows.Forms.Label LabelOperacjaTyp;
        private System.Windows.Forms.Label LabelOperacjaDzialanie;
        private System.Windows.Forms.ComboBox ComboBoxOperacjaDzialanie;
    }
}