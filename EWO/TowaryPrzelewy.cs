﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWO
{
    public class TowaryPrzelewyT
    {
        public string OpisPrzelewu { get; set; }
        public string Towar { get; set; }

        public TowaryPrzelewyT(string opisprzelewu, string towar)
        {
            Towar = towar;
            OpisPrzelewu = opisprzelewu;
        }

    }
}
