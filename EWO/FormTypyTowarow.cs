﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Data.SqlClient;

namespace EWO
{
    public partial class FormTypyTowarow : Form
    {
        string NazwaPrzyciskuTypTowaruDodawanie = "Zarejestruj typ towaru";
        string NazwaPrzyciskuTowarDodawanie = "Zarejestruj towar";
        string TowarOpis = "towar";
        string TypTowaruOpis = "typ towaru";
       
        public FormTypyTowarow()
        {
            InitializeComponent();
        }

        private void FormTypyTowarow_Load(object sender, EventArgs e)
        {
         
            this.Icon = EWO.Ikonka;

            TreeViewTowary.HideSelection = false;

            for (int i = 0; i < EWO.TypyDzialanIlosc(); i++)
            {
                TreeViewTowary.Nodes.Add(EWO.DzialanieOdczyt(i));

                if (i==0) TreeViewTowary.SelectedNode = TreeViewTowary.Nodes[i];

                for (int j = 0; j < EWO.TypyTowarowIlosc(); j++)
                {
                    TypyDzialanT ustawtypdzialania = new TypyDzialanT(EWO.DzialanieOdczyt(i));
                    EWO.TypyTowarowWczytaj(ustawtypdzialania);

                    TypyTowarowT ustawtyptowaru = new TypyTowarowT(EWO.TypTowaruOdczyt(j).TypTowaru,ustawtypdzialania.TypDzialania);
                    EWO.TowaryWczytaj(ustawtyptowaru);

                    TreeViewTowary.Nodes[i].Nodes.Add(EWO.TypTowaruOdczyt(j).TypTowaru);
                    TreeViewTowary.Nodes[i].Nodes[j].ContextMenuStrip = ContextMenuStripTowaryLevel1;

                    for (int k=0; k< EWO.TowaryIlosc();k++)
                    {
                            TreeViewTowary.Nodes[i].Nodes[j].Nodes.Add(EWO.TowarOdczyt(k).Towar);
                            TreeViewTowary.Nodes[i].Nodes[j].Nodes[k].ContextMenuStrip = ContextMenuStripTowaryLevel2;
                            TreeViewTowary.ExpandAll();
                    }
                }

            }
        }

        private void TextBoxWyczyscDane()
        {
            TextBoxTypyTowarow.Text = "";

            if (TreeViewTowary.SelectedNode.Level == 0)
                ButtonTypTowaruDodaj.Text = NazwaPrzyciskuTypTowaruDodawanie;

            if (TreeViewTowary.SelectedNode.Level == 1)
                ButtonTypTowaruDodaj.Text = NazwaPrzyciskuTowarDodawanie;

        }


        private void ToolStripMenuItemTypTowaruUsun_Click(object sender, EventArgs e)
        {
            int TypTowaruWybor = TreeViewTowary.SelectedNode.Index;
            int TypDzialania = TreeViewTowary.SelectedNode.Parent.Index;

            if (TypTowaruWybor < 0 || TypDzialania < 0)
                MessageBox.Show("Proszę wybrać typ towaru...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
            {
                try
                {
                    EWO.TypTowaruUsun(TreeViewTowary.SelectedNode.Text, TreeViewTowary.SelectedNode.Parent.Text);
                    TreeViewTowary.SelectedNode.Remove();
                    TextBoxWyczyscDane();
                }

                catch (SqlException error)
                {
                    MessageBox.Show("Błąd podczas usuwania danych - typ towaru został wykorzystany w bazie.\nSzczegółowe informacje o błędzie: " + error.Message, "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }

        }

        private void TextBoxTypyTowarow_DoubleClick(object sender, EventArgs e)
        {
            TextBoxWyczyscDane();
        }

        private void TreeViewTowary_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            ButtonTypTowaruDodaj.Visible = true;
            TextBoxTypyTowarow.Visible = true;

            TreeViewTowary.SelectedNode = e.Node;

            if (TreeViewTowary.SelectedNode.Level == 0)
                ButtonTypTowaruDodaj.Text = NazwaPrzyciskuTypTowaruDodawanie;

            if (TreeViewTowary.SelectedNode.Level == 1)
                ButtonTypTowaruDodaj.Text = NazwaPrzyciskuTowarDodawanie;

            if (TreeViewTowary.SelectedNode.Level == 2)
            {
                ButtonTypTowaruDodaj.Visible = false;
                TextBoxTypyTowarow.Visible = false;
            }

            
        }

        private void NowaKategoriaDodaj(string kategoria)
        {
            int licz = 0;

            if (TreeViewTowary.SelectedNode.Nodes.Count > 0)
            {
                for (int i = 0; i < TreeViewTowary.SelectedNode.Nodes.Count; i++)
                {
                    if (TextBoxTypyTowarow.Text == TreeViewTowary.SelectedNode.Nodes[i].Text)
                        licz++;
                }
            }

            if (TextBoxTypyTowarow.TextLength > 0)
            {
                if (licz>0)
                {
                    throw new Exception(kategoria + " już istnieje...");
                }
                else
                {
                    try
                    {
                        if (TreeViewTowary.SelectedNode.Level == 0)
                        {
                            EWO.TypTowaruDodaj(TextBoxTypyTowarow.Text, TreeViewTowary.SelectedNode.Text);
                            TreeViewTowary.SelectedNode.Nodes.Add(TextBoxTypyTowarow.Text);
                            TreeViewTowary.SelectedNode.Nodes[TreeViewTowary.SelectedNode.Nodes.Count - 1].ContextMenuStrip = ContextMenuStripTowaryLevel1;
                        }

                        if (TreeViewTowary.SelectedNode.Level == 1)
                        {
                            EWO.TowarDodaj(TextBoxTypyTowarow.Text, TreeViewTowary.SelectedNode.Text);
                            TreeViewTowary.SelectedNode.Nodes.Add(TextBoxTypyTowarow.Text);
                            TreeViewTowary.SelectedNode.Nodes[TreeViewTowary.SelectedNode.Nodes.Count - 1].ContextMenuStrip = ContextMenuStripTowaryLevel2;
                            TreeViewTowary.TopNode.ExpandAll();
                        }
                        
                        
                    }

                    catch (SqlException sqlerror)
                    {
                        MessageBox.Show("Błąd podczas wstawiania danych - " + kategoria + " już istnieje w bazie.\nSzczegółowe informacje o błędzie: " + sqlerror.Message, "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }
            }
            else
                MessageBox.Show("Podaj nazwę " + kategoria + "...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void ToolStripMenuItemUsunTowar_Click(object sender, EventArgs e)
        {
            int TowarWybor = TreeViewTowary.SelectedNode.Index;
            int TypTowaruWybor = TreeViewTowary.SelectedNode.Parent.Index;

            if (TypTowaruWybor < 0 || TowarWybor < 0)
                MessageBox.Show("Nie wybrano typu towaru lub towaru...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
            {
                try
                {
                    EWO.TowarUsun(TreeViewTowary.SelectedNode.Text, TreeViewTowary.SelectedNode.Parent.Text);
                    TreeViewTowary.SelectedNode.Remove();
                    TextBoxWyczyscDane();
                }

                catch (SqlException error)
                {
                    MessageBox.Show("Błąd podczas usuwania danych - towar został wykorzystany w bazie.\nSzczegółowe informacje o błędzie: " + error.Message, "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
        }

        private void ButtonTypTowaruDodaj_Click(object sender, EventArgs e)
        {
            if (TreeViewTowary.SelectedNode.Level == 0)
            {
                try
                {
                    NowaKategoriaDodaj(TypTowaruOpis);
                    TextBoxWyczyscDane();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message, "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            if (TreeViewTowary.SelectedNode.Level == 1)
            {
                try
                {
                    NowaKategoriaDodaj(TowarOpis);
                    TextBoxWyczyscDane();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message, "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

    }
}