﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using System.Security.Cryptography;

namespace EWO
{

    public class EWO
    {
        public static string FolderProgramu { get; set; }
        public static string Uzytkownik { get; set; }
        static List<OperacjaT> Operacje = new List<OperacjaT>();
        public static bool OperacjaWprowadzono { get; set; }
        static List<string> ListaMiesiecy = new List<string>();
        public static string AktualnyRokMsc { get; set; }
        public static string WybranyRokMsc { get; set; }
        public static string MaksymalnyRokMsc { get; set; }
        public static double WykresSumaKwot=0;
        public static bool MinimizeToTray = false;
        public static EmailT wiadomosc = new EmailT();
        public static System.Drawing.Icon Ikonka;
        public static string PlikImportu;
        public static string ImportWyborWariantu;
        static List<TypyDzialanT> TypyDzialan = new List<TypyDzialanT>();
        static List<TypyTowarowT> TypyTowarow = new List<TypyTowarowT>();
        static List<TowaryPrzelewyT> TowaryPrzelewy = new List<TowaryPrzelewyT>();
        static List<TowaryT> Towary = new List<TowaryT>();
        static List<SerweryPocztoweT> SerweryPocztowe = new List<SerweryPocztoweT>();
        static List<ZalacznikiT> Zalaczniki = new List<ZalacznikiT>();
        static List<UzytkownicyT> Uzytkownicy = new List<UzytkownicyT>();
        public static string tmpFolderPDF { get; set; }
        static List<TypyTowarowT> TypyTowarowTmp = new List<TypyTowarowT>();
        public static string tmpFolderXLSX { get; set; }
        static List<UprawnieniaT> Uprawnienia = new List<UprawnieniaT>();

        public EWO ()
        {
            Ikonka = new System.Drawing.Icon(FolderProgramu+"ewo32.ico");
            
            UstawieniaWczytaj();

            DateTime DataDzisiaj = DateTime.Today;
            int DataPoczRok = DataDzisiaj.Year;
            int DataPoczMsc = DataDzisiaj.Month;

            if (DataPoczMsc < 10)
                AktualnyRokMsc = DataPoczRok + "-0" + DataPoczMsc;
            else
                AktualnyRokMsc = DataPoczRok + "-" + DataPoczMsc;

            WybranyRokMsc = AktualnyRokMsc;
            UzupelnijListeMiesiecy();
            
            OperacjaWprowadzono = false;
            PlikImportu="";

            TypyDzialanWczytaj();
            TypyTowarowWczytaj();
            TowaryWczytaj();
            OpisPrzelewowWczytaj();

            SerweryPocztoweWczytaj();
            ZalacznikiWczytaj();

        }

        public static void TypyDzialanWczytaj()
        {
            TypyDzialan.Clear();

            EWODataContext ewo = new EWODataContext();
            var DzialaniaQ = ewo.TypyDzialan.OrderByDescending(td=>td.TypDzialania).Select(td => td.TypDzialania);

            foreach (var typy in DzialaniaQ)
            {
                TypyDzialanT wczytajnowy = new TypyDzialanT(typy);
                TypyDzialan.Add(wczytajnowy);
            }
        }

        public static string DzialanieOdczyt(int dzialanie)
        {
            return TypyDzialan[dzialanie].TypDzialania;
        }

        public static TypyTowarowT TypTowaruOdczyt(int typtowaru)
        {
            return TypyTowarow[typtowaru];
        }

        public static TypyTowarowT TypTowaruTmpOdczyt(int typtowaru)
        {
            return TypyTowarowTmp[typtowaru];
        }

        public static int TypyTowarowTmpIlosc()
        {
            return TypyTowarowTmp.Count;
        }

        public static TowaryT TowarOdczyt(int towar)
        {
            return Towary[towar];
        }

        public static void TypyTowarowWczytaj(TypyDzialanT typdzialania)
        {
            TypyTowarow.Clear();

            EWODataContext ewo = new EWODataContext();

            var TypyTowarowQ = ewo.TypyTowarow.Where(tt => tt.TypDzialania == typdzialania.TypDzialania).Select(tt => new { tt.TypTowaru, tt.TypDzialania });

            foreach (var typy in TypyTowarowQ)
            {
                TypyTowarowT wczytajnowy = new TypyTowarowT(typy.TypTowaru,typy.TypDzialania);
                TypyTowarow.Add(wczytajnowy);
            }

        }

        public static void TypyTowarowWczytaj()
        {
            TypyTowarow.Clear();

            EWODataContext ewo = new EWODataContext();

            var TypyTowarowQ = ewo.TypyTowarow.Select(tt => new { tt.TypTowaru, tt.TypDzialania });

            foreach (var typy in TypyTowarowQ)
            {
                TypyTowarowT wczytajnowy = new TypyTowarowT(typy.TypTowaru, typy.TypDzialania);
                TypyTowarow.Add(wczytajnowy);
            }

        }

        public static void TypyTowarowPobierz(TypyDzialanT typdzialania)
        {
            TypyTowarowTmp.Clear();

            var TypyTowarowPobierzQ = TypyTowarow.Where(tt => tt.TypDzialania == typdzialania.TypDzialania).Select(tt => new { tt.TypTowaru, tt.TypDzialania});

            foreach (var typy in TypyTowarowPobierzQ)
            {
                TypyTowarowT wczytajnowy = new TypyTowarowT(typy.TypTowaru, typy.TypDzialania);
                TypyTowarowTmp.Add(wczytajnowy);
            }

        }

        public static void TowaryWczytaj(TypyTowarowT typtowaru)
        {
            Towary.Clear();

            EWODataContext ewo = new EWODataContext();

            var TowaryQ = ewo.Towary.Join(ewo.TypyTowarow, t => t.TypTowaru, tt => tt.TypTowaru, (t, tt) => new { a = t, b = tt }).Where(c => c.b.TypTowaru == typtowaru.TypTowaru).Select(w => new { w.a.Towar, w.a.TypTowaru });

            foreach (var typy in TowaryQ)
            {
                TowaryT wczytajnowy = new TowaryT(typy.Towar, typy.TypTowaru);
                Towary.Add(wczytajnowy);
            }

        }

        public static void TowaryWczytaj(TypyDzialanT typdzialania)
        {
            Towary.Clear();

            EWODataContext ewo = new EWODataContext();

            var TowaryQ = ewo.Towary.Join(ewo.TypyTowarow, t => t.TypTowaru, tt => tt.TypTowaru, (t, tt) => new { a = t, b = tt }).Where(c => c.b.TypDzialania == typdzialania.TypDzialania).Select(w => new { w.a.Towar, w.a.TypTowaru });

            foreach (var typy in TowaryQ)
            {
                TowaryT wczytajnowy = new TowaryT(typy.Towar, typy.TypTowaru);
                Towary.Add(wczytajnowy);
            }

        }

        public static void TowaryWczytaj()
        {
            Towary.Clear();

            EWODataContext ewo = new EWODataContext();

            var TowaryQ = ewo.Towary.Join(ewo.TypyTowarow, t => t.TypTowaru, tt => tt.TypTowaru, (t, tt) => new { a = t, b = tt }).Select(w => new { w.a.Towar, w.a.TypTowaru });

            foreach (var towar in TowaryQ)
            {
                TowaryT wczytajnowy = new TowaryT(towar.Towar, towar.TypTowaru);
                Towary.Add(wczytajnowy);
            }

        }

        public static void ZalacznikiWczytaj()
        {
            EWODataContext ewo = new EWODataContext();
            var ZalacznikiQ = ewo.Zalaczniki.Where(zal => zal.Uzytkownik == Uzytkownik).Select(zal => new { zal.TypZalacznika, zal.Katalog });

            foreach (var zal in ZalacznikiQ)
            {
                ZalacznikiT wczytajnowy = new ZalacznikiT(zal.TypZalacznika, zal.Katalog);
                Zalaczniki.Add(wczytajnowy);

            }
        }

        public static void OperacjeWczytaj(int rok, int msc)
        {
            Operacje.Clear();

            EWODataContext ewo = new EWODataContext();
            var DaneOperacjiQ = (dynamic) null;
            
            // rok=0, msc=0 wykorzystano przy eksporcie do Excela - wybieranie wszystkich danych
            if (rok == 0 && msc == 0)
            {
                DaneOperacjiQ = ewo.Dane.Where(d => d.Uzytkownik == Uzytkownik).Join(ewo.Towary, d => d.Towar, t => t.Towar, (d, t) => new { d, t }).Join(ewo.TypyTowarow, t=>t.t.TypTowaru, tt=>tt.TypTowaru, (t,tt) => new {t,tt}).Select(w => new { w.t.d.ID, w.t.d.Towar, w.t.d.Kwota, w.t.t.TypTowaru, w.t.d.Data, w.tt.TypDzialania});
            }

            else
            {
                int DataKonDzi = DateTime.DaysInMonth(rok, msc);

                DateTime DataPocz = new DateTime(rok, msc, 1);
                DateTime DataKon = new DateTime(rok, msc, DataKonDzi);

                DaneOperacjiQ = ewo.Dane.Where(d => d.Uzytkownik == Uzytkownik).Join(ewo.Towary, d => d.Towar, t => t.Towar, (d, t) => new { d, t }).Join(ewo.TypyTowarow, t => t.t.TypTowaru, tt => tt.TypTowaru, (t, tt) => new { t, tt }).Where(w => w.t.d.Data >= DataPocz && w.t.d.Data <= DataKon).Select(w => new { w.t.d.ID, w.t.d.Towar, w.t.d.Kwota, w.t.t.TypTowaru, w.t.d.Data, w.tt.TypDzialania });

            }

            foreach (var rekord in DaneOperacjiQ)
            {
                TypyDzialanT NoweDzialanie = new TypyDzialanT(rekord.TypDzialania);
                TowaryWczytaj(NoweDzialanie);
                TowaryT NowyTowar = Towary.Where(t => t.Towar == rekord.Towar).Single();
                OperacjaT NowaOperacja = new OperacjaT(rekord.ID, NoweDzialanie, NowyTowar, Convert.ToDouble(rekord.Kwota), rekord.Data.ToString());
                Operacje.Add(NowaOperacja);
            }

        }

        public static void UstawieniaWczytaj()
        {
            EWODataContext ewo = new EWODataContext();

            var UstawieniaQ = ewo.Uzytkownicy.Where(uz => uz.Uzytkownik == Uzytkownik).SingleOrDefault();

            
            wiadomosc.SmtpSrvHost = UstawieniaQ.SmtpHost;
            wiadomosc.SmtpSrvPort = ZnajdzPortSerwera(wiadomosc.SmtpSrvHost);

            wiadomosc.SmtpLogin = UstawieniaQ.SmtpLogin;

            wiadomosc.SmtpPassword = "";
            if (wiadomosc.SmtpPassword.Length > 0)
                wiadomosc.SmtpPassword = wiadomosc.DeszyfrujHaslo(UstawieniaQ.SmtpPassword);

            wiadomosc.MailFrom = UstawieniaQ.MailFrom;
            wiadomosc.MailTo = UstawieniaQ.MailTo;
            wiadomosc.MailSubject = UstawieniaQ.MailSubject;
            wiadomosc.MailBody = UstawieniaQ.MailBody;

            MinimizeToTray = UstawieniaQ.MinimizeToTray;

            var ZalacznikPDF = ewo.Zalaczniki.Where(zal => zal.Uzytkownik == Uzytkownik && zal.TypZalacznika == "PDF").SingleOrDefault();
            wiadomosc.AttachPDF = ZalacznikPDF.CzyWysylac;

            var ZalacznikXLSX = ewo.Zalaczniki.Where(zal => zal.Uzytkownik == Uzytkownik && zal.TypZalacznika == "XLSX").SingleOrDefault();
            wiadomosc.AttachXLSX = ZalacznikXLSX.CzyWysylac;

        }

        public static void UstawieniaZapisz()
        {
            EWODataContext ewo = new EWODataContext();

            var UstawieniaQ = ewo.Uzytkownicy.Where(uz => uz.Uzytkownik == Uzytkownik).SingleOrDefault();
            

                UstawieniaQ.SmtpHost = wiadomosc.SmtpSrvHost;
                UstawieniaQ.SmtpLogin = wiadomosc.SmtpLogin;
                UstawieniaQ.SmtpPassword = wiadomosc.SzyfrujHaslo(wiadomosc.SmtpPassword);
                UstawieniaQ.MailFrom = wiadomosc.MailFrom;
                UstawieniaQ.MailTo = wiadomosc.MailTo;
                UstawieniaQ.MailSubject = wiadomosc.MailSubject;
                UstawieniaQ.MailBody = wiadomosc.MailBody;

                var ZalacznikPDF = ewo.Zalaczniki.Where(zal => zal.Uzytkownik == Uzytkownik && zal.TypZalacznika == "PDF").SingleOrDefault();
                ZalacznikPDF.CzyWysylac = wiadomosc.AttachPDF;

                var ZalacznikXLSX = ewo.Zalaczniki.Where(zal => zal.Uzytkownik == Uzytkownik && zal.TypZalacznika == "XLSX").SingleOrDefault();
                ZalacznikXLSX.CzyWysylac = wiadomosc.AttachXLSX;

                UstawieniaQ.MinimizeToTray = MinimizeToTray;

            ewo.SubmitChanges();
        }

        public static OperacjaT OperacjaOdczyt(int id)
        {
            try
            {
                return Operacje[id];
            }

            catch
            {
                MessageBox.Show("Wywołanie spoza zakresu...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public static string MiesiacOdczyt(int id)
        {
            try
            {
                return ListaMiesiecy[id];
            }

            catch
            {
                MessageBox.Show("Wywołanie spoza zakresu...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }


        public static int TypyTowarowIlosc()
        {
            return TypyTowarow.Count();
        }

        public static int TowaryIlosc()
        {
            return Towary.Count();
        }

        public static int TypyDzialanIlosc()
        {
            return TypyDzialan.Count();
        }

        public static int TypyTowarowIlosc(TypyDzialanT typdzialania)
        {
            return TypyTowarow.Where(tt => tt.TypDzialania == typdzialania.TypDzialania).Count();
        }

        public static int OperacjeIlosc()
        {
            return Operacje.Count;
        }

        public static int OperacjaZnajdz(int szukaneid)
        {
            int znalezioneid=-1;

            for (int i = 0; i < OperacjeIlosc(); i++)
            {
                if (Operacje[i].Id == szukaneid)
                    znalezioneid = i;    
            }

            return znalezioneid;
        }

        public static int ListaMiesiecyIlosc()
        {
            return ListaMiesiecy.Count;
        }

        public static void ListaMiesiecySort()
        {
            ListaMiesiecy.Sort();
        }

        public static void TypTowaruDodaj(string typtowaru,string typdzialania)
        {
            EWODataContext ewo = new EWODataContext();
            ewo.TypyTowarow.InsertOnSubmit(new TypyTowarow { TypTowaru = typtowaru, TypDzialania = typdzialania });
            ewo.SubmitChanges();

            TypyTowarowT dodajnowy = new TypyTowarowT(typtowaru,typdzialania);
            TypyTowarow.Add(dodajnowy);
        }

        public static void TowarDodaj(string towar, string typtowaru)
        {
            EWODataContext ewo = new EWODataContext();
            ewo.Towary.InsertOnSubmit(new Towary { Towar = towar, TypTowaru = typtowaru });
            ewo.SubmitChanges();

            TowaryT dodajnowy = new TowaryT(towar,typtowaru);
            Towary.Add(dodajnowy);
        }

        public static void TypTowaruUsun(string typtowaru, string typdzialania)
        {
            EWODataContext ewo = new EWODataContext();
            var TypyTowarowQ = ewo.TypyTowarow.Single(tt => tt.TypTowaru == typtowaru && tt.TypDzialania == typdzialania);
            ewo.TypyTowarow.DeleteOnSubmit(TypyTowarowQ);
            ewo.SubmitChanges();
        }

        public static void TowarUsun(string towar, string typtowaru)
        {
            EWODataContext ewo = new EWODataContext();
            var TowarQ = ewo.Towary.Single(t => t.Towar == towar && t.TypTowaru == typtowaru);
            ewo.Towary.DeleteOnSubmit(TowarQ);
            ewo.SubmitChanges();
        }

        public static void OperacjaDodaj(string dzialanie, double kwota, string towar, string data)
        {
            EWODataContext ewo = new EWODataContext();
            ewo.Dane.InsertOnSubmit(new Dane { Towar = towar, Kwota = Convert.ToDecimal(kwota), Data = Convert.ToDateTime(data),Uzytkownik=Uzytkownik});
            ewo.SubmitChanges();

            TypyDzialanT NoweDzialanie = new TypyDzialanT(dzialanie);
            TowaryWczytaj(NoweDzialanie);

            TowaryT NowyTowar = Towary.Where(t => t.Towar == towar).Single();
            OperacjaT NowaOperacja = new OperacjaT(OperacjeIloscWszystkie(),NoweDzialanie, NowyTowar, kwota, data);
            Operacje.Add(NowaOperacja);

            OperacjaWprowadzono = true;
        }

        public static void OperacjaZmien(int id, string dzialanie, double kwota, string towar, string data, int idlist)
        {
            EWODataContext ewo = new EWODataContext();
            var DaneOperacjiDoZmiany = ewo.Dane.Where(d => d.ID == id).SingleOrDefault();
          
                DaneOperacjiDoZmiany.Towar = towar;
                DaneOperacjiDoZmiany.Kwota = Convert.ToDecimal(kwota);
                DaneOperacjiDoZmiany.Data = Convert.ToDateTime(data);

                ewo.SubmitChanges();

            if (data.Substring(0, 7) == Operacje[idlist].Data.Substring(0,7))
            {
                TypyDzialanT NoweDzialanie = new TypyDzialanT(dzialanie);
                TowaryT NowyTowar = Towary.Where(t => t.Towar == towar).Single();
                OperacjaT ZmienOperacje = new OperacjaT(id, NoweDzialanie, NowyTowar, kwota, data);
                Operacje[idlist] = ZmienOperacje;
            }

            OperacjaWprowadzono = true;
            
        }

        public static void OperacjaUsun(int id)
        {
            EWODataContext ewo = new EWODataContext();

            var DaneOperacjiDoUsuniecia = ewo.Dane.Where(d => d.ID == id).SingleOrDefault();

            ewo.Dane.DeleteOnSubmit(DaneOperacjiDoUsuniecia);
            ewo.SubmitChanges();

            Operacje.Remove(Operacje[OperacjaZnajdz(id)]);
            
        }

        public static void UzupelnijListeMiesiecy()
        {
            EWODataContext ewo = new EWODataContext();

            List<string> TymczasowaListaMiesiacy = new List<string>();

            var DaneDoListyMiesiacy = ewo.Dane.Where(d => d.Uzytkownik == Uzytkownik).OrderByDescending(msc => msc.Data).Select(msc => msc.Data);

            foreach (var rekord in DaneDoListyMiesiacy)
            {
                TymczasowaListaMiesiacy.Add(rekord.ToString().Substring(0,7));
            }

            ListaMiesiecy = TymczasowaListaMiesiacy.Distinct().ToList();
        }

        public static int OperacjeIloscWszystkie()
        {
            int ilosc=0;

            EWODataContext ewo = new EWODataContext();

            var DaneDoIlosciOperacji = ewo.Dane.Select(rek=>rek.ID);

            foreach (var rekord in DaneDoIlosciOperacji)
            {
                if (rekord>ilosc)
                    ilosc = rekord;
            }

            return ilosc;
        }

        public static void ListaMiesiecyDodaj(string rokmsc)
        {
            ListaMiesiecy.Add(rokmsc);
        }

        public static void ListaMiesiecyUsun(string rokmsc)
        {
            ListaMiesiecy.Remove(rokmsc);
        }

        public static string OperacjaSprawdzDate(int id)
        {
            EWODataContext ewo = new EWODataContext();

            var DaneDoSprawdzeniaDaty = ewo.Dane.Where(i => i.ID == id).Select(i => i.Data).SingleOrDefault();

            return DaneDoSprawdzeniaDaty.ToString().Substring(0,10);
        }

        public static void OperacjaUsunZListy(int idlist)
        {
            Operacje.Remove(Operacje[idlist]);
        }

        public static void Wykres1KategorieMiesiace(string rokmsc, string typtowaru)
        {
            EWODataContext ewo = new EWODataContext();

            int rok = int.Parse(rokmsc.Substring(0, 4));
            int msc = int.Parse(rokmsc.Substring(5, 2));
            
            int DataKonDzi = DateTime.DaysInMonth(rok, msc);

            DateTime DataPocz = new DateTime(rok, msc, 1);
            DateTime DataKon = new DateTime(rok, msc, DataKonDzi);
            
            decimal SumaW1 = ewo.Dane.Where(d => d.Uzytkownik == Uzytkownik && d.Data >= DataPocz && d.Data <= DataKon).Join(ewo.Towary, d => d.Towar, t => t.Towar, (d, t) => new { d, t }).Where(tt => tt.t.TypTowaru == typtowaru).Select(w => (decimal?)w.d.Kwota).Sum() ?? 0m;

            WykresSumaKwot = (double) SumaW1;
        }

        public static void Wykres2WszystkoRazem(string rokmsc, string typdzialania)
        {
            EWODataContext ewo = new EWODataContext();

            int rok = int.Parse(rokmsc.Substring(0, 4));
            int msc = int.Parse(rokmsc.Substring(5, 2));

            int DataKonDzi = DateTime.DaysInMonth(rok, msc);

            DateTime DataPocz = new DateTime(rok, msc, 1);
            DateTime DataKon = new DateTime(rok, msc, DataKonDzi);

            decimal SumaW2 = ewo.Dane.Where(d => d.Uzytkownik == Uzytkownik && d.Data >= DataPocz && d.Data <= DataKon).Join(ewo.Towary, d => d.Towar, t => t.Towar, (d, t) => new { d, t }).Where(tt => tt.t.TypyTowarow.TypDzialania == typdzialania).Select(w => (decimal?)w.d.Kwota).Sum() ?? 0m;

            WykresSumaKwot = (double) SumaW2;
        }

        public static void Wykres3KategorieRazem(string typtowaru)
        {
            EWODataContext ewo = new EWODataContext();

            decimal SumaW3 = ewo.Dane.Where(d => d.Uzytkownik == Uzytkownik).Join(ewo.Towary, d => d.Towar, t => t.Towar, (d, t) => new { d, t }).Where(t => t.t.TypTowaru == typtowaru).Select(w => (decimal?)w.d.Kwota).Sum() ?? 0m;

            WykresSumaKwot = (double)SumaW3;
        }

        public static void SerweryPocztoweWczytaj()
        {
            SerweryPocztowe.Clear();

            EWODataContext ewo = new EWODataContext();

            var ListaSerwerow = ewo.SerwerySMTP.Select(srv => new { srv.SmtpHost, srv.SmtpPort });

            foreach (var serwer in ListaSerwerow)
            {
                SerweryPocztoweT wczytajnowy = new SerweryPocztoweT (serwer.SmtpHost,serwer.SmtpPort);
                SerweryPocztowe.Add(wczytajnowy);
            }

        }

        public static int SerweryPocztoweIlosc()
        {
            return SerweryPocztowe.Count();
        }

        public static SerweryPocztoweT SerwerPocztowyOdczyt(int id)
        {
            return SerweryPocztowe[id];
        }

        public static int ZnajdzPortSerwera(string serwer)
        {
            int PortSerwera = SerweryPocztowe.Where(srv => srv.SmtpHost == serwer).Select(srv => srv.SmtpPort).SingleOrDefault();

            return PortSerwera;
        }

        public static string ZnajdzTypTowaru(string towar)
        {
            string TypTowaru = Towary.Where(t => t.Towar == towar).Select(t => t.TypTowaru).Single();

            return TypTowaru;
        }
        
        public static void SerwerPocztowyDodaj(string serwer, int port)
        {
            EWODataContext ewo = new EWODataContext();
            ewo.SerwerySMTP.InsertOnSubmit(new SerwerySMTP { SmtpHost = serwer, SmtpPort = Convert.ToInt16(port) });
            ewo.SubmitChanges();

            SerweryPocztoweT dodajnowy = new SerweryPocztoweT (serwer, port);
            SerweryPocztowe.Add(dodajnowy);
        }

        public static void SerwerPocztowyZmien(string serwer, int port)
        {
            EWODataContext ewo = new EWODataContext();
            var DaneerweraDoZmiany = ewo.SerwerySMTP.Where(srv => srv.SmtpHost == serwer).SingleOrDefault();

            DaneerweraDoZmiany.SmtpPort = Convert.ToInt16(port);
            ewo.SubmitChanges();

            SerweryPocztoweT AktualizujListeSerwerow = SerweryPocztowe.FirstOrDefault(srv => srv.SmtpHost == serwer);
            AktualizujListeSerwerow.SmtpPort = port;
        }

        public static void SerwerPocztowyUsun(string serwer, int port)
        {
            EWODataContext ewo = new EWODataContext();
            var SerweryPocztoweQ = ewo.SerwerySMTP.Single(srv => srv.SmtpHost == serwer && srv.SmtpPort == port);
            ewo.SerwerySMTP.DeleteOnSubmit(SerweryPocztoweQ);
            ewo.SubmitChanges();

            SerweryPocztoweT AktualizujListeSerwerow = SerweryPocztowe.FirstOrDefault(srv => srv.SmtpHost == serwer);
            SerweryPocztowe.Remove(AktualizujListeSerwerow);
        }

        public static void KatalogZalacznikaZmien(string typzalacznika, string folder)
        {
            try
            {
                EWODataContext ewo = new EWODataContext();
                var DaneZalacznikaDoZmiany = ewo.Zalaczniki.Where(zal => zal.TypZalacznika == typzalacznika && zal.Uzytkownik == Uzytkownik).SingleOrDefault();

                DaneZalacznikaDoZmiany.Katalog = folder;
                ewo.SubmitChanges();

                ZalacznikiT AktualizujListeZalacznikow = Zalaczniki.FirstOrDefault(zal => zal.TypZalacznika == typzalacznika);
                AktualizujListeZalacznikow.Katalog = folder;
            }

            catch (Exception err)
            {
                MessageBox.Show("Wystąpił błąd.\nSzczegółowe informacje: " + err.Message, "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public static ZalacznikiT ZalacznikiOdczyt(string typzalacznika)
        {
            return Zalaczniki.Where(zal => zal.TypZalacznika == typzalacznika).SingleOrDefault();
        }

        public static void HasloZmien(string haslo)
        {
            string suma=Zakoduj(haslo);

            try
            {
                EWODataContext ewo = new EWODataContext();

                var HasloDoZmiany = ewo.Uzytkownicy.Where(uz => uz.Uzytkownik == Uzytkownik).SingleOrDefault();

                HasloDoZmiany.Haslo = suma;

                ewo.SubmitChanges();

                MessageBox.Show("Dane zapisano.","EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            catch (Exception err)
            {
                MessageBox.Show("Wystąpił błąd.\nSzczegółowe informacje: " + err.Message, "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
        }

        public static string Zakoduj(string haslo)
        {
            StringBuilder sBuilder = new StringBuilder();

            MD5 md5suma = MD5.Create();

            byte[] dane = md5suma.ComputeHash(Encoding.UTF8.GetBytes(haslo));

            for (int i = 0; i < dane.Length; i++)
            {
                sBuilder.Append(dane[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        public static void UzytkownicyWczytaj()
        {
            Uzytkownicy.Clear();

            EWODataContext ewo = new EWODataContext();

            var UzytkownicyQ = ewo.Uzytkownicy.Where(uz => uz.Uzytkownik != null);

            foreach (var uzyt in UzytkownicyQ)
            {

                string ZalFolderPDF = ewo.Zalaczniki.Where(zal => zal.Uzytkownik == uzyt.Uzytkownik && zal.TypZalacznika == "PDF").Select(zal => zal.Katalog).Single();
                string ZalFolderXLSX = ewo.Zalaczniki.Where(zal => zal.Uzytkownik == uzyt.Uzytkownik && zal.TypZalacznika == "XLSX").Select(zal => zal.Katalog).Single();
                UzytkownicyT dodajnowy = new UzytkownicyT(uzyt.Uzytkownik, uzyt.Haslo, uzyt.SmtpHost, uzyt.SmtpLogin, uzyt.MailFrom, uzyt.MailTo, uzyt.MailSubject, uzyt.MailBody, uzyt.MinimizeToTray, ZalFolderPDF, ZalFolderXLSX, uzyt.PoziomUprawnien);

                Uzytkownicy.Add(dodajnowy);
            }

        }

        public static void UzytkownikDodaj(string uzytkownik, string haslo, string smtphost, string smtplogin, string mailfrom, string mailto, string mailsubject, string mailbody, bool minimizetotray, string folderpdf, string folderxlsx, Int16 poziomuprawnien)
        {

            EWODataContext ewo = new EWODataContext();
            ewo.Uzytkownicy.InsertOnSubmit(new Uzytkownicy { Uzytkownik = uzytkownik, Haslo = haslo, SmtpHost = smtphost, SmtpLogin = smtplogin, MailFrom = mailfrom, MailTo = mailto, MailSubject = mailsubject, MailBody = mailbody, MinimizeToTray = minimizetotray, PoziomUprawnien = poziomuprawnien });
            ewo.Zalaczniki.InsertOnSubmit(new Zalaczniki { Uzytkownik = uzytkownik, TypZalacznika = "PDF", Katalog = folderpdf });
            ewo.Zalaczniki.InsertOnSubmit(new Zalaczniki { Uzytkownik = uzytkownik, TypZalacznika = "XLSX", Katalog = folderxlsx });
            
            ewo.SubmitChanges();

            UzytkownicyT dodajnowy = new UzytkownicyT(uzytkownik, haslo, smtphost, smtplogin, mailfrom, mailto, mailsubject, mailbody, minimizetotray, folderpdf, folderxlsx, poziomuprawnien);

            Uzytkownicy.Add(dodajnowy);            

        }

        public static UzytkownicyT UzytkownikOdczyt(int id)
        {
            return Uzytkownicy[id];
        }

        public static int UzytkownicyIlosc()
        {
            return Uzytkownicy.Count();
        }

        public static void UprawnieniaWczytaj()
        {
            Uprawnienia.Clear();

            EWODataContext ewo = new EWODataContext();

            var UprawnieniaQ = ewo.Uprawnienia.Select(upr => new { upr.Poziom, upr.Opis });

            foreach (var uprawnienia in UprawnieniaQ)
            {
                UprawnieniaT wczytajnowy = new UprawnieniaT(uprawnienia.Poziom, uprawnienia.Opis);
                Uprawnienia.Add(wczytajnowy);
            }

        }

        public static int UprawnieniaIlosc()
        {
            return Uprawnienia.Count();
        }

        public static UprawnieniaT UprawnieniaOdczyt(int id)
        {
            return Uprawnienia[id];
        }

        public static Int16 ZnajdzPoziomUprawnien(string opis)
        {
            Int16 PoziomUprawnien = Uprawnienia.Where(upr => upr.Opis == opis).Select(upr => upr.Poziom).SingleOrDefault();

            return PoziomUprawnien;
        }

        public static string ZnajdzOpisUprawnien(Int16 poziom)
        {
            string OpisUprawnien = Uprawnienia.Where(upr => upr.Poziom == poziom).Select(upr => upr.Opis).SingleOrDefault();

            return OpisUprawnien;
        }

        public static Int16 SprawdzPoziomUprawnien()
        {
            EWODataContext ewo = new EWODataContext();

            Int16 PoziomUprawnien = ewo.Uzytkownicy.Where(uz => uz.Uzytkownik == Uzytkownik).Select(uz => uz.PoziomUprawnien).SingleOrDefault();

            return PoziomUprawnien;
        }

        public static void OpisPrzelewowWczytaj()
        {
            TowaryPrzelewy.Clear();

            EWODataContext ewo = new EWODataContext();

            var DanePrzelewow = ewo.TowaryPrzelewy.Select(pt => new { pt.OpisPrzelewu, pt.Towar });

            foreach (var przelew in DanePrzelewow)
            {
                TowaryPrzelewyT wczytajnowy = new TowaryPrzelewyT(przelew.OpisPrzelewu, przelew.Towar);
                TowaryPrzelewy.Add(wczytajnowy);
            }

        }

        public static void OpisPrzelewuDodaj(string opisprzelewu, string towar)
        {

            int OpisPrzelewuSpr = TowaryPrzelewy.Where(tp => tp.OpisPrzelewu == opisprzelewu).Count();

            if (OpisPrzelewuSpr == 0)
            {
                EWODataContext ewo = new EWODataContext();

                ewo.TowaryPrzelewy.InsertOnSubmit(new TowaryPrzelewy { OpisPrzelewu = opisprzelewu, Towar = towar });
                ewo.SubmitChanges();

                TowaryPrzelewyT dodajnowy = new TowaryPrzelewyT(opisprzelewu, towar);
                TowaryPrzelewy.Add(dodajnowy);
            }
        }

        public static string OpisPrzelewuSprawdz(string opisprzelewu)
        {
            int OpisPrzelewuSpr = TowaryPrzelewy.Where(tp => tp.OpisPrzelewu == opisprzelewu).Count();

            if (OpisPrzelewuSpr > 0)
                return TowaryPrzelewy.Where(tp => tp.OpisPrzelewu == opisprzelewu).Select(tp => tp.Towar).SingleOrDefault();
            else
                return "nie importuj";

        }
    }
}

