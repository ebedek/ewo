﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWO
{
    public class UprawnieniaT
    {
        public Int16 Poziom { get; set; }
        public string Opis { get; set; }

        public UprawnieniaT(Int16 poziom, string opis)
        {
            Poziom = poziom;
            Opis = opis;
        }
    }
}
