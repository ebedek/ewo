﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EWO
{
    public partial class FormWykres3 : Form
    {
        TypyDzialanT TypDzialania = new TypyDzialanT("wydatek");

        public FormWykres3()
        {
            InitializeComponent();
        }

        private void FormWykres3_Load(object sender, EventArgs e)
        {
            this.Icon = EWO.Ikonka;

            this.FormR3RadioButton1.Checked = true;
            this.FormR3RadioButton2.Checked = false;
        }

        private void Wykres3PobierzDane(string dzialanie)
        {
            EWODataContext ewo = new EWODataContext();

            var dane = ewo.funWykres3KategorieRazem(EWO.Uzytkownik, dzialanie);

            chart3.Series[0].Points.Clear();

            int i = 0;

            foreach (var d in dane)
            {
                chart3.Series[0].Points.Add((double)d.Kwota);
                chart3.Series[0].IsValueShownAsLabel = true;
                chart3.Series[0].Points[i].LegendText = d.TypTowaru;
                i++;
            }

            chart3.Titles[0].Text = "Wszystkie miesiące razem - podział na kategorie";

        }

        private void FormR3RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (this.FormR3RadioButton1.Checked == true)
            {
                Wykres3PobierzDane("wydatek");
            }

            if (this.FormR3RadioButton2.Checked == true)
            {
                Wykres3PobierzDane("przychód");
            }
        }

        private void FormWykres3_Resize(object sender, EventArgs e)
        {
            int wysokoscf = this.Size.Height;
            int szerokoscf = this.Size.Width;

            FormR3GroupBox.Location = new Point(szerokoscf - (FormR3GroupBox.Size.Width + 30), wysokoscf - (FormR3GroupBox.Size.Height + 50));
        }
    }
}
