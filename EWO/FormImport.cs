﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace EWO
{
    public partial class FormImport : Form
    {
        public FormImport()
        {
            InitializeComponent();
        }

        int kw;
        int i;
        string w;
        int tytulem;
        string id;
        int szczegoly;


        private void FormImport_Load(object sender, EventArgs e)
        {

            this.Icon = EWO.Ikonka;

            int IloscOperacjiPrzedImportem = EWO.OperacjeIloscWszystkie();

            int nrwiersza=1;


                List<string> linijki = new List<string>();

                StreamReader file = new StreamReader(EWO.PlikImportu, Encoding.Default);

                while (!file.EndOfStream)
                {

                    string r = file.ReadLine();

                    w = r.ToLower();
                    /*
                    w = w.Replace("\",\"", ";");
                    w = w.Replace(",;", "");
                    w = w.Replace("__", ";");
                    w = w.Replace("___", ";");
                    w = w.Replace("____", ";");
                    w = w.Replace(" \t", ";brak;");
                    w = w.Replace("\t\t", ";brak;");
                    w = w.Replace("\t\t\t", ";brak;");
                    w = w.Replace("\t", ";");

                    w = w.Replace(";;;", ";brak informacji;");
                    w = w.Replace(";;", ";");                     // dla pliktow PKO
                    w = w.Replace(",,,", "");

                    */
                    w = w.Replace("\t", ";");
                    int index0 = w.IndexOf("\"");

                    if (index0 == 0)
                    {
                        w = w.Remove(0, 1); // kasuje znak \ jesli jest na poczatku
                    }


                    if (i > 0)
                    {
                        if (w.Contains(";-"))
                        {
                            string wp = "wydatek;";
                            w = wp + w;
                        }
                        else
                        {
                            string wp = "przychód;";
                            w = wp + w;
                        }

                    }



                    if (i < 1)
                    {
                        if (w.Contains("kwota operacji"))
                        {
                            string[] tabkw = w.Split(new string[] { ";", "\t" }, StringSplitOptions.None);
                            kw = Array.IndexOf(tabkw, "kwota operacji") + 1;
                        }

                        else if (w.Contains("kwota"))
                        {
                            string[] tabkw = w.Split(new string[] { ";", " \t" }, StringSplitOptions.None);
                            kw = Array.IndexOf(tabkw, "kwota") + 1;
                        }

                        if (w.Contains("szczegóły odbiorcy/nadawcy"))
                        {
                            string[] tabszcz = w.Split(new string[] { ";", "\t" }, StringSplitOptions.None);
                            szczegoly = Array.IndexOf(tabszcz, "szczegóły odbiorcy/nadawcy") + 1;
                        }

                        if (w.Contains("tytułem"))
                        {
                            string[] tabtyt = w.Split(new string[] { ";", "\t" }, StringSplitOptions.None);
                            tytulem = Array.IndexOf(tabtyt, "tytułem") + 1;
                        }

                        else if (w.Contains("opis transakcji"))
                        {
                            string[] tabtyt = w.Split(new string[] { ";", "\t" }, StringSplitOptions.None);
                            tytulem = Array.IndexOf(tabtyt, "opis transakcji") + 1;
                        }

                        else
                        {
                            MessageBox.Show("Nie znaleziono rubryki nazwy transakcji","EWO",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                        }

                    }

                    i++;

                    if (w.Contains("data księgowania"))
                    {
                        continue;
                    }

                    if (w.Contains("wypłata"))
                    {
                        continue;
                    }

                    else
                    {

                        string[] v = w.Split(new string[] { ";", "\t" }, StringSplitOptions.None);
                        string www = v[3];
                        int index5 = Array.IndexOf(v, www);

                        id = v[0];
                        string data = v[1];
                        string rok = data.Substring(0, 4);
                        string miesiac = data.Substring(4, 2);
                        string dzien = data.Substring(6, 2);
                        string data2 = rok + "-" + miesiac + "-" + dzien;

                        string kwota = v[kw];
                        kwota = kwota.Replace("-", "");
                        kwota = kwota.Replace("+", "");
                        
                        DataGridImport.Rows.Add(nrwiersza, v[0], v[szczegoly],v[tytulem], kwota, data2);
                        DataGridImport.Rows[nrwiersza - 1].Cells[6].Value = EWO.OpisPrzelewuSprawdz(v[tytulem]);

                        DataGridImport.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                        if (DataGridImport.Rows[nrwiersza-1].Cells[1].Value.ToString() == "wydatek")
                            DataGridImport.Rows[nrwiersza-1].DefaultCellStyle.BackColor = Color.Bisque;
                        else DataGridImport.Rows[nrwiersza-1].DefaultCellStyle.BackColor = Color.AliceBlue;

                        nrwiersza++;

                    }


            }
        }

        private void import_akcept_Click(object sender, EventArgs e)
        {
            int licznik = 0;

            for (int i = 0; i < this.DataGridImport.Rows.Count; i++)
            {

                
                    if (DataGridImport.Rows[i].Cells[6].Value.ToString() != "nie importuj")
                    {
                        EWO.OperacjaDodaj(DataGridImport.Rows[i].Cells[1].Value.ToString(), double.Parse(DataGridImport.Rows[i].Cells[4].Value.ToString()), DataGridImport.Rows[i].Cells[6].Value.ToString(), DataGridImport.Rows[i].Cells[5].Value.ToString());

                        EWO.OpisPrzelewuDodaj(DataGridImport.Rows[i].Cells[3].Value.ToString(), DataGridImport.Rows[i].Cells[6].Value.ToString());

                        string NowaData = DataGridImport.Rows[i].Cells[5].Value.ToString().Substring(0, 7);

                        int licz = 0;
                        for (int m = 0; m < EWO.ListaMiesiecyIlosc(); m++)
                        {
                            if (EWO.MiesiacOdczyt(m) == NowaData)
                                licz++;
                        }

                        if (licz == 0)
                        {
                            EWO.ListaMiesiecyDodaj(NowaData);
                        }

                        licznik++;
                }
            }

            MessageBox.Show("Zaimportowano " + licznik + " rekordów", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void DataGridImport_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Form FormListaKat = new FormListaKat(DataGridImport.CurrentRow.Index, DataGridImport.Rows[DataGridImport.CurrentRow.Index].Cells[1].Value.ToString());
            FormListaKat.ShowDialog();
            DataGridImport.Rows[DataGridImport.CurrentRow.Index].Cells[6].Value = EWO.ImportWyborWariantu;
        }

    }
       
    }

