﻿namespace EWO
{
    partial class FormTypyTowarow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TextBoxTypyTowarow = new System.Windows.Forms.TextBox();
            this.ContextMenuStripTowaryLevel1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItemTypTowaruUsun = new System.Windows.Forms.ToolStripMenuItem();
            this.ButtonTypTowaruDodaj = new System.Windows.Forms.Button();
            this.ToolTipTypyTowarowLB = new System.Windows.Forms.ToolTip(this.components);
            this.ToolTipTypyTowarowTB = new System.Windows.Forms.ToolTip(this.components);
            this.TreeViewTowary = new System.Windows.Forms.TreeView();
            this.ContextMenuStripTowaryLevel2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItemUsunTowar = new System.Windows.Forms.ToolStripMenuItem();
            this.GroupBoxTowary = new System.Windows.Forms.GroupBox();
            this.ContextMenuStripTowaryLevel1.SuspendLayout();
            this.ContextMenuStripTowaryLevel2.SuspendLayout();
            this.GroupBoxTowary.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextBoxTypyTowarow
            // 
            this.TextBoxTypyTowarow.Location = new System.Drawing.Point(23, 408);
            this.TextBoxTypyTowarow.Name = "TextBoxTypyTowarow";
            this.TextBoxTypyTowarow.Size = new System.Drawing.Size(155, 20);
            this.TextBoxTypyTowarow.TabIndex = 1;
            this.TextBoxTypyTowarow.DoubleClick += new System.EventHandler(this.TextBoxTypyTowarow_DoubleClick);
            // 
            // ContextMenuStripTowaryLevel1
            // 
            this.ContextMenuStripTowaryLevel1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemTypTowaruUsun});
            this.ContextMenuStripTowaryLevel1.Name = "ContextMenuStripTypyTowarow";
            this.ContextMenuStripTowaryLevel1.Size = new System.Drawing.Size(162, 48);
            // 
            // ToolStripMenuItemTypTowaruUsun
            // 
            this.ToolStripMenuItemTypTowaruUsun.Name = "ToolStripMenuItemTypTowaruUsun";
            this.ToolStripMenuItemTypTowaruUsun.Size = new System.Drawing.Size(161, 22);
            this.ToolStripMenuItemTypTowaruUsun.Text = "Usuń typ towaru";
            this.ToolStripMenuItemTypTowaruUsun.Click += new System.EventHandler(this.ToolStripMenuItemTypTowaruUsun_Click);
            // 
            // ButtonTypTowaruDodaj
            // 
            this.ButtonTypTowaruDodaj.Location = new System.Drawing.Point(23, 443);
            this.ButtonTypTowaruDodaj.Name = "ButtonTypTowaruDodaj";
            this.ButtonTypTowaruDodaj.Size = new System.Drawing.Size(155, 25);
            this.ButtonTypTowaruDodaj.TabIndex = 2;
            this.ButtonTypTowaruDodaj.Text = "Zarejestruj typ towaru";
            this.ButtonTypTowaruDodaj.UseVisualStyleBackColor = true;
            this.ButtonTypTowaruDodaj.Click += new System.EventHandler(this.ButtonTypTowaruDodaj_Click);
            // 
            // TreeViewTowary
            // 
            this.TreeViewTowary.Location = new System.Drawing.Point(20, 19);
            this.TreeViewTowary.Name = "TreeViewTowary";
            this.TreeViewTowary.Size = new System.Drawing.Size(383, 328);
            this.TreeViewTowary.TabIndex = 0;
            this.TreeViewTowary.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.TreeViewTowary_NodeMouseClick);
            // 
            // ContextMenuStripTowaryLevel2
            // 
            this.ContextMenuStripTowaryLevel2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemUsunTowar});
            this.ContextMenuStripTowaryLevel2.Name = "ContextMenuStripTowary";
            this.ContextMenuStripTowaryLevel2.Size = new System.Drawing.Size(135, 26);
            // 
            // ToolStripMenuItemUsunTowar
            // 
            this.ToolStripMenuItemUsunTowar.Name = "ToolStripMenuItemUsunTowar";
            this.ToolStripMenuItemUsunTowar.Size = new System.Drawing.Size(134, 22);
            this.ToolStripMenuItemUsunTowar.Text = "Usuń towar";
            this.ToolStripMenuItemUsunTowar.Click += new System.EventHandler(this.ToolStripMenuItemUsunTowar_Click);
            // 
            // GroupBoxTowary
            // 
            this.GroupBoxTowary.Controls.Add(this.TreeViewTowary);
            this.GroupBoxTowary.Location = new System.Drawing.Point(12, 12);
            this.GroupBoxTowary.Name = "GroupBoxTowary";
            this.GroupBoxTowary.Size = new System.Drawing.Size(423, 367);
            this.GroupBoxTowary.TabIndex = 5;
            this.GroupBoxTowary.TabStop = false;
            this.GroupBoxTowary.Text = "Struktura działań, typów towarów i towarów";
            // 
            // FormTypyTowarow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 503);
            this.Controls.Add(this.GroupBoxTowary);
            this.Controls.Add(this.TextBoxTypyTowarow);
            this.Controls.Add(this.ButtonTypTowaruDodaj);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormTypyTowarow";
            this.Text = "EWO";
            this.Load += new System.EventHandler(this.FormTypyTowarow_Load);
            this.ContextMenuStripTowaryLevel1.ResumeLayout(false);
            this.ContextMenuStripTowaryLevel2.ResumeLayout(false);
            this.GroupBoxTowary.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonTypTowaruDodaj;
        private System.Windows.Forms.TextBox TextBoxTypyTowarow;
        private System.Windows.Forms.ToolTip ToolTipTypyTowarowLB;
        private System.Windows.Forms.ContextMenuStrip ContextMenuStripTowaryLevel1;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemTypTowaruUsun;
        private System.Windows.Forms.ToolTip ToolTipTypyTowarowTB;
        private System.Windows.Forms.TreeView TreeViewTowary;
        private System.Windows.Forms.ContextMenuStrip ContextMenuStripTowaryLevel2;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemUsunTowar;
        private System.Windows.Forms.GroupBox GroupBoxTowary;
    }
}