﻿namespace EWO
{
    partial class FormHaslo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBoxHaslo1 = new System.Windows.Forms.TextBox();
            this.TextBoxHaslo2 = new System.Windows.Forms.TextBox();
            this.ButtonZapisz = new System.Windows.Forms.Button();
            this.LabelHaslo1 = new System.Windows.Forms.Label();
            this.LabelHaslo2 = new System.Windows.Forms.Label();
            this.GroupBoxHaslo = new System.Windows.Forms.GroupBox();
            this.GroupBoxHaslo.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextBoxHaslo1
            // 
            this.TextBoxHaslo1.Location = new System.Drawing.Point(14, 43);
            this.TextBoxHaslo1.Name = "TextBoxHaslo1";
            this.TextBoxHaslo1.Size = new System.Drawing.Size(149, 20);
            this.TextBoxHaslo1.TabIndex = 0;
            this.TextBoxHaslo1.UseSystemPasswordChar = true;
            // 
            // TextBoxHaslo2
            // 
            this.TextBoxHaslo2.Location = new System.Drawing.Point(14, 85);
            this.TextBoxHaslo2.Name = "TextBoxHaslo2";
            this.TextBoxHaslo2.Size = new System.Drawing.Size(149, 20);
            this.TextBoxHaslo2.TabIndex = 1;
            this.TextBoxHaslo2.UseSystemPasswordChar = true;
            // 
            // ButtonZapisz
            // 
            this.ButtonZapisz.Location = new System.Drawing.Point(116, 147);
            this.ButtonZapisz.Name = "ButtonZapisz";
            this.ButtonZapisz.Size = new System.Drawing.Size(75, 23);
            this.ButtonZapisz.TabIndex = 2;
            this.ButtonZapisz.Text = "Zapisz";
            this.ButtonZapisz.UseVisualStyleBackColor = true;
            this.ButtonZapisz.Click += new System.EventHandler(this.ButtonZapisz_Click);
            // 
            // LabelHaslo1
            // 
            this.LabelHaslo1.AutoSize = true;
            this.LabelHaslo1.Location = new System.Drawing.Point(16, 25);
            this.LabelHaslo1.Name = "LabelHaslo1";
            this.LabelHaslo1.Size = new System.Drawing.Size(64, 13);
            this.LabelHaslo1.TabIndex = 3;
            this.LabelHaslo1.Text = "Podaj hasło";
            // 
            // LabelHaslo2
            // 
            this.LabelHaslo2.AutoSize = true;
            this.LabelHaslo2.Location = new System.Drawing.Point(16, 66);
            this.LabelHaslo2.Name = "LabelHaslo2";
            this.LabelHaslo2.Size = new System.Drawing.Size(126, 13);
            this.LabelHaslo2.TabIndex = 4;
            this.LabelHaslo2.Text = "Podaj hasło (weryfikacja)";
            // 
            // GroupBoxHaslo
            // 
            this.GroupBoxHaslo.Controls.Add(this.LabelHaslo2);
            this.GroupBoxHaslo.Controls.Add(this.LabelHaslo1);
            this.GroupBoxHaslo.Controls.Add(this.TextBoxHaslo2);
            this.GroupBoxHaslo.Controls.Add(this.TextBoxHaslo1);
            this.GroupBoxHaslo.Location = new System.Drawing.Point(14, 10);
            this.GroupBoxHaslo.Name = "GroupBoxHaslo";
            this.GroupBoxHaslo.Size = new System.Drawing.Size(177, 131);
            this.GroupBoxHaslo.TabIndex = 5;
            this.GroupBoxHaslo.TabStop = false;
            this.GroupBoxHaslo.Text = "Zmiana hasła";
            // 
            // FormHaslo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(209, 182);
            this.Controls.Add(this.GroupBoxHaslo);
            this.Controls.Add(this.ButtonZapisz);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormHaslo";
            this.Text = "EWO";
            this.Load += new System.EventHandler(this.FormHaslo_Load);
            this.GroupBoxHaslo.ResumeLayout(false);
            this.GroupBoxHaslo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxHaslo1;
        private System.Windows.Forms.TextBox TextBoxHaslo2;
        private System.Windows.Forms.Button ButtonZapisz;
        private System.Windows.Forms.Label LabelHaslo1;
        private System.Windows.Forms.Label LabelHaslo2;
        private System.Windows.Forms.GroupBox GroupBoxHaslo;
    }
}