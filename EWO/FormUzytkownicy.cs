﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EWO
{
    public partial class FormUzytkownicy : Form
    {
        DataGridView DataGridUzytkownicy;

        public FormUzytkownicy()
        {
            InitializeComponent();
        }

        private void FormUzytkownicy_Load(object sender, EventArgs e)
        {
            this.Icon = EWO.Ikonka;

            EWO.UzytkownicyWczytaj();
            EWO.UprawnieniaWczytaj();

            for (int i = 0; i < EWO.SerweryPocztoweIlosc(); i++)
                ComboBoxSmtpHost.Items.Add(EWO.SerwerPocztowyOdczyt(i).SmtpHost);

            ComboBoxSmtpHost.Text = EWO.SerwerPocztowyOdczyt(0).SmtpHost;

            for (int i = 0; i < EWO.UprawnieniaIlosc(); i++)
                ComboBoxPoziomUprawnien.Items.Add(EWO.UprawnieniaOdczyt(i).Opis);

            ComboBoxPoziomUprawnien.Text = EWO.UprawnieniaOdczyt(0).Opis;

            TextBoxFolderPDF.Text = EWO.FolderProgramu;
            TextBoxFolderXLSX.Text = EWO.FolderProgramu;
            EWO.tmpFolderPDF = TextBoxFolderPDF.Text;
            EWO.tmpFolderXLSX = TextBoxFolderXLSX.Text;

            ToolTipFolderPDF.SetToolTip(TextBoxFolderPDF, EWO.tmpFolderPDF);
            ToolTipFolderXLSX.SetToolTip(TextBoxFolderXLSX, EWO.tmpFolderXLSX);

            DataGridUzytkownicy = new DataGridView();

            DataGridUzytkownicy.RowHeadersVisible = false;
            DataGridUzytkownicy.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            DataGridUzytkownicy.AllowUserToResizeRows = false;
            DataGridUzytkownicy.AllowUserToResizeColumns = false;
            DataGridUzytkownicy.AllowUserToAddRows = false;
            DataGridUzytkownicy.MultiSelect = false;
            DataGridUzytkownicy.ReadOnly = true;
            DataGridUzytkownicy.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            DataGridUzytkownicy.Location = new Point(15, 17);
            DataGridUzytkownicy.Size = new Size(440, 320);

            DataGridViewTextBoxColumn kolumna0 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn kolumna1 = new DataGridViewTextBoxColumn();
            DataGridViewComboBoxColumn kolumna2 = new DataGridViewComboBoxColumn();

            for (int i = 0; i < EWO.SerweryPocztoweIlosc(); i++)
                kolumna2.Items.Add(EWO.SerwerPocztowyOdczyt(i).SmtpHost);

            DataGridViewTextBoxColumn kolumna3 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn kolumna4 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn kolumna5 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn kolumna6 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn kolumna7 = new DataGridViewTextBoxColumn();
            DataGridViewCheckBoxColumn kolumna8 = new DataGridViewCheckBoxColumn();
            DataGridViewTextBoxColumn kolumna9 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn kolumna10 = new DataGridViewTextBoxColumn();

            DataGridViewComboBoxColumn kolumna11 = new DataGridViewComboBoxColumn();

            for (int i = 0; i < EWO.UprawnieniaIlosc(); i++)
                kolumna11.Items.Add(EWO.UprawnieniaOdczyt(i).Opis);

            DataGridUzytkownicy.Columns.Add(kolumna0);
            DataGridUzytkownicy.Columns.Add(kolumna1);
            DataGridUzytkownicy.Columns.Add(kolumna2);
            DataGridUzytkownicy.Columns.Add(kolumna3);
            DataGridUzytkownicy.Columns.Add(kolumna4);
            DataGridUzytkownicy.Columns.Add(kolumna5);
            DataGridUzytkownicy.Columns.Add(kolumna6);
            DataGridUzytkownicy.Columns.Add(kolumna7);
            DataGridUzytkownicy.Columns.Add(kolumna8);
            DataGridUzytkownicy.Columns.Add(kolumna9);
            DataGridUzytkownicy.Columns.Add(kolumna10);
            DataGridUzytkownicy.Columns.Add(kolumna11);

            kolumna0.HeaderText = "Użytkownik";
            kolumna1.HeaderText = "Hasło";
            kolumna2.HeaderText = "Serwer SMTP";
            kolumna3.HeaderText = "Nazwa użytkownika SMTP";
            kolumna4.HeaderText = "Nadawca";
            kolumna5.HeaderText = "Adresat";
            kolumna6.HeaderText = "Temat wiadomości";
            kolumna7.HeaderText = "Treść wiadomości";
            kolumna8.HeaderText = "Minimalizacja do zasobnika";
            kolumna9.HeaderText = "Folder (raport PDF)";
            kolumna10.HeaderText = "Folder (eksport XLSX)";
            kolumna11.HeaderText = "Poziom uprawnień";

            for (int i = 0; i < EWO.UzytkownicyIlosc(); i++)
            {
                var uzyt = EWO.UzytkownikOdczyt(i);

                DataGridUzytkownicy.Rows.Add(
                    uzyt.Nazwa,
                    uzyt.Haslo,
                    uzyt.SmtpHost,
                    uzyt.SmtpLogin,
                    uzyt.MailFrom,
                    uzyt.MailTo,
                    uzyt.MailSubject,
                    uzyt.MailBody,
                    uzyt.MinimizeToTray,
                    uzyt.FolderPDF,
                    uzyt.FolderXLSX,
                    EWO.ZnajdzOpisUprawnien(uzyt.PoziomUprawnien)
                                        );
            }

            GroupBoxUzytkownicy.Controls.Add(DataGridUzytkownicy);

            DataGridUzytkownicy.AutoResizeColumns();
        }

        private void ButtonDodaj_Click(object sender, EventArgs e)
        {
            if (TextBoxUzytkownik.Text.Length > 0 && TextBoxHaslo1.Text.Length > 0 && TextBoxHaslo2.Text.Length > 0 && TextBoxMailFrom.Text.Length > 0 && TextBoxMailTo.Text.Length > 0 && TextBoxSubject.Text.Length > 0 && TextBoxBody.Text.Length > 0)
            {
                if (TextBoxHaslo1.Text == TextBoxHaslo2.Text)
                {
                    string haslo = EWO.Zakoduj(TextBoxHaslo1.Text);
                    Int16 poziomupr = EWO.ZnajdzPoziomUprawnien(ComboBoxPoziomUprawnien.Text);
                    EWO.UzytkownikDodaj(TextBoxUzytkownik.Text, haslo, ComboBoxSmtpHost.Text, TextBoxSmtpLogin.Text, TextBoxMailFrom.Text, TextBoxMailTo.Text, TextBoxSubject.Text, TextBoxBody.Text, CheckBoxMinimizeToTray.Checked, TextBoxFolderPDF.Text, TextBoxFolderXLSX.Text, poziomupr);
                    DataGridUzytkownicy.Rows.Add(TextBoxUzytkownik.Text, haslo, ComboBoxSmtpHost.Text, TextBoxSmtpLogin.Text, TextBoxMailFrom.Text, TextBoxMailTo.Text, TextBoxSubject.Text, TextBoxBody.Text, CheckBoxMinimizeToTray.Checked, TextBoxFolderPDF.Text, TextBoxFolderXLSX.Text);
                }

                else
                    MessageBox.Show("Niezgodne hasła...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Wypełnij wszystkie pola...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void ToolStripMenuItemUstawFoldery_Click(object sender, EventArgs e)
        {
            Form FormFoldery = new FormFoldery(false);
            FormFoldery.ShowDialog();

            TextBoxFolderPDF.Text = EWO.tmpFolderPDF;
            TextBoxFolderXLSX.Text = EWO.tmpFolderXLSX;
            ToolTipFolderPDF.SetToolTip(TextBoxFolderPDF, EWO.tmpFolderPDF);
            ToolTipFolderXLSX.SetToolTip(TextBoxFolderXLSX, EWO.tmpFolderXLSX);
        }
    }
}
