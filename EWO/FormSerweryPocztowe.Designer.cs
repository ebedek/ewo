﻿namespace EWO
{
    partial class FormSerweryPocztowe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GroupBoxSerweryPocztowe = new System.Windows.Forms.GroupBox();
            this.ListBoxSerweryPocztowe = new System.Windows.Forms.ListBox();
            this.ContextMenuStripSerweryPocztowe = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.UsunDaneerweraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TextBoxHost = new System.Windows.Forms.TextBox();
            this.TextBoxPort = new System.Windows.Forms.TextBox();
            this.ButtonSerwerPocztowyDodaj = new System.Windows.Forms.Button();
            this.ToolTipSerweryPocztoweLB = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.ToolTipSerweryPocztoweTB = new System.Windows.Forms.ToolTip(this.components);
            this.GroupBoxSerweryPocztowe.SuspendLayout();
            this.ContextMenuStripSerweryPocztowe.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBoxSerweryPocztowe
            // 
            this.GroupBoxSerweryPocztowe.Controls.Add(this.ListBoxSerweryPocztowe);
            this.GroupBoxSerweryPocztowe.Location = new System.Drawing.Point(12, 12);
            this.GroupBoxSerweryPocztowe.Name = "GroupBoxSerweryPocztowe";
            this.GroupBoxSerweryPocztowe.Size = new System.Drawing.Size(260, 197);
            this.GroupBoxSerweryPocztowe.TabIndex = 0;
            this.GroupBoxSerweryPocztowe.TabStop = false;
            this.GroupBoxSerweryPocztowe.Text = "Serwery pocztowe";
            // 
            // ListBoxSerweryPocztowe
            // 
            this.ListBoxSerweryPocztowe.ContextMenuStrip = this.ContextMenuStripSerweryPocztowe;
            this.ListBoxSerweryPocztowe.FormattingEnabled = true;
            this.ListBoxSerweryPocztowe.Location = new System.Drawing.Point(16, 29);
            this.ListBoxSerweryPocztowe.Name = "ListBoxSerweryPocztowe";
            this.ListBoxSerweryPocztowe.Size = new System.Drawing.Size(219, 147);
            this.ListBoxSerweryPocztowe.TabIndex = 0;
            this.ListBoxSerweryPocztowe.DoubleClick += new System.EventHandler(this.ListBoxSerweryPocztowe_DoubleClick);
            // 
            // ContextMenuStripSerweryPocztowe
            // 
            this.ContextMenuStripSerweryPocztowe.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.UsunDaneerweraToolStripMenuItem});
            this.ContextMenuStripSerweryPocztowe.Name = "ContextMenuStripSerweryPocztowe";
            this.ContextMenuStripSerweryPocztowe.Size = new System.Drawing.Size(174, 26);
            // 
            // UsunDaneerweraToolStripMenuItem
            // 
            this.UsunDaneerweraToolStripMenuItem.Name = "UsunDaneerweraToolStripMenuItem";
            this.UsunDaneerweraToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.UsunDaneerweraToolStripMenuItem.Text = "Usuń dane serwera";
            this.UsunDaneerweraToolStripMenuItem.Click += new System.EventHandler(this.UsunDaneerweraToolStripMenuItem_Click);
            // 
            // TextBoxHost
            // 
            this.TextBoxHost.Location = new System.Drawing.Point(28, 225);
            this.TextBoxHost.Name = "TextBoxHost";
            this.TextBoxHost.Size = new System.Drawing.Size(150, 20);
            this.TextBoxHost.TabIndex = 1;
            this.TextBoxHost.DoubleClick += new System.EventHandler(this.TextBoxHost_DoubleClick);
            // 
            // TextBoxPort
            // 
            this.TextBoxPort.Location = new System.Drawing.Point(197, 225);
            this.TextBoxPort.Name = "TextBoxPort";
            this.TextBoxPort.Size = new System.Drawing.Size(50, 20);
            this.TextBoxPort.TabIndex = 2;
            this.TextBoxPort.DoubleClick += new System.EventHandler(this.TextBoxPort_DoubleClick);
            // 
            // ButtonSerwerPocztowyDodaj
            // 
            this.ButtonSerwerPocztowyDodaj.Location = new System.Drawing.Point(28, 261);
            this.ButtonSerwerPocztowyDodaj.Name = "ButtonSerwerPocztowyDodaj";
            this.ButtonSerwerPocztowyDodaj.Size = new System.Drawing.Size(219, 23);
            this.ButtonSerwerPocztowyDodaj.TabIndex = 3;
            this.ButtonSerwerPocztowyDodaj.Text = "Zarejestruj dane serwera";
            this.ButtonSerwerPocztowyDodaj.UseVisualStyleBackColor = true;
            this.ButtonSerwerPocztowyDodaj.Click += new System.EventHandler(this.ButtonSerwerPocztowyDodaj_Click);
            // 
            // FormSerweryPocztowe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(290, 296);
            this.Controls.Add(this.ButtonSerwerPocztowyDodaj);
            this.Controls.Add(this.TextBoxPort);
            this.Controls.Add(this.TextBoxHost);
            this.Controls.Add(this.GroupBoxSerweryPocztowe);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSerweryPocztowe";
            this.Text = "EWO";
            this.Load += new System.EventHandler(this.FormSerweryPocztowe_Load);
            this.GroupBoxSerweryPocztowe.ResumeLayout(false);
            this.ContextMenuStripSerweryPocztowe.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox GroupBoxSerweryPocztowe;
        private System.Windows.Forms.TextBox TextBoxHost;
        private System.Windows.Forms.TextBox TextBoxPort;
        private System.Windows.Forms.Button ButtonSerwerPocztowyDodaj;
        private System.Windows.Forms.ListBox ListBoxSerweryPocztowe;
        private System.Windows.Forms.ContextMenuStrip ContextMenuStripSerweryPocztowe;
        private System.Windows.Forms.ToolStripMenuItem UsunDaneerweraToolStripMenuItem;
        private System.Windows.Forms.ToolTip ToolTipSerweryPocztoweLB;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.ToolTip ToolTipSerweryPocztoweTB;
    }
}