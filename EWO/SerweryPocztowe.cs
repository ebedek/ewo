﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWO
{
    public class SerweryPocztoweT
    {
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }

        public SerweryPocztoweT(string smtphost, int smtpport)
        {
            SmtpHost = smtphost;
            SmtpPort = smtpport;
        }
    }
}
