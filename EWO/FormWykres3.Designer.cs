﻿namespace EWO
{
    partial class FormWykres3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.FormR3RadioButton1 = new System.Windows.Forms.RadioButton();
            this.FormR3RadioButton2 = new System.Windows.Forms.RadioButton();
            this.FormR3GroupBox = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            this.FormR3GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // chart3
            // 
            chartArea1.Area3DStyle.Enable3D = true;
            chartArea1.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea1);
            this.chart3.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Font = new System.Drawing.Font("Bookman Old Style", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            legend1.IsTextAutoFit = false;
            legend1.Name = "Legend1";
            this.chart3.Legends.Add(legend1);
            this.chart3.Location = new System.Drawing.Point(0, 0);
            this.chart3.Name = "chart3";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series1.Font = new System.Drawing.Font("Bookman Old Style", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            series1.IsValueShownAsLabel = true;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            series1.YValuesPerPoint = 4;
            this.chart3.Series.Add(series1);
            this.chart3.Size = new System.Drawing.Size(607, 443);
            this.chart3.TabIndex = 0;
            this.chart3.Text = "chart3";
            title1.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            title1.Name = "Title1";
            this.chart3.Titles.Add(title1);
            // 
            // FormR3RadioButton1
            // 
            this.FormR3RadioButton1.AutoSize = true;
            this.FormR3RadioButton1.Location = new System.Drawing.Point(6, 19);
            this.FormR3RadioButton1.Name = "FormR3RadioButton1";
            this.FormR3RadioButton1.Size = new System.Drawing.Size(61, 17);
            this.FormR3RadioButton1.TabIndex = 1;
            this.FormR3RadioButton1.TabStop = true;
            this.FormR3RadioButton1.Text = "wydatki";
            this.FormR3RadioButton1.UseVisualStyleBackColor = true;
            this.FormR3RadioButton1.CheckedChanged += new System.EventHandler(this.FormR3RadioButton_CheckedChanged);
            // 
            // FormR3RadioButton2
            // 
            this.FormR3RadioButton2.AutoSize = true;
            this.FormR3RadioButton2.Location = new System.Drawing.Point(6, 42);
            this.FormR3RadioButton2.Name = "FormR3RadioButton2";
            this.FormR3RadioButton2.Size = new System.Drawing.Size(73, 17);
            this.FormR3RadioButton2.TabIndex = 2;
            this.FormR3RadioButton2.TabStop = true;
            this.FormR3RadioButton2.Text = "przychody";
            this.FormR3RadioButton2.UseVisualStyleBackColor = true;
            this.FormR3RadioButton2.CheckedChanged += new System.EventHandler(this.FormR3RadioButton_CheckedChanged);
            // 
            // FormR3GroupBox
            // 
            this.FormR3GroupBox.BackColor = System.Drawing.SystemColors.Window;
            this.FormR3GroupBox.Controls.Add(this.FormR3RadioButton1);
            this.FormR3GroupBox.Controls.Add(this.FormR3RadioButton2);
            this.FormR3GroupBox.Location = new System.Drawing.Point(501, 355);
            this.FormR3GroupBox.Name = "FormR3GroupBox";
            this.FormR3GroupBox.Size = new System.Drawing.Size(94, 76);
            this.FormR3GroupBox.TabIndex = 3;
            this.FormR3GroupBox.TabStop = false;
            this.FormR3GroupBox.Text = "Operacje";
            // 
            // FormRaport3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 443);
            this.Controls.Add(this.FormR3GroupBox);
            this.Controls.Add(this.chart3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(623, 481);
            this.Name = "FormRaport3";
            this.Text = "EWO";
            this.Load += new System.EventHandler(this.FormWykres3_Load);
            this.Resize += new System.EventHandler(this.FormWykres3_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            this.FormR3GroupBox.ResumeLayout(false);
            this.FormR3GroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
        private System.Windows.Forms.RadioButton FormR3RadioButton1;
        private System.Windows.Forms.RadioButton FormR3RadioButton2;
        private System.Windows.Forms.GroupBox FormR3GroupBox;
    }
}