﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Mail;
using System.IO;

namespace EWO
{
    public partial class FormParametry : Form
    {
        public FormParametry()
        {
            InitializeComponent();
        }

        bool CzyPoprawnyAdresOd = true;
        bool CzyPoprawnyAdresDo = true;
        bool CzyPoprawnyPort = true;

        private void FormParametry_Load(object sender, EventArgs e)
        {
            this.Icon = EWO.Ikonka;

            for (int i = 0; i < EWO.SerweryPocztoweIlosc(); i++)
                ComboBoxHost.Items.Add(EWO.SerwerPocztowyOdczyt(i).SmtpHost);

            ComboBoxHost.Text = EWO.wiadomosc.SmtpSrvHost;
            TextBoxPort.Text = EWO.ZnajdzPortSerwera(EWO.wiadomosc.SmtpSrvHost).ToString();
            TextBoxLogin.Text = EWO.wiadomosc.SmtpLogin;
            TextBoxPassword.Text = EWO.wiadomosc.SmtpPassword;
            TextBoxFrom.Text = EWO.wiadomosc.MailFrom;
            TextBoxTo.Text = EWO.wiadomosc.MailTo;
            TextBoxSubject.Text = EWO.wiadomosc.MailSubject;
            TextBoxBody.Text = EWO.wiadomosc.MailBody;

            if (EWO.wiadomosc.AttachXLSX == true)
                CheckBoxXLSX.Checked = true;

            if (EWO.wiadomosc.AttachPDF == true)
                CheckBoxPDF.Checked = true;

            if (EWO.MinimizeToTray == true)
                CheckBoxMinimizeToTray.Checked = true;

            ToolTipPassword.SetToolTip(TextBoxPassword, "Hasło można wprowadzić przy wysyłaniu wiadomości...");

            if (File.Exists(EWO.ZalacznikiOdczyt("XLSX").Katalog+"\\EWO-export.xlsx"))
                CheckBoxXLSX.ForeColor = Color.Green;
            else
                CheckBoxXLSX.ForeColor = Color.Red;

            if (File.Exists(EWO.ZalacznikiOdczyt("PDF").Katalog+"\\EWO-raport.pdf"))
                CheckBoxPDF.ForeColor = Color.Green;
            else
                CheckBoxPDF.ForeColor = Color.Red;

        }

        private void ButtonZapisz_Click(object sender, EventArgs e)
        {
            if (CheckBoxXLSX.Checked == false && CheckBoxPDF.Checked == false)
            {
                MessageBox.Show("Zaznacz przynajmniej jeden plik do wysłania...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            else
            {
                if (CzyPoprawnyPort == false)
                    MessageBox.Show("Popraw port!", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
                else if (CzyPoprawnyAdresDo == false || CzyPoprawnyAdresOd == false)
                    MessageBox.Show("Popraw adres(y) e-mail!", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);

                else
                {
                    if (TextBoxFrom.Text.Length > 0 && TextBoxTo.Text.Length > 0 && TextBoxSubject.Text.Length > 0 && ComboBoxHost.Text.Length > 0 && TextBoxLogin.Text.Length > 0)
                    {
                        EWO.wiadomosc.SmtpSrvHost = ComboBoxHost.Text;
                        EWO.wiadomosc.SmtpLogin = TextBoxLogin.Text;
                        EWO.wiadomosc.SmtpPassword = TextBoxPassword.Text;
                        EWO.wiadomosc.MailFrom = TextBoxFrom.Text;
                        EWO.wiadomosc.MailTo = TextBoxTo.Text;
                        EWO.wiadomosc.MailSubject = TextBoxSubject.Text;
                        EWO.wiadomosc.MailBody = TextBoxBody.Text;

                        if (CheckBoxXLSX.Checked == true)
                            EWO.wiadomosc.AttachXLSX = true;
                        else
                            EWO.wiadomosc.AttachXLSX = false;

                        if (CheckBoxPDF.Checked == true)
                            EWO.wiadomosc.AttachPDF = true;
                        else
                            EWO.wiadomosc.AttachPDF = false;

                        if (CheckBoxMinimizeToTray.Checked == true)
                            EWO.MinimizeToTray = true;
                        else
                            EWO.MinimizeToTray = false;

                        EWO.UstawieniaZapisz();

                        MessageBox.Show("Dane zapisane.", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }

                    else
                        MessageBox.Show("Wypełnij pola obowiązkowe [*] ", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void TextBoxPort_Leave(object sender, EventArgs e)
        {
            try
            {
                int tmp = int.Parse(TextBoxPort.Text);
                TextBoxPort.BackColor = Color.White;
                CzyPoprawnyPort = true;
            }
            catch
            {
                MessageBox.Show("Podaj prawidłowy numer!", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                TextBoxPort.BackColor = Color.Red;
                CzyPoprawnyPort = false;
            }
        }

        private void TextBoxFrom_Leave(object sender, EventArgs e)
        {
            try
            {
                MailAddress m = new MailAddress(TextBoxFrom.Text);
                TextBoxFrom.BackColor = Color.White;
                CzyPoprawnyAdresOd = true;
            }
            catch
            {
                MessageBox.Show("Podaj prawidłowy adres e-mail!", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                TextBoxFrom.BackColor = Color.Red;
                CzyPoprawnyAdresOd = false;
            }
        }

        private void TextBoxTo_Leave(object sender, EventArgs e)
        {
            try
            {
                MailAddress m = new MailAddress(TextBoxTo.Text);
                TextBoxTo.BackColor = Color.White;
                CzyPoprawnyAdresDo = true;
            }
            catch
            {
                MessageBox.Show("Podaj prawidłowy adres e-mail!", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                TextBoxTo.BackColor = Color.Red;
                CzyPoprawnyAdresDo = false;
            }
        }

        private void ComboBoxHost_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxPort.Text = EWO.ZnajdzPortSerwera(ComboBoxHost.Text).ToString();
        }

        private void ToolStripMenuItemUstawFoldery_Click(object sender, EventArgs e)
        {
            Form FormFoldery = new FormFoldery(true);
            FormFoldery.ShowDialog();
        }

        private void ToolStripMenuItemZmienHaslo_Click(object sender, EventArgs e)
        {
            Form FormHaslo = new FormHaslo();
            FormHaslo.ShowDialog();
        }
    }
}
