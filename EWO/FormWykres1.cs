﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace EWO
{
    public partial class FormWykres1 : Form
    {
        TypyDzialanT TypDzialania = new TypyDzialanT("wydatek");

        public FormWykres1()
        {
            InitializeComponent();
        }

        private void FormWykres1_Load(object sender, EventArgs e)
        {
            this.Icon = EWO.Ikonka;

            EWO.ListaMiesiecySort();

            this.FormR1RadioButton1.Checked = true;
            this.FormR1RadioButton2.Checked = false;

        }

        private void FormR1RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (this.FormR1RadioButton1.Checked == true)
            {
                TypDzialania.TypDzialania = "wydatek";
                EWO.TypyTowarowPobierz(TypDzialania);

                chart1.Series.Clear();
                chart1.ChartAreas[0].AxisX.Interval = 1;

                EWODataContext ewo = new EWODataContext();

                for (int i = 0; i < EWO.TypyTowarowTmpIlosc(); i++)
                {
                    chart1.Series.Add(EWO.TypTowaruTmpOdczyt(i).TypTowaru);
                    chart1.Series[i].ChartType = SeriesChartType.Column;
                    chart1.Series[i]["DrawingStyle"] = "Cylinder";

                    for (int j = 0; j < EWO.ListaMiesiecyIlosc(); j++)
                    {
                        double kwota1 = (double) ewo.funWykres1KategorieMiesiace(EWO.Uzytkownik, EWO.TypTowaruTmpOdczyt(i).TypTowaru, EWO.MiesiacOdczyt(j));
                        this.chart1.Series[i].Points.Add(kwota1);
                        chart1.Series[i].Name = EWO.TypTowaruTmpOdczyt(i).TypTowaru;
                        chart1.Series[i].Points[j].AxisLabel = EWO.MiesiacOdczyt(j);
                    }

                }
                
                chart1.ChartAreas[0].RecalculateAxesScale();
                chart1.Titles[0].Text = "Wydatki w podziale na kategorie - podział na miesiące";
            }

            if (this.FormR1RadioButton2.Checked == true)
            {
                TypDzialania.TypDzialania = "przychód";
                EWO.TypyTowarowPobierz(TypDzialania);

                chart1.Series.Clear();
                chart1.ChartAreas[0].AxisX.Interval = 1;

                EWODataContext ewo = new EWODataContext();

                for (int i = 0; i <  EWO.TypyTowarowTmpIlosc(); i++)
                {
                    chart1.Series.Add(EWO.TypTowaruTmpOdczyt(i).TypTowaru);
                    chart1.Series[i].ChartType = SeriesChartType.Column;
                    chart1.Series[i]["DrawingStyle"] = "Cylinder";

                    for (int j = 0; j < EWO.ListaMiesiecyIlosc(); j++)
                    {
                        double kwota1 = (double) ewo.funWykres1KategorieMiesiace(EWO.Uzytkownik, EWO.TypTowaruTmpOdczyt(i).TypTowaru, EWO.MiesiacOdczyt(j));
                        this.chart1.Series[i].Points.Add(kwota1);
                        chart1.Series[i].Name = EWO.TypTowaruTmpOdczyt(i).TypTowaru;
                        chart1.Series[i].Points[j].AxisLabel = EWO.MiesiacOdczyt(j);
                    }

                }

                chart1.ChartAreas[0].RecalculateAxesScale();
                chart1.Titles[0].Text = "Przychody w podziale na kategorie - podział na miesiące";
            }
        }

        private void FormWykres1_Resize(object sender, EventArgs e)
        {
            int wysokoscf = this.Size.Height;
            int szerokoscf = this.Size.Width;

            FormR1GroupBox.Location = new Point(szerokoscf - (FormR1GroupBox.Size.Width+30),wysokoscf - (FormR1GroupBox.Size.Height+50));

        }
    }
}
