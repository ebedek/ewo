﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

namespace EWO
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (System.Diagnostics.Process.GetProcessesByName(@Path.GetFileNameWithoutExtension(Application.ExecutablePath)).Length > 1)
                MessageBox.Show("Program jest już uruchomiony.", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                string PelnaSciezka = Path.GetFullPath(@Path.GetFileName(Application.ExecutablePath));
                EWO.FolderProgramu = PelnaSciezka.Substring(0, PelnaSciezka.Length - Path.GetFileName(Application.ExecutablePath).Length);
                Application.Run(new FormLogowanie());
            }
        }
    }
}
