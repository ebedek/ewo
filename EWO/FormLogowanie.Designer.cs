﻿namespace EWO
{
    partial class FormLogowanie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLogowanie));
            this.TextBoxUzytkownik = new System.Windows.Forms.TextBox();
            this.TextBoxHaslo = new System.Windows.Forms.TextBox();
            this.ButtonZaloguj = new System.Windows.Forms.Button();
            this.LabelUzytkownik = new System.Windows.Forms.Label();
            this.LabelHaslo = new System.Windows.Forms.Label();
            this.GroupBoxLogowanie = new System.Windows.Forms.GroupBox();
            this.GroupBoxLogowanie.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextBoxUzytkownik
            // 
            this.TextBoxUzytkownik.Location = new System.Drawing.Point(98, 19);
            this.TextBoxUzytkownik.Name = "TextBoxUzytkownik";
            this.TextBoxUzytkownik.Size = new System.Drawing.Size(154, 20);
            this.TextBoxUzytkownik.TabIndex = 0;
            // 
            // TextBoxHaslo
            // 
            this.TextBoxHaslo.Location = new System.Drawing.Point(98, 45);
            this.TextBoxHaslo.Name = "TextBoxHaslo";
            this.TextBoxHaslo.Size = new System.Drawing.Size(154, 20);
            this.TextBoxHaslo.TabIndex = 1;
            this.TextBoxHaslo.UseSystemPasswordChar = true;
            // 
            // ButtonZaloguj
            // 
            this.ButtonZaloguj.Location = new System.Drawing.Point(205, 99);
            this.ButtonZaloguj.Name = "ButtonZaloguj";
            this.ButtonZaloguj.Size = new System.Drawing.Size(75, 23);
            this.ButtonZaloguj.TabIndex = 2;
            this.ButtonZaloguj.Text = "Zaloguj";
            this.ButtonZaloguj.UseVisualStyleBackColor = true;
            this.ButtonZaloguj.Click += new System.EventHandler(this.ButtonZaloguj_Click);
            // 
            // LabelUzytkownik
            // 
            this.LabelUzytkownik.AutoSize = true;
            this.LabelUzytkownik.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LabelUzytkownik.Location = new System.Drawing.Point(9, 22);
            this.LabelUzytkownik.Name = "LabelUzytkownik";
            this.LabelUzytkownik.Size = new System.Drawing.Size(62, 13);
            this.LabelUzytkownik.TabIndex = 3;
            this.LabelUzytkownik.Text = "Użytkownik";
            // 
            // LabelHaslo
            // 
            this.LabelHaslo.AutoSize = true;
            this.LabelHaslo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LabelHaslo.Location = new System.Drawing.Point(9, 48);
            this.LabelHaslo.Name = "LabelHaslo";
            this.LabelHaslo.Size = new System.Drawing.Size(36, 13);
            this.LabelHaslo.TabIndex = 4;
            this.LabelHaslo.Text = "Hasło";
            // 
            // GroupBoxLogowanie
            // 
            this.GroupBoxLogowanie.Controls.Add(this.LabelHaslo);
            this.GroupBoxLogowanie.Controls.Add(this.TextBoxHaslo);
            this.GroupBoxLogowanie.Controls.Add(this.LabelUzytkownik);
            this.GroupBoxLogowanie.Controls.Add(this.TextBoxUzytkownik);
            this.GroupBoxLogowanie.Location = new System.Drawing.Point(12, 3);
            this.GroupBoxLogowanie.Name = "GroupBoxLogowanie";
            this.GroupBoxLogowanie.Size = new System.Drawing.Size(268, 82);
            this.GroupBoxLogowanie.TabIndex = 5;
            this.GroupBoxLogowanie.TabStop = false;
            this.GroupBoxLogowanie.Text = "Logowanie";
            // 
            // FormLogowanie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 134);
            this.Controls.Add(this.ButtonZaloguj);
            this.Controls.Add(this.GroupBoxLogowanie);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormLogowanie";
            this.Text = "EWO";
            this.Load += new System.EventHandler(this.FormLogowanie_Load);
            this.GroupBoxLogowanie.ResumeLayout(false);
            this.GroupBoxLogowanie.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxUzytkownik;
        private System.Windows.Forms.TextBox TextBoxHaslo;
        private System.Windows.Forms.Button ButtonZaloguj;
        private System.Windows.Forms.Label LabelUzytkownik;
        private System.Windows.Forms.Label LabelHaslo;
        private System.Windows.Forms.GroupBox GroupBoxLogowanie;
    }
}