﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EWO
{
    public partial class FormStatystyki : Form
    {
        public FormStatystyki()
        {
            InitializeComponent();
        }

        EWODataContext ewo = new EWODataContext();

        private void FormStatystyki_Load(object sender, EventArgs e)
        {
            this.Icon = EWO.Ikonka;

            ComboBoxListaLat.DataSource = ewo.funListaLat(EWO.Uzytkownik).Select(r => new { r.Rok }.Rok);
            ComboBoxListaFunkcji.DataSource = ewo.funListaFunkcji().Select(f => new { f.Funkcja }.Funkcja);
            ComboBoxListaLat.SelectedItem = ComboBoxListaLat.Items[0];
            ComboBoxListaFunkcji.SelectedItem = ComboBoxListaFunkcji.Items[0];
            RadioButtonW.Checked = true;
            RadioButtonP.Checked = false;
        }

        private void OdswiezDane(object sender, EventArgs e)
        {
            if (RadioButtonW.Checked)
                DataGridStatystyki.DataSource = ewo.funStatystyki(EWO.Uzytkownik, "wydatek", int.Parse(ComboBoxListaLat.Text), ComboBoxListaFunkcji.Text);
            else
                DataGridStatystyki.DataSource = ewo.funStatystyki(EWO.Uzytkownik, "przychód", int.Parse(ComboBoxListaLat.Text), ComboBoxListaFunkcji.Text);

            DataGridStatystyki.Columns[0].Width = (DataGridStatystyki.Width / 18) + 13;

            for(int i=1; i<= 17; i++)
            {
                DataGridStatystyki.Columns[i].Width = (DataGridStatystyki.Width - 10) / 18;
                DataGridStatystyki.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }

            DataGridStatystyki.Columns[0].DefaultCellStyle.BackColor = Color.Orange;
            DataGridStatystyki.Columns[4].DefaultCellStyle.BackColor = Color.OrangeRed;
            DataGridStatystyki.Columns[8].DefaultCellStyle.BackColor = Color.OrangeRed;
            DataGridStatystyki.Columns[12].DefaultCellStyle.BackColor = Color.OrangeRed;
            DataGridStatystyki.Columns[16].DefaultCellStyle.BackColor = Color.OrangeRed;
            DataGridStatystyki.Columns[17].DefaultCellStyle.BackColor = Color.IndianRed;
        }


    }
}
