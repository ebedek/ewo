﻿namespace EWO
{
    partial class FormListaKat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComboBoxWybWariant = new System.Windows.Forms.ComboBox();
            this.LabelWybWariant = new System.Windows.Forms.Label();
            this.ButtonWybWariant = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ComboBoxWybWariant
            // 
            this.ComboBoxWybWariant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxWybWariant.FormattingEnabled = true;
            this.ComboBoxWybWariant.Location = new System.Drawing.Point(21, 43);
            this.ComboBoxWybWariant.Name = "ComboBoxWybWariant";
            this.ComboBoxWybWariant.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxWybWariant.TabIndex = 0;
            // 
            // LabelWybWariant
            // 
            this.LabelWybWariant.AutoSize = true;
            this.LabelWybWariant.Location = new System.Drawing.Point(18, 9);
            this.LabelWybWariant.Name = "LabelWybWariant";
            this.LabelWybWariant.Size = new System.Drawing.Size(131, 13);
            this.LabelWybWariant.TabIndex = 1;
            this.LabelWybWariant.Text = "Wybierz kategorię / rodzaj";
            // 
            // ButtonWybWariant
            // 
            this.ButtonWybWariant.Location = new System.Drawing.Point(172, 40);
            this.ButtonWybWariant.Name = "ButtonWybWariant";
            this.ButtonWybWariant.Size = new System.Drawing.Size(75, 23);
            this.ButtonWybWariant.TabIndex = 2;
            this.ButtonWybWariant.Text = "OK";
            this.ButtonWybWariant.UseVisualStyleBackColor = true;
            this.ButtonWybWariant.Click += new System.EventHandler(this.ButtonWybWariant_Click);
            // 
            // FormListaKat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 91);
            this.Controls.Add(this.ButtonWybWariant);
            this.Controls.Add(this.LabelWybWariant);
            this.Controls.Add(this.ComboBoxWybWariant);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormListaKat";
            this.Text = "EWO";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormListaKat_FormClosing);
            this.Load += new System.EventHandler(this.FormListaKat_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ComboBoxWybWariant;
        private System.Windows.Forms.Label LabelWybWariant;
        private System.Windows.Forms.Button ButtonWybWariant;
    }
}