﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.IO;
using System.Windows.Forms;

namespace EWO
{
    public class EmailT
    {
        public string SmtpSrvHost { get; set; }
        public int SmtpSrvPort { get; set; }
        public string SmtpLogin { get; set; }
        public string SmtpPassword { get; set; }
        public string MailFrom { get; set; }
        public string MailTo { get; set; }
        public string MailSubject { get; set; }
        public string MailBody { get; set; }
        public bool AttachXLSX { get; set; }
        public bool AttachPDF { get; set; }

        public void Wyslij()
        {
            MailMessage Wiadomosc = new MailMessage(EWO.wiadomosc.MailFrom, EWO.wiadomosc.MailTo, EWO.wiadomosc.MailSubject, EWO.wiadomosc.MailBody);
            SmtpClient Polaczenie = new SmtpClient(EWO.wiadomosc.SmtpSrvHost, Convert.ToInt16(EWO.wiadomosc.SmtpSrvPort));
            Polaczenie.UseDefaultCredentials = false;

            System.Net.NetworkCredential Logowanie = new System.Net.NetworkCredential(EWO.wiadomosc.SmtpLogin, EWO.wiadomosc.SmtpPassword);
            Polaczenie.Credentials = Logowanie;

            if (EWO.wiadomosc.AttachXLSX == true && File.Exists(EWO.ZalacznikiOdczyt("XLSX").Katalog+"\\EWO-export.xlsx"))
            {
                Attachment zalacznik2 = new Attachment(EWO.ZalacznikiOdczyt("XLSX").Katalog+"\\EWO-export.xlsx");
                Wiadomosc.Attachments.Add(zalacznik2);
            }

            if (EWO.wiadomosc.AttachPDF == true && File.Exists(EWO.ZalacznikiOdczyt("PDF").Katalog+"\\EWO-raport.pdf"))
            {
                Attachment zalacznik3 = new Attachment(EWO.ZalacznikiOdczyt("PDF").Katalog+"\\EWO-raport.pdf");
                Wiadomosc.Attachments.Add(zalacznik3);
            }

            try
            {
                Polaczenie.Send(Wiadomosc);
                MessageBox.Show("Pliki wysłane...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception err)
            {
                MessageBox.Show("Wystąpił bąd podczas wysyłania! Sprawdź połączenie sieciowe...\nSzczegółowe informacje: " +err.Message, "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public string SzyfrujHaslo(string odkodowane)
        {

            int[] noweslowo = new int[odkodowane.Length];
            string zakodowane = "";

            for (int i = 0; i < odkodowane.Length; i++)
            {
                noweslowo[i] = odkodowane[i] + 13;
                zakodowane += (char)noweslowo[i];
            }

            return zakodowane;
        }

        public string DeszyfrujHaslo(string zakodowane)
        {
            string odkodowane = "";

            for (int i = 0; i < zakodowane.Length; i++)
            {
                int tmp = zakodowane[i] - 13;
                odkodowane += (char)tmp;
            }

            return odkodowane;

        }
    }

}
