﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EWO
{
    public partial class FormWykres2 : Form
    {
        public FormWykres2()
        {
            InitializeComponent();
        }

        private void FormWykres2_Load(object sender, EventArgs e)
        {
            this.Icon = EWO.Ikonka;
            
            EWO.ListaMiesiecySort();

            chart2.Series[0].Name = "wydatek";
            chart2.Series[1].Name = "przychód";
            chart2.ChartAreas[0].AxisX.Interval = 1;

            EWODataContext ewo = new EWODataContext();

            double kwota2;

            for (int i = 0; i < EWO.ListaMiesiecyIlosc(); i++)
            {
                kwota2 = (double) ewo.funWykres2WszystkoRazem(EWO.Uzytkownik, "wydatek", EWO.MiesiacOdczyt(i));
                chart2.Series[0].Points.Add(kwota2);

                kwota2 = (double)ewo.funWykres2WszystkoRazem(EWO.Uzytkownik, "przychód", EWO.MiesiacOdczyt(i));
                chart2.Series[1].Points.Add(kwota2);

                chart2.Series[0].Points[i].AxisLabel = EWO.MiesiacOdczyt(i);
            }

        }
    }
}
