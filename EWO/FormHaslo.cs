﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EWO
{
    public partial class FormHaslo : Form
    {
        public FormHaslo()
        {
            InitializeComponent();
        }

        private void FormHaslo_Load(object sender, EventArgs e)
        {
            this.Icon = EWO.Ikonka;
        }

        private void ButtonZapisz_Click(object sender, EventArgs e)
        {
            if (TextBoxHaslo1.Text == TextBoxHaslo2.Text)
                EWO.HasloZmien(TextBoxHaslo1.Text);
            else
                MessageBox.Show("Niezgodne hasła...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
