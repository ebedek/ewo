﻿namespace EWO
{
    partial class FormWykres1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.FormR1GroupBox = new System.Windows.Forms.GroupBox();
            this.FormR1RadioButton1 = new System.Windows.Forms.RadioButton();
            this.FormR1RadioButton2 = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.FormR1GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Name = "Legend1";
            this.chart1.Legends.Add(legend2);
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(506, 380);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            title2.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            title2.Name = "Title1";
            this.chart1.Titles.Add(title2);
            // 
            // FormR1GroupBox
            // 
            this.FormR1GroupBox.BackColor = System.Drawing.SystemColors.Window;
            this.FormR1GroupBox.Controls.Add(this.FormR1RadioButton1);
            this.FormR1GroupBox.Controls.Add(this.FormR1RadioButton2);
            this.FormR1GroupBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormR1GroupBox.Location = new System.Drawing.Point(400, 292);
            this.FormR1GroupBox.Name = "FormR1GroupBox";
            this.FormR1GroupBox.Size = new System.Drawing.Size(94, 76);
            this.FormR1GroupBox.TabIndex = 4;
            this.FormR1GroupBox.TabStop = false;
            this.FormR1GroupBox.Text = "Operacje";
            // 
            // FormR1RadioButton1
            // 
            this.FormR1RadioButton1.AutoSize = true;
            this.FormR1RadioButton1.Location = new System.Drawing.Point(6, 19);
            this.FormR1RadioButton1.Name = "FormR1RadioButton1";
            this.FormR1RadioButton1.Size = new System.Drawing.Size(61, 17);
            this.FormR1RadioButton1.TabIndex = 1;
            this.FormR1RadioButton1.TabStop = true;
            this.FormR1RadioButton1.Text = "wydatki";
            this.FormR1RadioButton1.UseVisualStyleBackColor = true;
            this.FormR1RadioButton1.CheckedChanged += new System.EventHandler(this.FormR1RadioButton_CheckedChanged);
            // 
            // FormR1RadioButton2
            // 
            this.FormR1RadioButton2.AutoSize = true;
            this.FormR1RadioButton2.Location = new System.Drawing.Point(6, 42);
            this.FormR1RadioButton2.Name = "FormR1RadioButton2";
            this.FormR1RadioButton2.Size = new System.Drawing.Size(73, 17);
            this.FormR1RadioButton2.TabIndex = 2;
            this.FormR1RadioButton2.TabStop = true;
            this.FormR1RadioButton2.Text = "przychody";
            this.FormR1RadioButton2.UseVisualStyleBackColor = true;
            this.FormR1RadioButton2.CheckedChanged += new System.EventHandler(this.FormR1RadioButton_CheckedChanged);
            // 
            // FormRaport1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 380);
            this.Controls.Add(this.FormR1GroupBox);
            this.Controls.Add(this.chart1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(522, 418);
            this.Name = "FormRaport1";
            this.Text = "EWO";
            this.Load += new System.EventHandler(this.FormWykres1_Load);
            this.Resize += new System.EventHandler(this.FormWykres1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.FormR1GroupBox.ResumeLayout(false);
            this.FormR1GroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.GroupBox FormR1GroupBox;
        private System.Windows.Forms.RadioButton FormR1RadioButton1;
        private System.Windows.Forms.RadioButton FormR1RadioButton2;

    }
}