﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EWO
{
    public partial class FormHasloEmail : Form
    {
        public FormHasloEmail()
        {
            InitializeComponent();
        }

        private void ButtonOK_Click(object sender, EventArgs e)
        {

                if (TextBoxPassword.Text.Length > 0)
                {
                    EWO.wiadomosc.SmtpPassword = TextBoxPassword.Text;
                    EmailT emajl = new EmailT();
                    emajl.Wyslij();

                    FormHasloEmail.ActiveForm.Close();
                }
                else
                    MessageBox.Show("Podaj hasło...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void FormPassword_Load(object sender, EventArgs e)
        {
            this.Icon = EWO.Ikonka;
        }
    }
}
