﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;

namespace EWO
{
    public partial class Form1 : Form
    {

        int OperacjaWybor;
        DataGridView DataGridOperacje = new DataGridView();
        private StringReader sr = null;
        double KwotaWydatkow=0.00;
        double KwotaPrzychodow=0.00;
        

        public Form1()
        {
            InitializeComponent();

            EWO Glowna = new EWO();
        }

        private void UtworzDataGrid()
        {
            DataGridOperacje.MouseDoubleClick -= new MouseEventHandler(DataGridOperacje_DoubleClick);

            DataGridOperacje.Rows.Clear();
            DataGridOperacje.Columns.Clear();
            
            DataGridViewTextBoxColumn kolumna0 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn kolumna1 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn kolumna2 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn kolumna3 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn kolumna4 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn kolumna5 = new DataGridViewTextBoxColumn();

            DataGridOperacje.Columns.Add(kolumna0);
            DataGridOperacje.Columns.Add(kolumna1);
            DataGridOperacje.Columns.Add(kolumna2);
            DataGridOperacje.Columns.Add(kolumna3);
            DataGridOperacje.Columns.Add(kolumna4);
            DataGridOperacje.Columns.Add(kolumna5);

            kolumna0.HeaderText = "Id";
            kolumna1.HeaderText = "Działanie";
            kolumna2.HeaderText = "Towar";
            kolumna3.HeaderText = "Kwota";
            kolumna4.HeaderText = "Wariant operacji";
            kolumna5.HeaderText = "Data";

            DataGridOperacje.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridOperacje.Location = new Point(0, 0);
            DataGridOperacje.Size = new Size(TabPageOperacjeGlowna.Width, TabPageOperacjeGlowna.Height);
            
            DataGridOperacje.RowHeadersVisible = false;
            DataGridOperacje.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            DataGridOperacje.AllowUserToResizeRows = false;
            DataGridOperacje.AllowUserToResizeColumns = false;
            DataGridOperacje.AllowUserToAddRows = false;
            
            int SzerokoscDataGridOperacje = DataGridOperacje.Width;

            kolumna0.Width = Convert.ToInt16((SzerokoscDataGridOperacje*0.05));
            kolumna1.Width = Convert.ToInt16((SzerokoscDataGridOperacje*0.10));
            kolumna2.Width = Convert.ToInt16((SzerokoscDataGridOperacje*0.40));
            kolumna3.Width = Convert.ToInt16((SzerokoscDataGridOperacje*0.1));
            kolumna4.Width = Convert.ToInt16((SzerokoscDataGridOperacje*0.15));
            kolumna5.Width = Convert.ToInt16((SzerokoscDataGridOperacje*0.15));

            int SzerokoscKolumn = kolumna0.Width + kolumna1.Width + kolumna2.Width + kolumna3.Width + kolumna4.Width + kolumna5.Width;
            int SzerokoscRoznica = SzerokoscDataGridOperacje - SzerokoscKolumn -3;
            kolumna2.Width += SzerokoscRoznica;
            
            TabControlOperacje.SelectedTab.Controls.Add(DataGridOperacje);

            DataGridOperacje.ScrollBars = ScrollBars.Vertical;
            DataGridOperacje.MultiSelect = false;
            DataGridOperacje.ReadOnly = true;
            DataGridOperacje.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            
            DataGridOperacje.MouseDoubleClick += new MouseEventHandler(DataGridOperacje_DoubleClick);
        }

        private void WstawZakladke(string nowadata, bool czydodawacmsc)
        {
            string NowaData = nowadata;
            if (czydodawacmsc == true)
                EWO.ListaMiesiecyDodaj(NowaData);

            int NowaDataKonw = int.Parse(NowaData.Substring(0, 4) + NowaData.Substring(5, 2));

            bool CzyWstawionoZakladke = false;
            int m = 0;

            while (CzyWstawionoZakladke == false && m < TabControlOperacje.TabPages.Count)
            {
                int NazwaZakladkiKonw = int.Parse(TabControlOperacje.TabPages[m].Text.Remove(4, 1));

                if (NowaDataKonw > NazwaZakladkiKonw)
                {
                    TabControlOperacje.TabPages.Insert(m, NowaData, nowadata);
                    CzyWstawionoZakladke = true;
                }

                m++;
            }

            if (CzyWstawionoZakladke == false)
                TabControlOperacje.TabPages.Add(NowaData, NowaData);

        }

        private void OperacjaZmien()
        {
            EWO.OperacjaWprowadzono = false;
            string NowaData;
            
            try
            {
                OperacjaWybor = DataGridOperacje.CurrentRow.Index;
            }

            catch (NullReferenceException)
            {
                MessageBox.Show("Nie wybrano operacji", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            if (OperacjaWybor >= 0 && OperacjaWybor < EWO.OperacjeIlosc())
            {
                
                int OdczytaneIDGrid = int.Parse(DataGridOperacje[0, OperacjaWybor].Value.ToString());
                int ZnalezioneIDLista = EWO.OperacjaZnajdz(OdczytaneIDGrid);
                string StaraData = TabControlOperacje.SelectedTab.Text;

                TypyDzialanT Dzialanie = new TypyDzialanT(EWO.OperacjaOdczyt(ZnalezioneIDLista).TypDzialania.TypDzialania);
                Form FormOperacjaZmien = new FormOperacja(ZnalezioneIDLista, Dzialanie.TypDzialania, EWO.OperacjaOdczyt(ZnalezioneIDLista).Towar.Towar, EWO.OperacjaOdczyt(ZnalezioneIDLista).Kwota, EWO.ZnajdzTypTowaru(EWO.OperacjaOdczyt(ZnalezioneIDLista).Towar.Towar),EWO.OperacjaOdczyt(ZnalezioneIDLista).Data);
                double KwotaDoZmiany = EWO.OperacjaOdczyt(ZnalezioneIDLista).Kwota;
                TypyDzialanT DzialanieDoZmiany = new TypyDzialanT(EWO.OperacjaOdczyt(ZnalezioneIDLista).TypDzialania.TypDzialania);
                FormOperacjaZmien.ShowDialog();
                EWO.TowaryWczytaj();

                if (EWO.OperacjaWprowadzono == true)
                {
                    if (DzialanieDoZmiany.TypDzialania == "wydatek")
                        KwotaWydatkow -= KwotaDoZmiany;
                    else
                        KwotaPrzychodow -= KwotaDoZmiany;

                    NowaData = EWO.OperacjaSprawdzDate(EWO.OperacjaOdczyt(ZnalezioneIDLista).Id).Substring(0,7);

                    if (EWO.WybranyRokMsc == NowaData)
                    {
                        DataGridOperacje[0, OperacjaWybor].Value = EWO.OperacjaOdczyt(ZnalezioneIDLista).Id;
                        DataGridOperacje[1, OperacjaWybor].Value = EWO.OperacjaOdczyt(ZnalezioneIDLista).TypDzialania.TypDzialania;
                        DataGridOperacje[2, OperacjaWybor].Value = EWO.OperacjaOdczyt(ZnalezioneIDLista).Towar.Towar;
                        DataGridOperacje[3, OperacjaWybor].Value = string.Format("{0:0.00}", EWO.OperacjaOdczyt(ZnalezioneIDLista).Kwota);
                        DataGridOperacje[4, OperacjaWybor].Value = EWO.ZnajdzTypTowaru(EWO.OperacjaOdczyt(ZnalezioneIDLista).Towar.Towar);
                        DataGridOperacje[5, OperacjaWybor].Value = EWO.OperacjaOdczyt(ZnalezioneIDLista).Data.Substring(0,10);
                        
                        if (EWO.OperacjaOdczyt(ZnalezioneIDLista).TypDzialania.TypDzialania == "wydatek")
                            KwotaWydatkow += EWO.OperacjaOdczyt(ZnalezioneIDLista).Kwota;
                        else
                            KwotaPrzychodow += EWO.OperacjaOdczyt(ZnalezioneIDLista).Kwota;

                        if (EWO.OperacjaOdczyt(ZnalezioneIDLista).TypDzialania.TypDzialania == "przychód")
                            DataGridOperacje.Rows[OperacjaWybor].DefaultCellStyle.BackColor = Color.Bisque;
                        else DataGridOperacje.Rows[OperacjaWybor].DefaultCellStyle.BackColor = Color.AliceBlue;
                    }

                    else
                    {
                        DataGridOperacje.Rows.Remove(DataGridOperacje.Rows[OperacjaWybor]);
                        EWO.OperacjaUsunZListy(ZnalezioneIDLista);

                        int licz = 0;
                        for (int i = 0; i < EWO.ListaMiesiecyIlosc(); i++)
                        {
                            if (EWO.MiesiacOdczyt(i) == NowaData)
                                licz++;
                        }

                        if (licz == 0)
                        {
                            WstawZakladke(NowaData,true);

                            if (EWO.OperacjeIlosc() == 0)
                            {
                                EWO.ListaMiesiecyUsun(StaraData);
                                TabControlOperacje.TabPages.Remove(TabControlOperacje.TabPages[StaraData]);
                            }

                            TabControlOperacje.SelectTab(NowaData);
                        }

                        else
                        {
                            if (EWO.OperacjeIlosc() == 0)
                            {
                                EWO.ListaMiesiecyUsun(StaraData);
                                TabControlOperacje.TabPages.Remove(TabControlOperacje.TabPages[StaraData]);
                                TabControlOperacje.SelectTab(NowaData);
                            }

                        }
                    }
                }
            }
            else
                MessageBox.Show("Operacja spoza zakresu...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);

            TextBoxWydatkow.Text = string.Format("{0:0.00}", KwotaWydatkow);
            TextBoxPrzychodow.Text = string.Format("{0:0.00}", KwotaPrzychodow);
            TextBoxSaldo.Text = string.Format("{0:0.00}", KwotaPrzychodow - KwotaWydatkow);
        }

        private void TypyTowarowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form FormTypyTowarow = new FormTypyTowarow();
            FormTypyTowarow.ShowDialog();
            EWO.TypyDzialanWczytaj();
            EWO.TypyTowarowWczytaj();
            EWO.TowaryWczytaj();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            if (EWO.SprawdzPoziomUprawnien()!=0)
                UzytkownicyToolStripMenuItem.Visible = false;

            KwotaWydatkow = 0;
            KwotaPrzychodow = 0;

            if (EWO.ListaMiesiecyIlosc() > 0)
            {
                int licz=0;

                for (int i = 0; i < EWO.ListaMiesiecyIlosc(); i++)
                {
                    TabControlOperacje.TabPages.Add(EWO.MiesiacOdczyt(i),EWO.MiesiacOdczyt(i));

                    if (EWO.AktualnyRokMsc == EWO.MiesiacOdczyt(i))
                        licz++;
                }

                TabControlOperacje.TabPages.Remove(TabControlOperacje.TabPages[0]);

                if (licz == 0)
                    WstawZakladke(EWO.AktualnyRokMsc,false);
                
                TabControlOperacje.SelectTab(EWO.AktualnyRokMsc);

            }
            else
            {
                TabControlOperacje.TabPages[0].Text = EWO.AktualnyRokMsc;
                TabControlOperacje.TabPages[0].Name = EWO.AktualnyRokMsc;
                EWO.WybranyRokMsc = EWO.AktualnyRokMsc;
                EWO.ListaMiesiecyDodaj(EWO.AktualnyRokMsc);

                UtworzDataGrid();

                ButtonOperacjaSkasuj.Enabled = false;
                ButtonOperacjaZmien.Enabled = false;

                TextBoxWydatkow.Text = string.Format("{0:0.00}", KwotaWydatkow);
                TextBoxPrzychodow.Text = string.Format("{0:0.00}", KwotaPrzychodow);
                TextBoxSaldo.Text = string.Format("{0:0.00}", KwotaPrzychodow - KwotaWydatkow);

            }

        }

        private void ButtonOperacjaDodaj_Click(object sender, EventArgs e)
        {

            if (EWO.TypyTowarowIlosc(new TypyDzialanT("przychód")) == 0 || EWO.TypyTowarowIlosc(new TypyDzialanT("wydatek")) == 0)
            {
                MessageBox.Show("Proszę uzupełnić słowniki...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            else
            {
                EWO.OperacjaWprowadzono = false;
                Form FormOperacjaDodaj = new FormOperacja();
                FormOperacjaDodaj.ShowDialog();
                EWO.TowaryWczytaj();

                if (EWO.OperacjaWprowadzono == true)
                {
                    if (EWO.OperacjaOdczyt(EWO.OperacjeIlosc() - 1).Data.Substring(0, 7) == EWO.WybranyRokMsc)
                    {
                        DataGridOperacje.Rows.Add(EWO.OperacjeIloscWszystkie(), EWO.OperacjaOdczyt(EWO.OperacjeIlosc() - 1).TypDzialania.TypDzialania, EWO.OperacjaOdczyt(EWO.OperacjeIlosc() - 1).Towar.Towar, string.Format("{0:0.00}", EWO.OperacjaOdczyt(EWO.OperacjeIlosc() - 1).Kwota), EWO.OperacjaOdczyt(EWO.OperacjeIlosc() - 1).Towar.TypTowaru, EWO.OperacjaOdczyt(EWO.OperacjeIlosc() - 1).Data.Substring(0,10));
                        
                        if (EWO.OperacjaOdczyt(EWO.OperacjeIlosc() - 1).TypDzialania.TypDzialania == "wydatek")
                            KwotaWydatkow += EWO.OperacjaOdczyt(EWO.OperacjeIlosc() - 1).Kwota;
                        else
                            KwotaPrzychodow += EWO.OperacjaOdczyt(EWO.OperacjeIlosc() - 1).Kwota;
                        
                        if (EWO.OperacjeIlosc() >= 1)
                        {
                            if (EWO.OperacjaOdczyt(EWO.OperacjeIlosc() - 1).TypDzialania.TypDzialania == "przychód")
                                DataGridOperacje.Rows[EWO.OperacjeIlosc() - 1].DefaultCellStyle.BackColor = Color.Bisque;
                            else DataGridOperacje.Rows[EWO.OperacjeIlosc() - 1].DefaultCellStyle.BackColor = Color.AliceBlue;
                        }
                        
                        DataGridOperacje[1, EWO.OperacjeIlosc()-1].Selected = true;
                    }

                    else
                    {
                        string NowaData = EWO.OperacjaSprawdzDate(EWO.OperacjeIloscWszystkie()).Substring(0,7);
                        int licz = 0;
                        for (int i = 0; i < EWO.ListaMiesiecyIlosc(); i++)
                        {
                            if (EWO.MiesiacOdczyt(i) == NowaData)
                                licz++;
                        }

                        if (licz == 0)
                            WstawZakladke(NowaData,true);

                        TabControlOperacje.SelectTab(NowaData);
                    }

                    if (EWO.OperacjeIlosc() > 0)
                    {
                        ButtonOperacjaSkasuj.Enabled = true;
                        ButtonOperacjaZmien.Enabled = true;
                    }
                }

            }

            TextBoxWydatkow.Text = string.Format("{0:0.00}", KwotaWydatkow);
            TextBoxPrzychodow.Text = string.Format("{0:0.00}", KwotaPrzychodow);
            TextBoxSaldo.Text = string.Format("{0:0.00}", KwotaPrzychodow - KwotaWydatkow);
        }

        private void ButtonOperacjaZmien_Click(object sender, EventArgs e)
        {
            OperacjaZmien();
        }

        private void ButtonOperacjaSkasuj_Click(object sender, EventArgs e)
        {
            try
            {
                OperacjaWybor = DataGridOperacje.CurrentCell.RowIndex;
            }

            catch (NullReferenceException)
            {
                MessageBox.Show("Nie wybrano operacji", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

                if (OperacjaWybor >= 0 && OperacjaWybor < EWO.OperacjeIlosc())
                {
                    int OdczytaneIDGrid = int.Parse(DataGridOperacje[0, OperacjaWybor].Value.ToString());
                    int ZnalezioneIDLista = EWO.OperacjaZnajdz(OdczytaneIDGrid);
                    string MiesiacDoUsuniecia = EWO.OperacjaOdczyt(ZnalezioneIDLista).Data.Substring(0, 7);

                    if (MessageBox.Show("Potwierdź usunięcie operacji: id[" + EWO.OperacjaOdczyt(ZnalezioneIDLista).Id + "] - " + EWO.OperacjaOdczyt(ZnalezioneIDLista).Towar.Towar + " z " + EWO.OperacjaOdczyt(ZnalezioneIDLista).Data.Substring(0,10), "EWO", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        if (EWO.OperacjeIlosc() > 0)
                        {
                            try
                            {
                                if (EWO.OperacjaOdczyt(ZnalezioneIDLista).TypDzialania.TypDzialania == "wydatek")
                                    KwotaWydatkow -= EWO.OperacjaOdczyt(ZnalezioneIDLista).Kwota;
                                else
                                    KwotaPrzychodow -= EWO.OperacjaOdczyt(ZnalezioneIDLista).Kwota;

                                EWO.OperacjaUsun(OdczytaneIDGrid);
                                MessageBox.Show("Operacja usunięta...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                DataGridOperacje.Rows.Remove(DataGridOperacje.Rows[OperacjaWybor]);

                            }

                            catch
                            {
                                MessageBox.Show("Błąd w czasie usuwania operacji!", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                        }

                        if (EWO.OperacjeIlosc() == 0 && EWO.ListaMiesiecyIlosc() > 1)
                        {
                                TabControlOperacje.TabPages.Remove(TabControlOperacje.SelectedTab);
                                EWO.ListaMiesiecyUsun(MiesiacDoUsuniecia);
                        }

                        if (EWO.OperacjeIlosc() == 0)
                        {
                            ButtonOperacjaSkasuj.Enabled = false;
                            ButtonOperacjaZmien.Enabled = false;
                        }
                    }

                }
                else
                    MessageBox.Show("Operacja spoza zakresu...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);

            TextBoxWydatkow.Text = string.Format("{0:0.00}", KwotaWydatkow);
            TextBoxPrzychodow.Text = string.Format("{0:0.00}", KwotaPrzychodow);
            TextBoxSaldo.Text = string.Format("{0:0.00}", KwotaPrzychodow - KwotaWydatkow);
        }

        private void oProgramieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form AboutBox = new AboutBox();
            AboutBox.ShowDialog();
        }

        private void TabControlOperacje_Selected(object sender, TabControlEventArgs e)
        {
            DataGridOperacje.SuspendLayout();

            KwotaWydatkow = 0;
            KwotaPrzychodow = 0;

            DataGridOperacje.CellMouseDoubleClick += new DataGridViewCellMouseEventHandler(DataGridOperacje_DoubleClick);

            UtworzDataGrid();

            EWO.WybranyRokMsc = TabControlOperacje.SelectedTab.Text;
            EWO.OperacjeWczytaj(int.Parse(EWO.WybranyRokMsc.Substring(0, 4)),int.Parse(EWO.WybranyRokMsc.Substring(5, 2)));

            for (int i = 0; i < EWO.OperacjeIlosc(); i++)
            {
                

                DataGridOperacje.Rows.Add(EWO.OperacjaOdczyt(i).Id,EWO.OperacjaOdczyt(i).TypDzialania.TypDzialania, EWO.OperacjaOdczyt(i).Towar.Towar, string.Format("{0:0.00}", EWO.OperacjaOdczyt(i).Kwota), EWO.OperacjaOdczyt(i).Towar.TypTowaru, EWO.OperacjaOdczyt(i).Data.Substring(0,10));

                if (EWO.OperacjaOdczyt(i).TypDzialania.TypDzialania == "wydatek")
                    KwotaWydatkow += EWO.OperacjaOdczyt(i).Kwota;
                else
                    KwotaPrzychodow += EWO.OperacjaOdczyt(i).Kwota;

                if (EWO.OperacjaOdczyt(i).TypDzialania.TypDzialania == "przychód")
                    DataGridOperacje.Rows[i].DefaultCellStyle.BackColor = Color.Bisque;
                else DataGridOperacje.Rows[i].DefaultCellStyle.BackColor = Color.AliceBlue;
            }

            if (EWO.OperacjeIlosc() == 0)
            {
                ButtonOperacjaSkasuj.Enabled = false;
                ButtonOperacjaZmien.Enabled = false;
            }
            else
            {
                ButtonOperacjaSkasuj.Enabled = true;
                ButtonOperacjaZmien.Enabled = true;
            }

            TextBoxWydatkow.Text = string.Format("{0:0.00}", KwotaWydatkow);
            TextBoxPrzychodow.Text = string.Format("{0:0.00}", KwotaPrzychodow);
            TextBoxSaldo.Text = string.Format("{0:0.00}", KwotaPrzychodow - KwotaWydatkow);

            DataGridOperacje.ResumeLayout();
        }

        private void DataGridOperacje_DoubleClick(object sender, EventArgs e)
        {
            OperacjaZmien();
        }

        private void DrukUstawieniaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PageSetupDialog.ShowDialog();
        }

        private void PrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

            string zestawienie = "Dane z aplikacji EWO - wydruk z miesiąca " + EWO.WybranyRokMsc.ToString();
            zestawienie += Environment.NewLine;
            zestawienie += "Id\tTowar\t\t   Kwota\t\tData\t\tDziałanie\tWariant";
            zestawienie += Environment.NewLine;

            for (int i = 0; i < EWO.OperacjeIlosc(); i++)
            {
                zestawienie += EWO.OperacjaOdczyt(i).Id.ToString() + "\t";

                if (EWO.OperacjaOdczyt(i).Towar.Towar.Length < 8)
                    zestawienie += EWO.OperacjaOdczyt(i).Towar.Towar.ToString() + "\t\t";
                else
                    zestawienie += EWO.OperacjaOdczyt(i).Towar.Towar.ToString() + "\t";

                string kwota = string.Format("{0:0.00}", EWO.OperacjaOdczyt(i).Kwota);
                if (kwota.Length == 4)
                    zestawienie += "      " + kwota + "\t\t";
                else if (kwota.Length == 5)
                    zestawienie += "    " + kwota + "\t\t";
                else if (kwota.Length == 6)
                    zestawienie += "  " + kwota + "\t\t";
                else
                    zestawienie += kwota + "\t\t";

                zestawienie += EWO.OperacjaOdczyt(i).Data.ToString().Substring(0,10) + "\t";

                if (EWO.OperacjaOdczyt(i).TypDzialania.TypDzialania == EWO.DzialanieOdczyt(0))
                    zestawienie += EWO.OperacjaOdczyt(i).TypDzialania.TypDzialania + "\t\t";
                else
                    zestawienie += EWO.OperacjaOdczyt(i).TypDzialania.TypDzialania + "\t";

                zestawienie += EWO.OperacjaOdczyt(i).Towar.TypTowaru.ToString();
                zestawienie += Environment.NewLine;
            }

            System.Drawing.Font czcionka = DataGridOperacje.Font;
            int wysokoscWiersza = (int)czcionka.GetHeight(e.Graphics);
            int iloscLinii = e.MarginBounds.Height / wysokoscWiersza;

            if (sr == null) sr = new StringReader(zestawienie);
            e.HasMorePages = true;

            for (int i = 0; i < iloscLinii; i++)
            {
                string wiersz = sr.ReadLine();
                if (wiersz == null)
                {
                    e.HasMorePages = false;
                    sr = null;
                    break;
                }
                e.Graphics.DrawString(wiersz, czcionka, Brushes.Black, 3, 3+i*wysokoscWiersza);

            }

        }

        private void DrukToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (PrintDialog.ShowDialog() == DialogResult.OK)
            {
                PrintDocument.DocumentName = "EWO";
                PrintDocument.Print();
            }
        }

        private void DrukPodgladToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrintPreviewDialog.ShowDialog();
        }

        private void KategorieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form FormRaport1 = new FormWykres1();
            FormRaport1.ShowDialog();
        }

        private void RazemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form FormRaport2 = new FormWykres2();
            FormRaport2.ShowDialog();
        }

        private void KategorieRazemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form FormRaport3 = new FormWykres3();
            FormRaport3.ShowDialog();
        }

        
        private void ExportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filenamexlsx = EWO.ZalacznikiOdczyt("XLSX").Katalog + "\\EWO-export.xlsx";

            try
            {
                Excel.Application xlsApplication = new Excel.Application();
                Excel.Workbook xlsBook;
                Excel.Worksheet xlsSheet;
                object m = Type.Missing;
                xlsApplication.Workbooks.Add(m);
                xlsBook = xlsApplication.Workbooks[1];
                //Worksheet.Add(Before,After,Count,Type)
                xlsSheet = (xlsBook.Worksheets.Add(m, m, m, m)) as Excel.Worksheet;
                xlsSheet.Name = "EWO-export";

                xlsSheet.Cells[1, 1] = "Id";
                xlsSheet.Cells[1, 2] = "Towar";
                xlsSheet.Cells[1, 3] = "Kwota";
                xlsSheet.Cells[1, 4] = "Data";
                xlsSheet.Cells[1, 5] = "Działanie";
                xlsSheet.Cells[1, 6] = "Wariant";

                EWO.OperacjeWczytaj(0, 0);

                Excel.Range zakreskwoty = xlsSheet.get_Range("C2", "C" + (EWO.OperacjeIlosc()+1).ToString());
                zakreskwoty.NumberFormat = "0.00";

                for (int i = 1; i <= EWO.OperacjeIlosc(); i++)
                {
                    xlsSheet.Cells[i+1, 1] = EWO.OperacjaOdczyt(i - 1).Id;
                    xlsSheet.Cells[i+1, 2] = EWO.OperacjaOdczyt(i - 1).Towar.Towar;
                    xlsSheet.Cells[i+1, 3] = EWO.OperacjaOdczyt(i - 1).Kwota;
                    xlsSheet.Cells[i+1, 4] = EWO.OperacjaOdczyt(i - 1).Data.Substring(0,10);
                    xlsSheet.Cells[i+1, 5] = EWO.OperacjaOdczyt(i - 1).TypDzialania.TypDzialania;
                    xlsSheet.Cells[i+1, 6] = EWO.OperacjaOdczyt(i - 1).Towar.TypTowaru;
                }

                Excel.Range dane = xlsSheet.get_Range("A1", "F" + (EWO.OperacjeIlosc() + 1).ToString());
                dane.Columns.AutoFit();
                
                foreach (Excel.Range kolumna in dane.Columns)
                {
                    kolumna.ColumnWidth = (double)kolumna.ColumnWidth + 3;
                }
                
                xlsBook.Worksheets[4].Delete();
                xlsBook.Worksheets[3].Delete();
                xlsBook.Worksheets[2].Delete();

                Excel.Range naglowek = xlsSheet.get_Range("A1","F1");
                //Range.AutoFilter(Field,Criteria1,XlAutoFilterOperator Operator, Criteria2,VisibleDropDown)
                naglowek.AutoFilter(1, m, Excel.XlAutoFilterOperator.xlAnd, m, true);
                //Workbook.SaveAs(Filename,FileFormat,Password,WriteResPassword,ReadOnlyRecommended,CreateBackup,XlSaveAsAccessMode AccessMode,ConflictResolution,AddToMru,TextCodepage,TextVisualLayout,Local)
                xlsBook.SaveAs(filenamexlsx, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, m, m, false, false, Excel.XlSaveAsAccessMode.xlNoChange, m, m, m, m, m);
                //Workbook.Close(SaveChanges,Filename,RouteWorkbook)
                xlsBook.Close(false, m, m);
                xlsApplication.Quit();

                

                if (MessageBox.Show("Plik XLXS został utworzony. Czy chcesz go otworzyć?", "EWO", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        Process.Start(filenamexlsx);
                    }
                    catch
                    {
                        MessageBox.Show("Problem z otwarciem pliku XLSX. Sprawdź czy masz zainstalowany odpowiedni program - http://filext.com/file-extension/XLSX", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }

            catch
            {
                MessageBox.Show("Problem z generowaniem pliku XLSX. Ta funkcja wymaga zainstalowania MS Office.", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void ParametryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form FormParametry = new FormParametry();
            FormParametry.ShowDialog();
        }

        private void WyslijToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (EWO.wiadomosc.SmtpPassword.Length == 0)
            {
                Form FormPassword = new FormHasloEmail();
                FormPassword.ShowDialog();
            }
            else
            {
                EmailT emajl = new EmailT();
                emajl.Wyslij();
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                if (EWO.MinimizeToTray == true)
                {
                    Hide();
                    NotifyIcon.Visible = true;
                    NotifyIcon.BalloonTipText = "EWO";
                    NotifyIcon.ShowBalloonTip(500);
                }

            }
        }

        private void NotifyIcon_Click(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void RaportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EWO.OperacjeWczytaj(0, 0);

            string folderpdf = EWO.ZalacznikiOdczyt("PDF").Katalog;
            string filenamepdf = folderpdf + "\\EWO-raport.pdf";
            try
            {
                BaseFont czcionka = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1250, BaseFont.NOT_EMBEDDED);

                Document pdfdocument = new Document(PageSize.A4, 15, 15, 15, 15);
                PdfWriter.GetInstance(pdfdocument, new FileStream(filenamepdf, FileMode.Create));
                pdfdocument.Open();

                //czcionka,rozmiar,(0-normal,1-bold,?-),kolor
                iTextSharp.text.Font fontblack = new iTextSharp.text.Font(czcionka, 14, 0, BaseColor.BLACK);
                iTextSharp.text.Font fontred = new iTextSharp.text.Font(czcionka, 14, 0, BaseColor.RED);
                iTextSharp.text.Font fontredbold = new iTextSharp.text.Font(czcionka, 14, 1, BaseColor.RED);
                iTextSharp.text.Font fontblue = new iTextSharp.text.Font(czcionka, 14, 0, BaseColor.BLUE);
                iTextSharp.text.Font fontbluebold = new iTextSharp.text.Font(czcionka, 14, 1, BaseColor.BLUE);
                iTextSharp.text.Font fontgreenbold = new iTextSharp.text.Font(czcionka, 14, 1, BaseColor.GREEN);
                iTextSharp.text.Font fontgraybold = new iTextSharp.text.Font(czcionka, 14, 1, BaseColor.GRAY);

                PdfPTable TabelaPoczatkowa = new PdfPTable(6);
                TabelaPoczatkowa.WidthPercentage = 100;

                Phrase info = new Phrase("Dane z aplikacji EWO - raport z dnia " + System.DateTime.Now, fontblack);
                PdfPCell krinfo = new PdfPCell(info);
                krinfo.Colspan = 5;
                krinfo.Border = 0;
                TabelaPoczatkowa.AddCell(krinfo);

                iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(EWO.FolderProgramu + "EWO.png");
                logo.ScalePercent(20f);
                PdfPCell krlogo = new PdfPCell(logo);
                krlogo.Border = 0;
                TabelaPoczatkowa.AddCell(krlogo);

                pdfdocument.Add(TabelaPoczatkowa);

                EWO.ListaMiesiecySort();

                double SumaKwotWydaneWszystkie = 0;
                double SumaKwotPrzychodWszystkie = 0;

                TypyDzialanT TD = new TypyDzialanT("wydatek");
                int IloscKatWydatek = EWO.TypyTowarowIlosc(TD);

                List<double> SumaKwotKatWydatkoWwszystkie = new List<double>();
                for (int ww = 0; ww < IloscKatWydatek; ww++)
                    SumaKwotKatWydatkoWwszystkie.Add(0.00);

                TD.TypDzialania = "przychód";
                int IloscRodzPrzychodow = EWO.TypyTowarowIlosc(TD);

                List<double> SumaKwotRodzPrzychodowWszystkie = new List<double>();
                for (int pp = 0; pp < IloscRodzPrzychodow; pp++)
                    SumaKwotRodzPrzychodowWszystkie.Add(0.00);

                for (int k = 0; k < EWO.ListaMiesiecyIlosc(); k++)
                {
                    PdfPTable TabelaMsc = new PdfPTable(6);
                    TabelaMsc.WidthPercentage = 100;

                    var SzerokoscKolumn = new[] { 13f, 102f, 26f, 39f, 39f, 39f };
                    TabelaMsc.SetWidths(SzerokoscKolumn);

                    string miesiac = EWO.MiesiacOdczyt(k);

                    Phrase msc = new Phrase(miesiac, fontgraybold);
                    PdfPCell krmsc = new PdfPCell(msc);
                    krmsc.Colspan = 6;
                    krmsc.Border = 0;
                    krmsc.HorizontalAlignment = 1; //0=do lewej, 1=wycentrowane, 2=do prawej
                    TabelaMsc.AddCell(krmsc);

                    double SumaKwotWydane = 0;
                    double SumaKwotPrzychod = 0;

                    List<double> SumaKwotKatWydatkow = new List<double>();
                    for (int w = 0; w < IloscKatWydatek; w++)
                        SumaKwotKatWydatkow.Add(0.00);

                    List<double> SumaKwotRodzPrzychodow = new List<double>();
                    for (int p = 0; p < IloscRodzPrzychodow; p++)
                        SumaKwotRodzPrzychodow.Add(0.00);

                    TabelaMsc.AddCell(new Phrase("Id", fontblack));
                    TabelaMsc.AddCell(new Phrase("Towar", fontblack));
                    TabelaMsc.AddCell(new Phrase("Kwota", fontblack));
                    TabelaMsc.AddCell(new Phrase("Data", fontblack));
                    TabelaMsc.AddCell(new Phrase("Działanie", fontblack));
                    TabelaMsc.AddCell(new Phrase("Wariant", fontblack));

                    for (int i = 0; i < EWO.OperacjeIlosc(); i++)
                    {
                        if (EWO.OperacjaOdczyt(i).Data.Contains(miesiac))
                        {
                            Phrase id = new Phrase(EWO.OperacjaOdczyt(i).Id.ToString(), fontblack);
                            PdfPCell krid = new PdfPCell(id);
                            krid.HorizontalAlignment = 2;
                            TabelaMsc.AddCell(krid);

                            TabelaMsc.AddCell(new Phrase(EWO.OperacjaOdczyt(i).Towar.Towar, fontblack));

                            Phrase kwota = new Phrase(string.Format("{0:0.00}", EWO.OperacjaOdczyt(i).Kwota), fontblack);
                            PdfPCell krkwota = new PdfPCell(kwota);
                            krkwota.HorizontalAlignment = 2;
                            TabelaMsc.AddCell(krkwota);

                            TabelaMsc.AddCell(new Phrase(EWO.OperacjaOdczyt(i).Data.Substring(0,10), fontblack));
                            TabelaMsc.AddCell(new Phrase(EWO.OperacjaOdczyt(i).TypDzialania.TypDzialania, fontblack));
                            TabelaMsc.AddCell(new Phrase(EWO.OperacjaOdczyt(i).Towar.TypTowaru, fontblack));

                            if (EWO.OperacjaOdczyt(i).TypDzialania.TypDzialania == "wydatek")
                                SumaKwotWydane += EWO.OperacjaOdczyt(i).Kwota;
                            if (EWO.OperacjaOdczyt(i).TypDzialania.TypDzialania == "przychód")
                                SumaKwotPrzychod += EWO.OperacjaOdczyt(i).Kwota;

                            TD.TypDzialania = "wydatek";
                            EWO.TypyTowarowPobierz(TD);

                            for (int w = 0; w < IloscKatWydatek; w++)
                            {
                                if (EWO.TypTowaruTmpOdczyt(w).TypTowaru == EWO.OperacjaOdczyt(i).Towar.TypTowaru)
                                    SumaKwotKatWydatkow[w] += EWO.OperacjaOdczyt(i).Kwota;
                            }

                            TD.TypDzialania = "przychód";
                            EWO.TypyTowarowPobierz(TD);

                            for (int p = 0; p < IloscRodzPrzychodow; p++)
                            {
                                if (EWO.TypTowaruTmpOdczyt(p).TypTowaru == EWO.OperacjaOdczyt(i).Towar.TypTowaru)
                                    SumaKwotRodzPrzychodow[p] += EWO.OperacjaOdczyt(i).Kwota;
                            }

                        }
                    }

                    TabelaMsc.SpacingBefore = 20f;
                    TabelaMsc.SpacingAfter = 30f;

                    TD.TypDzialania = "przychód";
                    EWO.TypyTowarowPobierz(TD);

                    for (int p = 0; p < IloscRodzPrzychodow; p++)
                    {
                        Phrase prp = new Phrase("Suma kwoty " + EWO.TypTowaruTmpOdczyt(p).TypTowaru + ": " + string.Format("{0:0.00}", SumaKwotRodzPrzychodow[p]), fontblue);
                        PdfPCell krprp = new PdfPCell(prp);
                        krprp.Colspan = 6;
                        krprp.Border = 0;
                        TabelaMsc.AddCell(krprp);
                        SumaKwotRodzPrzychodowWszystkie[p] += SumaKwotRodzPrzychodow[p];
                    }

                    Phrase pp = new Phrase("Suma kwoty przychodów: " + string.Format("{0:0.00}", SumaKwotPrzychod), fontbluebold);
                    PdfPCell krpp = new PdfPCell(pp);
                    krpp.Colspan = 6;
                    krpp.Border = 0;
                    TabelaMsc.AddCell(krpp);

                    SumaKwotPrzychodWszystkie += SumaKwotPrzychod;

                    TD.TypDzialania = "wydatek";
                    EWO.TypyTowarowPobierz(TD);

                    for (int w = 0; w < IloscKatWydatek; w++)
                    {
                        Phrase pkw = new Phrase("Suma kwoty " + EWO.TypTowaruTmpOdczyt(w).TypTowaru + ": " + string.Format("{0:0.00}", SumaKwotKatWydatkow[w]), fontred);
                        PdfPCell krpkw = new PdfPCell(pkw);
                        krpkw.Colspan = 6;
                        krpkw.Border = 0;
                        TabelaMsc.AddCell(krpkw);
                        SumaKwotKatWydatkoWwszystkie[w] += SumaKwotKatWydatkow[w];
                    }


                    Phrase pw = new Phrase("Suma kwoty wydatków: " + string.Format("{0:0.00}", SumaKwotWydane), fontredbold);
                    PdfPCell krpw = new PdfPCell(pw);
                    krpw.Colspan = 6;
                    krpw.Border = 0;
                    TabelaMsc.AddCell(krpw);

                    SumaKwotWydaneWszystkie += SumaKwotWydane;

                    Phrase bilans = new Phrase("Saldo: " + string.Format("{0:0.00}", (SumaKwotPrzychod - SumaKwotWydane)), fontgreenbold);
                    PdfPCell krbilans = new PdfPCell(bilans);
                    krbilans.Colspan = 6;
                    krbilans.Border = 0;
                    TabelaMsc.AddCell(krbilans);

                    pdfdocument.Add(TabelaMsc);

                }

                PdfPTable TabelaKoncowa = new PdfPTable(6);
                TabelaKoncowa.WidthPercentage = 100;

                TD.TypDzialania = "przychód";
                EWO.TypyTowarowPobierz(TD);

                for (int pw = 0; pw < IloscRodzPrzychodow; pw++)
                {
                    Phrase prpw = new Phrase("Suma kwoty ze wszystkich miesięcy dla kategorii " + EWO.TypTowaruTmpOdczyt(pw).TypTowaru + ": " + string.Format("{0:0.00}", SumaKwotRodzPrzychodowWszystkie[pw]), fontblue);
                    PdfPCell krprpw = new PdfPCell(prpw);
                    krprpw.Colspan = 6;
                    krprpw.Border = 0;
                    TabelaKoncowa.AddCell(krprpw);
                }

                TD.TypDzialania = "wydatek";
                EWO.TypyTowarowPobierz(TD);

                for (int ww = 0; ww < IloscKatWydatek; ww++)
                {
                    Phrase pkww = new Phrase("Suma kwoty ze wszystkich miesięcy dla kategorii " + EWO.TypTowaruTmpOdczyt(ww).TypTowaru + ": " + string.Format("{0:0.00}", SumaKwotKatWydatkoWwszystkie[ww]), fontred);
                    PdfPCell krpkww = new PdfPCell(pkww);
                    krpkww.Colspan = 6;
                    krpkww.Border = 0;
                    TabelaKoncowa.AddCell(krpkww);
                }

                Phrase kpw = new Phrase("Suma kwoty przychodów ze wszystkich miesięcy: " + string.Format("{0:0.00}", SumaKwotPrzychodWszystkie), fontbluebold);
                PdfPCell krkpw = new PdfPCell(kpw);
                krkpw.Colspan = 6;
                krkpw.Border = 0;
                TabelaKoncowa.AddCell(krkpw);

                Phrase kww = new Phrase("Suma kwoty wydatków ze wszystkich miesięcy: " + string.Format("{0:0.00}", SumaKwotWydaneWszystkie), fontredbold);
                PdfPCell krkww = new PdfPCell(kww);
                krkww.Colspan = 6;
                krkww.Border = 0;
                TabelaKoncowa.AddCell(krkww);

                Phrase bilansw = new Phrase("Saldo ze wszystkich miesięcy: " + string.Format("{0:0.00}", (SumaKwotPrzychodWszystkie - SumaKwotWydaneWszystkie)), fontgreenbold);
                PdfPCell krbilansw = new PdfPCell(bilansw);
                krbilansw.Colspan = 6;
                krbilansw.Border = 0;
                TabelaKoncowa.AddCell(krbilansw);

                pdfdocument.Add(TabelaKoncowa);

                pdfdocument.Close();

                MessageBox.Show("Plik PDF utworzony...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

            catch
            {
                MessageBox.Show("Problem z generowaniem pliku PDF.", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            try
            {
                Process.Start(filenamepdf);
            }
            catch
            {
                MessageBox.Show("Problem z otwarciem pliku PDF. Sprawdź czy masz zainstalowany odpowiedni program - http://filext.com/file-extension/PDF", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void ImportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImportZPliku();
        }

        private void ImportZPliku()
        {
            bool CzyAnulowano = false;
            string Zakladka = TabControlOperacje.SelectedTab.Text;

            if (EWO.PlikImportu.Length == 0)
            {
                OpenFileDialog zimportuj = new OpenFileDialog();
                zimportuj.Filter = "Plik bazodanowy (*.csv)|*.csv|Plik tekstowy (*.txt)|*.txt";
                zimportuj.Multiselect = false;

                if (zimportuj.ShowDialog() == DialogResult.Cancel)
                {
                    CzyAnulowano = true;
                }
                else
                {
                    EWO.PlikImportu = zimportuj.FileName;
                }

            }

            if (CzyAnulowano == false)
            {
                int IloscMiesiecy = EWO.ListaMiesiecyIlosc();

                Form FormImport = new FormImport();
                FormImport.ShowDialog();
                EWO.PlikImportu = "";

                for (int i = IloscMiesiecy; i < EWO.ListaMiesiecyIlosc(); i++)
                {
                    WstawZakladke(EWO.MiesiacOdczyt(i), false);
                }

                TabControlOperacje.SelectTab(EWO.AktualnyRokMsc);
                TabControlOperacje.SelectTab(Zakladka);
            }

        }
  
        private void TabControlOperacje_DragDrop(object sender, DragEventArgs e)
        {
            int howmanyfiles = 0;

            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            foreach (string file in files)
                howmanyfiles++;

            if (howmanyfiles > 1)
                MessageBox.Show("Za dużo plików... - " + howmanyfiles.ToString(), "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
            {
                EWO.PlikImportu = files[0];
                string fileext = EWO.PlikImportu.Substring(EWO.PlikImportu.LastIndexOf("."), EWO.PlikImportu.Length - EWO.PlikImportu.LastIndexOf("."));

                if (fileext == ".txt" || fileext == ".csv")
                {
                    ImportZPliku();
                }

                else
                {
                    MessageBox.Show("Nieobsługiwane rozszerzenie: " + fileext, "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void TabControlOperacje_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void SerweryPocztoweToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form FormSerweryPocztowe = new FormSerweryPocztowe();
            FormSerweryPocztowe.ShowDialog();
        }

        private void UzytkownicyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormUzytkownicy FormUzytkownicy = new FormUzytkownicy();
            FormUzytkownicy.ShowDialog();
        }

        private void SaldaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormSalda FormSalda = new FormSalda();
            FormSalda.ShowDialog();
        }

        private void StatystykiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormStatystyki FormStatystyki = new FormStatystyki();
            FormStatystyki.ShowDialog();
        }

    }
}
