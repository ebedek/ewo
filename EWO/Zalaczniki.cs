﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWO
{
    public class ZalacznikiT
    {
        public string TypZalacznika { get; set; }
        public string Katalog { get; set; }

        public ZalacznikiT(string typzalacznika, string katalog)
        {
            TypZalacznika = typzalacznika;
            Katalog = katalog;
        }
    }
}
