﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EWO
{
    public partial class FormSerweryPocztowe : Form
    {
        bool TrybEdycji = false;
        int WybranySerwer;
        string NazwaPrzyciskuDodawanie = "Zarejestruj dane serwera";
        string NazwaPrzyciskuEdycja = "Zmodyfikuj dane serwera";

        public FormSerweryPocztowe()
        {
            InitializeComponent();
        }

        private void FormSerweryPocztowe_Load(object sender, EventArgs e)
        {
            this.Icon = EWO.Ikonka;

            for (int i = 0; i < EWO.SerweryPocztoweIlosc(); i++)
            {
                ListBoxSerweryPocztowe.Items.Add(EWO.SerwerPocztowyOdczyt(i).SmtpHost + ":" + EWO.SerwerPocztowyOdczyt(i).SmtpPort.ToString());
            }

            ToolTipSerweryPocztoweTB.SetToolTip(TextBoxHost, "Kliknij dwukrotnie w celu wyczyszczenia...");
            ToolTipSerweryPocztoweTB.SetToolTip(TextBoxPort, "Kliknij dwukrotnie w celu wyczyszczenia...");
            ToolTipSerweryPocztoweLB.SetToolTip(ListBoxSerweryPocztowe, "Kliknij dwukrotnie w celu zmodyfikowania typu towaru...");
        }

        private void TextBoxDaneWyczysc()
        {
            ButtonSerwerPocztowyDodaj.Text = NazwaPrzyciskuDodawanie;
            TrybEdycji = false;
            TextBoxHost.Text = "";
            TextBoxPort.Text = "";
            TextBoxHost.Enabled = true;
        }

        private void ButtonSerwerPocztowyDodaj_Click(object sender, EventArgs e)
        {
            if (TrybEdycji == false)
            {
                if (TextBoxHost.Text.Length > 0 && TextBoxPort.Text.Length > 0)
                {
                    try
                    {
                        try
                        {
                            EWO.SerwerPocztowyDodaj(TextBoxHost.Text, int.Parse(TextBoxPort.Text));
                            ListBoxSerweryPocztowe.Items.Add(TextBoxHost.Text + ":" + TextBoxPort.Text);
                            TextBoxDaneWyczysc();
                        }

                        catch (FormatException error)
                        {
                            MessageBox.Show("Nieprawidłowy port!\nSzczegółowe informacje o błędzie: " + error.Message, "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }

                    }

                    catch (SqlException sqlerror)
                    {
                        MessageBox.Show("Błąd podczas wstawiania danych.\nSzczegółowe informacje o błędzie: " + sqlerror.Message, "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }

                else
                {
                    MessageBox.Show("Proszę uzupełnić dane!", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }

            else
            {
                if (TextBoxHost.Text.Length > 0 && TextBoxPort.Text.Length > 0)
                {
                    try
                    {
                        try
                        {
                            EWO.SerwerPocztowyZmien(TextBoxHost.Text, int.Parse(TextBoxPort.Text));
                            ListBoxSerweryPocztowe.Items[WybranySerwer]=TextBoxHost.Text + ":" + TextBoxPort.Text;
                            TextBoxDaneWyczysc();
                        }

                        catch (FormatException error)
                        {
                            MessageBox.Show("Nieprawidłowy port!\nSzczegółowe informacje o błędzie: " + error.Message, "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }

                    }

                    catch (SqlException sqlerror)
                    {
                        MessageBox.Show("Błąd podczas modyfikacji danych.\nSzczegółowe informacje o błędzie: " + sqlerror.Message, "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }

                else
                {
                    MessageBox.Show("Proszę uzupełnić dane!", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
        }

        private void ListBoxSerweryPocztowe_DoubleClick(object sender, EventArgs e)
        {
            WybranySerwer = ListBoxSerweryPocztowe.SelectedIndex;

            if (WybranySerwer < 0)
            {
                MessageBox.Show("Proszę wybrać serwer!", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            else
            {
                string SerwerPort = ListBoxSerweryPocztowe.Items[WybranySerwer].ToString();
                TextBoxHost.Text = SerwerPort.Substring(0, SerwerPort.IndexOf(":"));
                TextBoxPort.Text = SerwerPort.Substring(SerwerPort.IndexOf(":") + 1, SerwerPort.Length - SerwerPort.IndexOf(":") - 1);
                TrybEdycji = true;
                ButtonSerwerPocztowyDodaj.Text = NazwaPrzyciskuEdycja;
                TextBoxHost.Enabled = false;
            }
        }

        private void TextBoxHost_DoubleClick(object sender, EventArgs e)
        {
            TextBoxDaneWyczysc();
        }

        private void TextBoxPort_DoubleClick(object sender, EventArgs e)
        {
            TextBoxDaneWyczysc();
        }

        private void UsunDaneerweraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WybranySerwer = ListBoxSerweryPocztowe.SelectedIndex;

            string Serwer = ListBoxSerweryPocztowe.Items[WybranySerwer].ToString();
            string SerwerNazwa = Serwer.Substring(0, Serwer.IndexOf(":"));
            string SerwerPort = Serwer.Substring(Serwer.IndexOf(":") + 1, Serwer.Length - Serwer.IndexOf(":") - 1);

            if (WybranySerwer < 0)
                MessageBox.Show("Proszę wybrać serwer...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
            {
                try
                {
                    EWO.SerwerPocztowyUsun(SerwerNazwa,int.Parse(SerwerPort));
                    ListBoxSerweryPocztowe.Items.Remove(ListBoxSerweryPocztowe.Items[WybranySerwer].ToString());
                    TextBoxDaneWyczysc();
                }

                catch (SqlException error)
                {
                    MessageBox.Show("Błąd podczas usuwania danych - serwer został wykorzystany w bazie.\nSzczegółowe informacje o błędzie: " + error.Message, "EWO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
        }
    }
}
