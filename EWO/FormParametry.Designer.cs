﻿namespace EWO
{
    partial class FormParametry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CheckBoxMinimizeToTray = new System.Windows.Forms.CheckBox();
            this.ButtonZapisz = new System.Windows.Forms.Button();
            this.GroupBoxOgolne = new System.Windows.Forms.GroupBox();
            this.GroupBoxEmail = new System.Windows.Forms.GroupBox();
            this.ComboBoxHost = new System.Windows.Forms.ComboBox();
            this.LabelPassword = new System.Windows.Forms.Label();
            this.LabelLogin = new System.Windows.Forms.Label();
            this.LabelPort = new System.Windows.Forms.Label();
            this.LabelHost = new System.Windows.Forms.Label();
            this.LabelBody = new System.Windows.Forms.Label();
            this.LabelSubject = new System.Windows.Forms.Label();
            this.LabelTo = new System.Windows.Forms.Label();
            this.LabelFrom = new System.Windows.Forms.Label();
            this.CheckBoxPDF = new System.Windows.Forms.CheckBox();
            this.CheckBoxXLSX = new System.Windows.Forms.CheckBox();
            this.TextBoxPassword = new System.Windows.Forms.MaskedTextBox();
            this.TextBoxBody = new System.Windows.Forms.TextBox();
            this.TextBoxTo = new System.Windows.Forms.TextBox();
            this.TextBoxFrom = new System.Windows.Forms.TextBox();
            this.TextBoxSubject = new System.Windows.Forms.TextBox();
            this.TextBoxLogin = new System.Windows.Forms.TextBox();
            this.TextBoxPort = new System.Windows.Forms.TextBox();
            this.ToolTipPassword = new System.Windows.Forms.ToolTip(this.components);
            this.ContextMenuStripParametry = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItemUstawFoldery = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemZmienHaslo = new System.Windows.Forms.ToolStripMenuItem();
            this.GroupBoxOgolne.SuspendLayout();
            this.GroupBoxEmail.SuspendLayout();
            this.ContextMenuStripParametry.SuspendLayout();
            this.SuspendLayout();
            // 
            // CheckBoxMinimizeToTray
            // 
            this.CheckBoxMinimizeToTray.AutoSize = true;
            this.CheckBoxMinimizeToTray.Location = new System.Drawing.Point(19, 29);
            this.CheckBoxMinimizeToTray.Name = "CheckBoxMinimizeToTray";
            this.CheckBoxMinimizeToTray.Size = new System.Drawing.Size(154, 17);
            this.CheckBoxMinimizeToTray.TabIndex = 12;
            this.CheckBoxMinimizeToTray.Text = "Minimalizacja do zasobnika";
            this.CheckBoxMinimizeToTray.UseVisualStyleBackColor = true;
            // 
            // ButtonZapisz
            // 
            this.ButtonZapisz.Location = new System.Drawing.Point(661, 227);
            this.ButtonZapisz.Name = "ButtonZapisz";
            this.ButtonZapisz.Size = new System.Drawing.Size(75, 23);
            this.ButtonZapisz.TabIndex = 13;
            this.ButtonZapisz.Text = "Zapisz zmiany";
            this.ButtonZapisz.UseVisualStyleBackColor = true;
            this.ButtonZapisz.Click += new System.EventHandler(this.ButtonZapisz_Click);
            // 
            // GroupBoxOgolne
            // 
            this.GroupBoxOgolne.Controls.Add(this.CheckBoxMinimizeToTray);
            this.GroupBoxOgolne.Location = new System.Drawing.Point(556, 12);
            this.GroupBoxOgolne.Name = "GroupBoxOgolne";
            this.GroupBoxOgolne.Size = new System.Drawing.Size(180, 62);
            this.GroupBoxOgolne.TabIndex = 2;
            this.GroupBoxOgolne.TabStop = false;
            this.GroupBoxOgolne.Text = "Ogólne";
            // 
            // GroupBoxEmail
            // 
            this.GroupBoxEmail.Controls.Add(this.ComboBoxHost);
            this.GroupBoxEmail.Controls.Add(this.LabelPassword);
            this.GroupBoxEmail.Controls.Add(this.LabelLogin);
            this.GroupBoxEmail.Controls.Add(this.LabelPort);
            this.GroupBoxEmail.Controls.Add(this.LabelHost);
            this.GroupBoxEmail.Controls.Add(this.LabelBody);
            this.GroupBoxEmail.Controls.Add(this.LabelSubject);
            this.GroupBoxEmail.Controls.Add(this.LabelTo);
            this.GroupBoxEmail.Controls.Add(this.LabelFrom);
            this.GroupBoxEmail.Controls.Add(this.CheckBoxPDF);
            this.GroupBoxEmail.Controls.Add(this.CheckBoxXLSX);
            this.GroupBoxEmail.Controls.Add(this.TextBoxPassword);
            this.GroupBoxEmail.Controls.Add(this.TextBoxBody);
            this.GroupBoxEmail.Controls.Add(this.TextBoxTo);
            this.GroupBoxEmail.Controls.Add(this.TextBoxFrom);
            this.GroupBoxEmail.Controls.Add(this.TextBoxSubject);
            this.GroupBoxEmail.Controls.Add(this.TextBoxLogin);
            this.GroupBoxEmail.Controls.Add(this.TextBoxPort);
            this.GroupBoxEmail.Location = new System.Drawing.Point(12, 12);
            this.GroupBoxEmail.Name = "GroupBoxEmail";
            this.GroupBoxEmail.Size = new System.Drawing.Size(523, 235);
            this.GroupBoxEmail.TabIndex = 3;
            this.GroupBoxEmail.TabStop = false;
            this.GroupBoxEmail.Text = "E-mail";
            // 
            // ComboBoxHost
            // 
            this.ComboBoxHost.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxHost.FormattingEnabled = true;
            this.ComboBoxHost.Location = new System.Drawing.Point(372, 15);
            this.ComboBoxHost.Name = "ComboBoxHost";
            this.ComboBoxHost.Size = new System.Drawing.Size(133, 21);
            this.ComboBoxHost.TabIndex = 14;
            this.ComboBoxHost.SelectedIndexChanged += new System.EventHandler(this.ComboBoxHost_SelectedIndexChanged);
            // 
            // LabelPassword
            // 
            this.LabelPassword.AutoSize = true;
            this.LabelPassword.Location = new System.Drawing.Point(333, 96);
            this.LabelPassword.Name = "LabelPassword";
            this.LabelPassword.Size = new System.Drawing.Size(36, 13);
            this.LabelPassword.TabIndex = 30;
            this.LabelPassword.Text = "Hasło";
            // 
            // LabelLogin
            // 
            this.LabelLogin.AutoSize = true;
            this.LabelLogin.Location = new System.Drawing.Point(254, 70);
            this.LabelLogin.Name = "LabelLogin";
            this.LabelLogin.Size = new System.Drawing.Size(115, 13);
            this.LabelLogin.TabIndex = 29;
            this.LabelLogin.Text = "Nazwa użytkownika [*]";
            // 
            // LabelPort
            // 
            this.LabelPort.AutoSize = true;
            this.LabelPort.Location = new System.Drawing.Point(330, 45);
            this.LabelPort.Name = "LabelPort";
            this.LabelPort.Size = new System.Drawing.Size(26, 13);
            this.LabelPort.TabIndex = 28;
            this.LabelPort.Text = "Port";
            // 
            // LabelHost
            // 
            this.LabelHost.AutoSize = true;
            this.LabelHost.Location = new System.Drawing.Point(243, 19);
            this.LabelHost.Name = "LabelHost";
            this.LabelHost.Size = new System.Drawing.Size(126, 13);
            this.LabelHost.TabIndex = 27;
            this.LabelHost.Text = "Nazwa serwera SMTP [*]";
            // 
            // LabelBody
            // 
            this.LabelBody.AutoSize = true;
            this.LabelBody.Location = new System.Drawing.Point(7, 161);
            this.LabelBody.Name = "LabelBody";
            this.LabelBody.Size = new System.Drawing.Size(34, 13);
            this.LabelBody.TabIndex = 26;
            this.LabelBody.Text = "Treść";
            // 
            // LabelSubject
            // 
            this.LabelSubject.AutoSize = true;
            this.LabelSubject.Location = new System.Drawing.Point(7, 128);
            this.LabelSubject.Name = "LabelSubject";
            this.LabelSubject.Size = new System.Drawing.Size(50, 13);
            this.LabelSubject.TabIndex = 25;
            this.LabelSubject.Text = "Temat [*]";
            // 
            // LabelTo
            // 
            this.LabelTo.AutoSize = true;
            this.LabelTo.Location = new System.Drawing.Point(7, 96);
            this.LabelTo.Name = "LabelTo";
            this.LabelTo.Size = new System.Drawing.Size(56, 13);
            this.LabelTo.TabIndex = 24;
            this.LabelTo.Text = "Adresat [*]";
            // 
            // LabelFrom
            // 
            this.LabelFrom.AutoSize = true;
            this.LabelFrom.Location = new System.Drawing.Point(7, 19);
            this.LabelFrom.Name = "LabelFrom";
            this.LabelFrom.Size = new System.Drawing.Size(66, 13);
            this.LabelFrom.TabIndex = 23;
            this.LabelFrom.Text = "Nadawca [*]";
            // 
            // CheckBoxPDF
            // 
            this.CheckBoxPDF.AutoSize = true;
            this.CheckBoxPDF.Location = new System.Drawing.Point(427, 195);
            this.CheckBoxPDF.Name = "CheckBoxPDF";
            this.CheckBoxPDF.Size = new System.Drawing.Size(47, 17);
            this.CheckBoxPDF.TabIndex = 11;
            this.CheckBoxPDF.Text = "PDF";
            this.CheckBoxPDF.UseVisualStyleBackColor = true;
            // 
            // CheckBoxXLSX
            // 
            this.CheckBoxXLSX.AutoSize = true;
            this.CheckBoxXLSX.Location = new System.Drawing.Point(427, 172);
            this.CheckBoxXLSX.Name = "CheckBoxXLSX";
            this.CheckBoxXLSX.Size = new System.Drawing.Size(53, 17);
            this.CheckBoxXLSX.TabIndex = 10;
            this.CheckBoxXLSX.Text = "XLSX";
            this.CheckBoxXLSX.UseVisualStyleBackColor = true;
            // 
            // TextBoxPassword
            // 
            this.TextBoxPassword.Location = new System.Drawing.Point(372, 93);
            this.TextBoxPassword.Name = "TextBoxPassword";
            this.TextBoxPassword.Size = new System.Drawing.Size(133, 20);
            this.TextBoxPassword.TabIndex = 8;
            this.TextBoxPassword.UseSystemPasswordChar = true;
            // 
            // TextBoxBody
            // 
            this.TextBoxBody.Location = new System.Drawing.Point(79, 161);
            this.TextBoxBody.Multiline = true;
            this.TextBoxBody.Name = "TextBoxBody";
            this.TextBoxBody.Size = new System.Drawing.Size(277, 51);
            this.TextBoxBody.TabIndex = 4;
            // 
            // TextBoxTo
            // 
            this.TextBoxTo.Location = new System.Drawing.Point(79, 93);
            this.TextBoxTo.Name = "TextBoxTo";
            this.TextBoxTo.Size = new System.Drawing.Size(158, 20);
            this.TextBoxTo.TabIndex = 2;
            this.TextBoxTo.Leave += new System.EventHandler(this.TextBoxTo_Leave);
            // 
            // TextBoxFrom
            // 
            this.TextBoxFrom.Location = new System.Drawing.Point(79, 16);
            this.TextBoxFrom.Name = "TextBoxFrom";
            this.TextBoxFrom.Size = new System.Drawing.Size(158, 20);
            this.TextBoxFrom.TabIndex = 1;
            this.TextBoxFrom.Leave += new System.EventHandler(this.TextBoxFrom_Leave);
            // 
            // TextBoxSubject
            // 
            this.TextBoxSubject.Location = new System.Drawing.Point(79, 125);
            this.TextBoxSubject.Name = "TextBoxSubject";
            this.TextBoxSubject.Size = new System.Drawing.Size(277, 20);
            this.TextBoxSubject.TabIndex = 3;
            // 
            // TextBoxLogin
            // 
            this.TextBoxLogin.Location = new System.Drawing.Point(372, 67);
            this.TextBoxLogin.Name = "TextBoxLogin";
            this.TextBoxLogin.Size = new System.Drawing.Size(133, 20);
            this.TextBoxLogin.TabIndex = 7;
            // 
            // TextBoxPort
            // 
            this.TextBoxPort.Enabled = false;
            this.TextBoxPort.Location = new System.Drawing.Point(372, 42);
            this.TextBoxPort.Name = "TextBoxPort";
            this.TextBoxPort.Size = new System.Drawing.Size(133, 20);
            this.TextBoxPort.TabIndex = 6;
            this.TextBoxPort.Leave += new System.EventHandler(this.TextBoxPort_Leave);
            // 
            // ContextMenuStripParametry
            // 
            this.ContextMenuStripParametry.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemUstawFoldery,
            this.ToolStripMenuItemZmienHaslo});
            this.ContextMenuStripParametry.Name = "ContextMenuStripParametry";
            this.ContextMenuStripParametry.Size = new System.Drawing.Size(153, 70);
            // 
            // ToolStripMenuItemUstawFoldery
            // 
            this.ToolStripMenuItemUstawFoldery.Name = "ToolStripMenuItemUstawFoldery";
            this.ToolStripMenuItemUstawFoldery.Size = new System.Drawing.Size(152, 22);
            this.ToolStripMenuItemUstawFoldery.Text = "Ustaw foldery";
            this.ToolStripMenuItemUstawFoldery.Click += new System.EventHandler(this.ToolStripMenuItemUstawFoldery_Click);
            // 
            // ToolStripMenuItemZmienHaslo
            // 
            this.ToolStripMenuItemZmienHaslo.Name = "ToolStripMenuItemZmienHaslo";
            this.ToolStripMenuItemZmienHaslo.Size = new System.Drawing.Size(152, 22);
            this.ToolStripMenuItemZmienHaslo.Text = "Zmień hasło";
            this.ToolStripMenuItemZmienHaslo.Click += new System.EventHandler(this.ToolStripMenuItemZmienHaslo_Click);
            // 
            // FormParametry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(755, 262);
            this.ContextMenuStrip = this.ContextMenuStripParametry;
            this.Controls.Add(this.GroupBoxEmail);
            this.Controls.Add(this.GroupBoxOgolne);
            this.Controls.Add(this.ButtonZapisz);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormParametry";
            this.Text = "EWO";
            this.Load += new System.EventHandler(this.FormParametry_Load);
            this.GroupBoxOgolne.ResumeLayout(false);
            this.GroupBoxOgolne.PerformLayout();
            this.GroupBoxEmail.ResumeLayout(false);
            this.GroupBoxEmail.PerformLayout();
            this.ContextMenuStripParametry.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonZapisz;
        private System.Windows.Forms.CheckBox CheckBoxMinimizeToTray;
        private System.Windows.Forms.GroupBox GroupBoxOgolne;
        private System.Windows.Forms.GroupBox GroupBoxEmail;
        private System.Windows.Forms.CheckBox CheckBoxPDF;
        private System.Windows.Forms.CheckBox CheckBoxXLSX;
        private System.Windows.Forms.MaskedTextBox TextBoxPassword;
        private System.Windows.Forms.TextBox TextBoxBody;
        private System.Windows.Forms.TextBox TextBoxTo;
        private System.Windows.Forms.TextBox TextBoxFrom;
        private System.Windows.Forms.TextBox TextBoxSubject;
        private System.Windows.Forms.TextBox TextBoxLogin;
        private System.Windows.Forms.TextBox TextBoxPort;
        private System.Windows.Forms.Label LabelPassword;
        private System.Windows.Forms.Label LabelLogin;
        private System.Windows.Forms.Label LabelPort;
        private System.Windows.Forms.Label LabelHost;
        private System.Windows.Forms.Label LabelBody;
        private System.Windows.Forms.Label LabelSubject;
        private System.Windows.Forms.Label LabelTo;
        private System.Windows.Forms.Label LabelFrom;
        private System.Windows.Forms.ToolTip ToolTipPassword;
        private System.Windows.Forms.ComboBox ComboBoxHost;
        private System.Windows.Forms.ContextMenuStrip ContextMenuStripParametry;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemUstawFoldery;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemZmienHaslo;
    }
}