﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EWO
{
    public partial class FormSalda : Form
    {
        public FormSalda()
        {
            InitializeComponent();
        }

        private void FormSalda_Load(object sender, EventArgs e)
        {
            this.Icon = EWO.Ikonka;

            EWODataContext ewo = new EWODataContext();

            DataGridSalda.DataSource = ewo.funSalda(EWO.Uzytkownik);

            DataGridSalda.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridSalda.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridSalda.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridSalda.Columns[0].DefaultCellStyle.BackColor = Color.Orange;
            DataGridSalda.Columns[3].DefaultCellStyle.BackColor = Color.IndianRed;

        }
    }
}
