﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace EWO
{
    public partial class FormLogowanie : Form
    {
        public FormLogowanie()
        {
            InitializeComponent();
        }

        private void ButtonZaloguj_Click(object sender, EventArgs e)
        {

            EWODataContext ewo = new EWODataContext();
            string Haslo="";

            try
            {
                Haslo = ewo.Uzytkownicy.Where(uz => uz.Uzytkownik == TextBoxUzytkownik.Text).Select(uzyt => uzyt.Haslo).SingleOrDefault();
            }

            catch (Exception err)
            {
                MessageBox.Show("Problem z połączeniem do bazy.\nSzczegółowe informacje: " + err, "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            try
            {
                if (EWO.Zakoduj(TextBoxHaslo.Text) == Haslo)
                {
                    EWO.Uzytkownik = TextBoxUzytkownik.Text;
                    Form1 Form1 = new Form1();
                    this.Hide();
                    Form1.ShowDialog();
                    this.Close();
                }
                else
                    MessageBox.Show("Nieprawidłowe dane...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            catch (Exception err)
            {
                MessageBox.Show("Wystąpił błąd.\nSzczegółowe informacje: " + err.Message, "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void FormLogowanie_Load(object sender, EventArgs e)
        {
            this.Icon = EWO.Ikonka;
        }


    }
}
