﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWO
{
    public class UzytkownicyT
    {
        public string Nazwa { get; set; }
        public string Haslo { get; set; }
        public string SmtpHost { get; set; }
        public string SmtpLogin { get; set; }
        public string MailFrom { get; set; }
        public string MailTo { get; set; }
        public string MailSubject { get; set; }
        public string MailBody { get; set; }
        public bool MinimizeToTray { get; set; }
        public string FolderPDF { get; set; }
        public string FolderXLSX { get; set; }
        public Int16 PoziomUprawnien { get; set; }

        public UzytkownicyT(string uzytkownik, string haslo, string smtphost, string smtplogin, string mailfrom, string mailto, string mailsubject, string mailbody, bool minimizetotray, string folderpdf, string folderxlsx, Int16 poziomuprawnien)
        {
            Nazwa = uzytkownik;
            Haslo = haslo;
            SmtpHost = smtphost;
            SmtpLogin = smtplogin;
            MailFrom = mailfrom;
            MailTo = mailto;
            MailSubject = mailsubject;
            MailBody = mailbody;
            MinimizeToTray = minimizetotray;
            FolderPDF = folderpdf;
            FolderXLSX = folderxlsx;
            PoziomUprawnien = poziomuprawnien;
        }

    }
}
