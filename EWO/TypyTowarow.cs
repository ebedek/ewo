﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWO
{
    public class TypyTowarowT
    {
        public string TypTowaru { get; set; }
        public string TypDzialania { get; set; }

        public TypyTowarowT(string typtowaru, string typdzialania)
        {
            TypTowaru = typtowaru;
            TypDzialania = typdzialania;
        }
    }
}
