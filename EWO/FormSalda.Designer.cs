﻿namespace EWO
{
    partial class FormSalda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataGridSalda = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSalda)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridSalda
            // 
            this.DataGridSalda.AllowUserToAddRows = false;
            this.DataGridSalda.AllowUserToDeleteRows = false;
            this.DataGridSalda.AllowUserToResizeColumns = false;
            this.DataGridSalda.AllowUserToResizeRows = false;
            this.DataGridSalda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSalda.Location = new System.Drawing.Point(12, 12);
            this.DataGridSalda.Name = "DataGridSalda";
            this.DataGridSalda.ReadOnly = true;
            this.DataGridSalda.RowHeadersVisible = false;
            this.DataGridSalda.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DataGridSalda.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridSalda.Size = new System.Drawing.Size(403, 303);
            this.DataGridSalda.TabIndex = 0;
            // 
            // FormSalda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 327);
            this.Controls.Add(this.DataGridSalda);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(434, 355);
            this.Name = "FormSalda";
            this.Text = "EWO";
            this.Load += new System.EventHandler(this.FormSalda_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSalda)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridSalda;

    }
}