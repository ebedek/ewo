﻿namespace EWO
{
    partial class FormUzytkownicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TextBoxUzytkownik = new System.Windows.Forms.TextBox();
            this.TextBoxHaslo2 = new System.Windows.Forms.TextBox();
            this.TextBoxSmtpLogin = new System.Windows.Forms.TextBox();
            this.TextBoxMailTo = new System.Windows.Forms.TextBox();
            this.TextBoxMailFrom = new System.Windows.Forms.TextBox();
            this.LabelUzytkownik = new System.Windows.Forms.Label();
            this.LabelSmtpHost = new System.Windows.Forms.Label();
            this.LabelHaslo2 = new System.Windows.Forms.Label();
            this.LabelSmtpLogin = new System.Windows.Forms.Label();
            this.LabelMailFrom = new System.Windows.Forms.Label();
            this.LabelMailTo = new System.Windows.Forms.Label();
            this.LabelSubject = new System.Windows.Forms.Label();
            this.TextBoxSubject = new System.Windows.Forms.TextBox();
            this.LabelBody = new System.Windows.Forms.Label();
            this.TextBoxBody = new System.Windows.Forms.TextBox();
            this.LabelMinimizeToTray = new System.Windows.Forms.Label();
            this.LabelFolderXLSX = new System.Windows.Forms.Label();
            this.LabelFolderPDF = new System.Windows.Forms.Label();
            this.TextBoxFolderXLSX = new System.Windows.Forms.TextBox();
            this.TextBoxFolderPDF = new System.Windows.Forms.TextBox();
            this.ButtonDodaj = new System.Windows.Forms.Button();
            this.ComboBoxSmtpHost = new System.Windows.Forms.ComboBox();
            this.CheckBoxMinimizeToTray = new System.Windows.Forms.CheckBox();
            this.ContextMenuStripUzytkownicy = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItemUstawFoldery = new System.Windows.Forms.ToolStripMenuItem();
            this.LabelHaslo1 = new System.Windows.Forms.Label();
            this.TextBoxHaslo1 = new System.Windows.Forms.TextBox();
            this.GroupBoxUzytkownik = new System.Windows.Forms.GroupBox();
            this.LabelPoziomUprawnien = new System.Windows.Forms.Label();
            this.ComboBoxPoziomUprawnien = new System.Windows.Forms.ComboBox();
            this.ToolTipFolderPDF = new System.Windows.Forms.ToolTip(this.components);
            this.ToolTipFolderXLSX = new System.Windows.Forms.ToolTip(this.components);
            this.GroupBoxUzytkownicy = new System.Windows.Forms.GroupBox();
            this.ContextMenuStripUzytkownicy.SuspendLayout();
            this.GroupBoxUzytkownik.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextBoxUzytkownik
            // 
            this.TextBoxUzytkownik.Location = new System.Drawing.Point(160, 34);
            this.TextBoxUzytkownik.Name = "TextBoxUzytkownik";
            this.TextBoxUzytkownik.Size = new System.Drawing.Size(126, 20);
            this.TextBoxUzytkownik.TabIndex = 0;
            // 
            // TextBoxHaslo2
            // 
            this.TextBoxHaslo2.Location = new System.Drawing.Point(160, 86);
            this.TextBoxHaslo2.Name = "TextBoxHaslo2";
            this.TextBoxHaslo2.Size = new System.Drawing.Size(126, 20);
            this.TextBoxHaslo2.TabIndex = 2;
            this.TextBoxHaslo2.UseSystemPasswordChar = true;
            // 
            // TextBoxSmtpLogin
            // 
            this.TextBoxSmtpLogin.Location = new System.Drawing.Point(160, 140);
            this.TextBoxSmtpLogin.Name = "TextBoxSmtpLogin";
            this.TextBoxSmtpLogin.Size = new System.Drawing.Size(236, 20);
            this.TextBoxSmtpLogin.TabIndex = 4;
            // 
            // TextBoxMailTo
            // 
            this.TextBoxMailTo.Location = new System.Drawing.Point(160, 192);
            this.TextBoxMailTo.Name = "TextBoxMailTo";
            this.TextBoxMailTo.Size = new System.Drawing.Size(236, 20);
            this.TextBoxMailTo.TabIndex = 6;
            // 
            // TextBoxMailFrom
            // 
            this.TextBoxMailFrom.Location = new System.Drawing.Point(160, 166);
            this.TextBoxMailFrom.Name = "TextBoxMailFrom";
            this.TextBoxMailFrom.Size = new System.Drawing.Size(236, 20);
            this.TextBoxMailFrom.TabIndex = 5;
            // 
            // LabelUzytkownik
            // 
            this.LabelUzytkownik.AutoSize = true;
            this.LabelUzytkownik.Location = new System.Drawing.Point(18, 37);
            this.LabelUzytkownik.Name = "LabelUzytkownik";
            this.LabelUzytkownik.Size = new System.Drawing.Size(62, 13);
            this.LabelUzytkownik.TabIndex = 100;
            this.LabelUzytkownik.Text = "Użytkownik";
            // 
            // LabelSmtpHost
            // 
            this.LabelSmtpHost.AutoSize = true;
            this.LabelSmtpHost.Location = new System.Drawing.Point(6, 100);
            this.LabelSmtpHost.Name = "LabelSmtpHost";
            this.LabelSmtpHost.Size = new System.Drawing.Size(73, 13);
            this.LabelSmtpHost.TabIndex = 103;
            this.LabelSmtpHost.Text = "Serwer SMTP";
            // 
            // LabelHaslo2
            // 
            this.LabelHaslo2.AutoSize = true;
            this.LabelHaslo2.Location = new System.Drawing.Point(18, 89);
            this.LabelHaslo2.Name = "LabelHaslo2";
            this.LabelHaslo2.Size = new System.Drawing.Size(98, 13);
            this.LabelHaslo2.TabIndex = 102;
            this.LabelHaslo2.Text = "Hasło (weryfikacja)";
            // 
            // LabelSmtpLogin
            // 
            this.LabelSmtpLogin.AutoSize = true;
            this.LabelSmtpLogin.Location = new System.Drawing.Point(18, 143);
            this.LabelSmtpLogin.Name = "LabelSmtpLogin";
            this.LabelSmtpLogin.Size = new System.Drawing.Size(135, 13);
            this.LabelSmtpLogin.TabIndex = 104;
            this.LabelSmtpLogin.Text = "Nazwa użytkownika SMTP";
            // 
            // LabelMailFrom
            // 
            this.LabelMailFrom.AutoSize = true;
            this.LabelMailFrom.Location = new System.Drawing.Point(18, 169);
            this.LabelMailFrom.Name = "LabelMailFrom";
            this.LabelMailFrom.Size = new System.Drawing.Size(53, 13);
            this.LabelMailFrom.TabIndex = 105;
            this.LabelMailFrom.Text = "Nadawca";
            // 
            // LabelMailTo
            // 
            this.LabelMailTo.AutoSize = true;
            this.LabelMailTo.Location = new System.Drawing.Point(18, 195);
            this.LabelMailTo.Name = "LabelMailTo";
            this.LabelMailTo.Size = new System.Drawing.Size(43, 13);
            this.LabelMailTo.TabIndex = 106;
            this.LabelMailTo.Text = "Adresat";
            // 
            // LabelSubject
            // 
            this.LabelSubject.AutoSize = true;
            this.LabelSubject.Location = new System.Drawing.Point(18, 224);
            this.LabelSubject.Name = "LabelSubject";
            this.LabelSubject.Size = new System.Drawing.Size(95, 13);
            this.LabelSubject.TabIndex = 107;
            this.LabelSubject.Text = "Temat wiadomości";
            // 
            // TextBoxSubject
            // 
            this.TextBoxSubject.Location = new System.Drawing.Point(160, 221);
            this.TextBoxSubject.Name = "TextBoxSubject";
            this.TextBoxSubject.Size = new System.Drawing.Size(236, 20);
            this.TextBoxSubject.TabIndex = 7;
            // 
            // LabelBody
            // 
            this.LabelBody.AutoSize = true;
            this.LabelBody.Location = new System.Drawing.Point(18, 250);
            this.LabelBody.Name = "LabelBody";
            this.LabelBody.Size = new System.Drawing.Size(92, 13);
            this.LabelBody.TabIndex = 108;
            this.LabelBody.Text = "Treść wiadomości";
            // 
            // TextBoxBody
            // 
            this.TextBoxBody.Location = new System.Drawing.Point(160, 247);
            this.TextBoxBody.Name = "TextBoxBody";
            this.TextBoxBody.Size = new System.Drawing.Size(236, 20);
            this.TextBoxBody.TabIndex = 8;
            // 
            // LabelMinimizeToTray
            // 
            this.LabelMinimizeToTray.AutoSize = true;
            this.LabelMinimizeToTray.Location = new System.Drawing.Point(18, 277);
            this.LabelMinimizeToTray.Name = "LabelMinimizeToTray";
            this.LabelMinimizeToTray.Size = new System.Drawing.Size(135, 13);
            this.LabelMinimizeToTray.TabIndex = 109;
            this.LabelMinimizeToTray.Text = "Minimalizacja do zasobnika";
            // 
            // LabelFolderXLSX
            // 
            this.LabelFolderXLSX.AutoSize = true;
            this.LabelFolderXLSX.Location = new System.Drawing.Point(19, 329);
            this.LabelFolderXLSX.Name = "LabelFolderXLSX";
            this.LabelFolderXLSX.Size = new System.Drawing.Size(110, 13);
            this.LabelFolderXLSX.TabIndex = 111;
            this.LabelFolderXLSX.Text = "Folder (eksport XLSX)";
            // 
            // LabelFolderPDF
            // 
            this.LabelFolderPDF.AutoSize = true;
            this.LabelFolderPDF.Location = new System.Drawing.Point(18, 303);
            this.LabelFolderPDF.Name = "LabelFolderPDF";
            this.LabelFolderPDF.Size = new System.Drawing.Size(96, 13);
            this.LabelFolderPDF.TabIndex = 110;
            this.LabelFolderPDF.Text = "Folder (raport PDF)";
            // 
            // TextBoxFolderXLSX
            // 
            this.TextBoxFolderXLSX.Location = new System.Drawing.Point(160, 326);
            this.TextBoxFolderXLSX.Name = "TextBoxFolderXLSX";
            this.TextBoxFolderXLSX.ReadOnly = true;
            this.TextBoxFolderXLSX.Size = new System.Drawing.Size(236, 20);
            this.TextBoxFolderXLSX.TabIndex = 11;
            // 
            // TextBoxFolderPDF
            // 
            this.TextBoxFolderPDF.Location = new System.Drawing.Point(160, 300);
            this.TextBoxFolderPDF.Name = "TextBoxFolderPDF";
            this.TextBoxFolderPDF.ReadOnly = true;
            this.TextBoxFolderPDF.Size = new System.Drawing.Size(236, 20);
            this.TextBoxFolderPDF.TabIndex = 10;
            // 
            // ButtonDodaj
            // 
            this.ButtonDodaj.Location = new System.Drawing.Point(336, 406);
            this.ButtonDodaj.Name = "ButtonDodaj";
            this.ButtonDodaj.Size = new System.Drawing.Size(75, 23);
            this.ButtonDodaj.TabIndex = 12;
            this.ButtonDodaj.Text = "Dodaj";
            this.ButtonDodaj.UseVisualStyleBackColor = true;
            this.ButtonDodaj.Click += new System.EventHandler(this.ButtonDodaj_Click);
            // 
            // ComboBoxSmtpHost
            // 
            this.ComboBoxSmtpHost.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxSmtpHost.FormattingEnabled = true;
            this.ComboBoxSmtpHost.Location = new System.Drawing.Point(160, 112);
            this.ComboBoxSmtpHost.Name = "ComboBoxSmtpHost";
            this.ComboBoxSmtpHost.Size = new System.Drawing.Size(126, 21);
            this.ComboBoxSmtpHost.TabIndex = 3;
            // 
            // CheckBoxMinimizeToTray
            // 
            this.CheckBoxMinimizeToTray.AutoSize = true;
            this.CheckBoxMinimizeToTray.Location = new System.Drawing.Point(160, 277);
            this.CheckBoxMinimizeToTray.Name = "CheckBoxMinimizeToTray";
            this.CheckBoxMinimizeToTray.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxMinimizeToTray.TabIndex = 9;
            this.CheckBoxMinimizeToTray.UseVisualStyleBackColor = true;
            // 
            // ContextMenuStripUzytkownicy
            // 
            this.ContextMenuStripUzytkownicy.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemUstawFoldery});
            this.ContextMenuStripUzytkownicy.Name = "ContextMenuStripUzytkownicy";
            this.ContextMenuStripUzytkownicy.Size = new System.Drawing.Size(147, 26);
            // 
            // ToolStripMenuItemUstawFoldery
            // 
            this.ToolStripMenuItemUstawFoldery.Name = "ToolStripMenuItemUstawFoldery";
            this.ToolStripMenuItemUstawFoldery.Size = new System.Drawing.Size(146, 22);
            this.ToolStripMenuItemUstawFoldery.Text = "Ustaw foldery";
            this.ToolStripMenuItemUstawFoldery.Click += new System.EventHandler(this.ToolStripMenuItemUstawFoldery_Click);
            // 
            // LabelHaslo1
            // 
            this.LabelHaslo1.AutoSize = true;
            this.LabelHaslo1.Location = new System.Drawing.Point(18, 63);
            this.LabelHaslo1.Name = "LabelHaslo1";
            this.LabelHaslo1.Size = new System.Drawing.Size(36, 13);
            this.LabelHaslo1.TabIndex = 101;
            this.LabelHaslo1.Text = "Hasło";
            // 
            // TextBoxHaslo1
            // 
            this.TextBoxHaslo1.Location = new System.Drawing.Point(160, 60);
            this.TextBoxHaslo1.Name = "TextBoxHaslo1";
            this.TextBoxHaslo1.Size = new System.Drawing.Size(126, 20);
            this.TextBoxHaslo1.TabIndex = 1;
            this.TextBoxHaslo1.UseSystemPasswordChar = true;
            // 
            // GroupBoxUzytkownik
            // 
            this.GroupBoxUzytkownik.ContextMenuStrip = this.ContextMenuStripUzytkownicy;
            this.GroupBoxUzytkownik.Controls.Add(this.LabelPoziomUprawnien);
            this.GroupBoxUzytkownik.Controls.Add(this.ComboBoxPoziomUprawnien);
            this.GroupBoxUzytkownik.Controls.Add(this.LabelSmtpHost);
            this.GroupBoxUzytkownik.Location = new System.Drawing.Point(12, 15);
            this.GroupBoxUzytkownik.Name = "GroupBoxUzytkownik";
            this.GroupBoxUzytkownik.Size = new System.Drawing.Size(399, 372);
            this.GroupBoxUzytkownik.TabIndex = 112;
            this.GroupBoxUzytkownik.TabStop = false;
            this.GroupBoxUzytkownik.Text = "Nowy użytkownik";
            // 
            // LabelPoziomUprawnien
            // 
            this.LabelPoziomUprawnien.AutoSize = true;
            this.LabelPoziomUprawnien.Location = new System.Drawing.Point(6, 340);
            this.LabelPoziomUprawnien.Name = "LabelPoziomUprawnien";
            this.LabelPoziomUprawnien.Size = new System.Drawing.Size(93, 13);
            this.LabelPoziomUprawnien.TabIndex = 114;
            this.LabelPoziomUprawnien.Text = "Poziom uprawnień";
            // 
            // ComboBoxPoziomUprawnien
            // 
            this.ComboBoxPoziomUprawnien.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxPoziomUprawnien.FormattingEnabled = true;
            this.ComboBoxPoziomUprawnien.Location = new System.Drawing.Point(148, 337);
            this.ComboBoxPoziomUprawnien.Name = "ComboBoxPoziomUprawnien";
            this.ComboBoxPoziomUprawnien.Size = new System.Drawing.Size(126, 21);
            this.ComboBoxPoziomUprawnien.TabIndex = 114;
            // 
            // GroupBoxUzytkownicy
            // 
            this.GroupBoxUzytkownicy.Location = new System.Drawing.Point(431, 15);
            this.GroupBoxUzytkownicy.Name = "GroupBoxUzytkownicy";
            this.GroupBoxUzytkownicy.Size = new System.Drawing.Size(471, 348);
            this.GroupBoxUzytkownicy.TabIndex = 113;
            this.GroupBoxUzytkownicy.TabStop = false;
            this.GroupBoxUzytkownicy.Text = "Lista użytkowników";
            // 
            // FormUzytkownicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 441);
            this.Controls.Add(this.GroupBoxUzytkownicy);
            this.Controls.Add(this.TextBoxHaslo1);
            this.Controls.Add(this.CheckBoxMinimizeToTray);
            this.Controls.Add(this.LabelHaslo1);
            this.Controls.Add(this.ButtonDodaj);
            this.Controls.Add(this.LabelHaslo2);
            this.Controls.Add(this.ComboBoxSmtpHost);
            this.Controls.Add(this.TextBoxFolderXLSX);
            this.Controls.Add(this.TextBoxFolderPDF);
            this.Controls.Add(this.LabelFolderPDF);
            this.Controls.Add(this.TextBoxBody);
            this.Controls.Add(this.LabelFolderXLSX);
            this.Controls.Add(this.LabelMinimizeToTray);
            this.Controls.Add(this.TextBoxSubject);
            this.Controls.Add(this.LabelBody);
            this.Controls.Add(this.TextBoxUzytkownik);
            this.Controls.Add(this.LabelSubject);
            this.Controls.Add(this.LabelMailTo);
            this.Controls.Add(this.LabelMailFrom);
            this.Controls.Add(this.LabelSmtpLogin);
            this.Controls.Add(this.LabelUzytkownik);
            this.Controls.Add(this.TextBoxMailFrom);
            this.Controls.Add(this.TextBoxMailTo);
            this.Controls.Add(this.TextBoxSmtpLogin);
            this.Controls.Add(this.TextBoxHaslo2);
            this.Controls.Add(this.GroupBoxUzytkownik);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormUzytkownicy";
            this.Text = "EWO";
            this.Load += new System.EventHandler(this.FormUzytkownicy_Load);
            this.ContextMenuStripUzytkownicy.ResumeLayout(false);
            this.GroupBoxUzytkownik.ResumeLayout(false);
            this.GroupBoxUzytkownik.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxUzytkownik;
        private System.Windows.Forms.TextBox TextBoxHaslo2;
        private System.Windows.Forms.TextBox TextBoxSmtpLogin;
        private System.Windows.Forms.TextBox TextBoxMailTo;
        private System.Windows.Forms.TextBox TextBoxMailFrom;
        private System.Windows.Forms.Label LabelUzytkownik;
        private System.Windows.Forms.Label LabelSmtpHost;
        private System.Windows.Forms.Label LabelHaslo2;
        private System.Windows.Forms.Label LabelSmtpLogin;
        private System.Windows.Forms.Label LabelMailFrom;
        private System.Windows.Forms.Label LabelMailTo;
        private System.Windows.Forms.Label LabelSubject;
        private System.Windows.Forms.TextBox TextBoxSubject;
        private System.Windows.Forms.Label LabelBody;
        private System.Windows.Forms.TextBox TextBoxBody;
        private System.Windows.Forms.Label LabelMinimizeToTray;
        private System.Windows.Forms.Label LabelFolderXLSX;
        private System.Windows.Forms.Label LabelFolderPDF;
        private System.Windows.Forms.TextBox TextBoxFolderXLSX;
        private System.Windows.Forms.TextBox TextBoxFolderPDF;
        private System.Windows.Forms.Button ButtonDodaj;
        private System.Windows.Forms.ComboBox ComboBoxSmtpHost;
        private System.Windows.Forms.CheckBox CheckBoxMinimizeToTray;
        private System.Windows.Forms.ContextMenuStrip ContextMenuStripUzytkownicy;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemUstawFoldery;
        private System.Windows.Forms.Label LabelHaslo1;
        private System.Windows.Forms.TextBox TextBoxHaslo1;
        private System.Windows.Forms.GroupBox GroupBoxUzytkownik;
        private System.Windows.Forms.ToolTip ToolTipFolderPDF;
        private System.Windows.Forms.ToolTip ToolTipFolderXLSX;
        private System.Windows.Forms.GroupBox GroupBoxUzytkownicy;
        private System.Windows.Forms.Label LabelPoziomUprawnien;
        private System.Windows.Forms.ComboBox ComboBoxPoziomUprawnien;

    }
}