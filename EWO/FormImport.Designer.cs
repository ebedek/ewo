﻿namespace EWO
{
    partial class FormImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataGridImport = new System.Windows.Forms.DataGridView();
            this.import_akcept = new System.Windows.Forms.Button();
            this.Nr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dzialanie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.odbiorcaprzelewu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.opisprzelewu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kwota = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.towar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridImport)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridImport
            // 
            this.DataGridImport.AllowDrop = true;
            this.DataGridImport.AllowUserToAddRows = false;
            this.DataGridImport.AllowUserToResizeColumns = false;
            this.DataGridImport.AllowUserToResizeRows = false;
            this.DataGridImport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridImport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nr,
            this.dzialanie,
            this.odbiorcaprzelewu,
            this.opisprzelewu,
            this.kwota,
            this.data,
            this.towar});
            this.DataGridImport.Location = new System.Drawing.Point(12, 12);
            this.DataGridImport.MultiSelect = false;
            this.DataGridImport.Name = "DataGridImport";
            this.DataGridImport.RowHeadersVisible = false;
            this.DataGridImport.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DataGridImport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridImport.Size = new System.Drawing.Size(729, 330);
            this.DataGridImport.TabIndex = 0;
            this.DataGridImport.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridImport_CellMouseDoubleClick);
            // 
            // import_akcept
            // 
            this.import_akcept.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.import_akcept.Location = new System.Drawing.Point(758, 302);
            this.import_akcept.Name = "import_akcept";
            this.import_akcept.Size = new System.Drawing.Size(102, 40);
            this.import_akcept.TabIndex = 1;
            this.import_akcept.Text = "Zatwierdź";
            this.import_akcept.UseVisualStyleBackColor = true;
            this.import_akcept.Click += new System.EventHandler(this.import_akcept_Click);
            // 
            // Nr
            // 
            this.Nr.Frozen = true;
            this.Nr.HeaderText = "Id";
            this.Nr.Name = "Nr";
            this.Nr.Width = 50;
            // 
            // dzialanie
            // 
            this.dzialanie.Frozen = true;
            this.dzialanie.HeaderText = "Działanie";
            this.dzialanie.Name = "dzialanie";
            this.dzialanie.ReadOnly = true;
            this.dzialanie.Width = 75;
            // 
            // odbiorcaprzelewu
            // 
            this.odbiorcaprzelewu.Frozen = true;
            this.odbiorcaprzelewu.HeaderText = "Odbiorca przelewu";
            this.odbiorcaprzelewu.Name = "odbiorcaprzelewu";
            this.odbiorcaprzelewu.ReadOnly = true;
            this.odbiorcaprzelewu.Width = 150;
            // 
            // opisprzelewu
            // 
            this.opisprzelewu.Frozen = true;
            this.opisprzelewu.HeaderText = "Opis przelewu";
            this.opisprzelewu.Name = "opisprzelewu";
            this.opisprzelewu.ReadOnly = true;
            this.opisprzelewu.Width = 200;
            // 
            // kwota
            // 
            this.kwota.Frozen = true;
            this.kwota.HeaderText = "Kwota";
            this.kwota.Name = "kwota";
            this.kwota.ReadOnly = true;
            this.kwota.Width = 75;
            // 
            // data
            // 
            this.data.Frozen = true;
            this.data.HeaderText = "Data";
            this.data.Name = "data";
            this.data.ReadOnly = true;
            this.data.Width = 75;
            // 
            // towar
            // 
            this.towar.Frozen = true;
            this.towar.HeaderText = "Towar";
            this.towar.Name = "towar";
            this.towar.ToolTipText = "Proszę kilknąć 2 razy na rekordzie w celu wybrania wariantu";
            // 
            // FormImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 358);
            this.Controls.Add(this.import_akcept);
            this.Controls.Add(this.DataGridImport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormImport";
            this.Text = "EWO";
            this.Load += new System.EventHandler(this.FormImport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridImport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridImport;
        private System.Windows.Forms.Button import_akcept;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nr;
        private System.Windows.Forms.DataGridViewTextBoxColumn dzialanie;
        private System.Windows.Forms.DataGridViewTextBoxColumn odbiorcaprzelewu;
        private System.Windows.Forms.DataGridViewTextBoxColumn opisprzelewu;
        private System.Windows.Forms.DataGridViewTextBoxColumn kwota;
        private System.Windows.Forms.DataGridViewTextBoxColumn data;
        private System.Windows.Forms.DataGridViewTextBoxColumn towar;

    }
}