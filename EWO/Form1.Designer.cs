﻿namespace EWO
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.TabControlOperacje = new System.Windows.Forms.TabControl();
            this.TabPageOperacjeGlowna = new System.Windows.Forms.TabPage();
            this.ButtonOperacjaDodaj = new System.Windows.Forms.Button();
            this.ButtonOperacjaZmien = new System.Windows.Forms.Button();
            this.ButtonOperacjaSkasuj = new System.Windows.Forms.Button();
            this.MenuStrip = new System.Windows.Forms.MenuStrip();
            this.plikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ParametryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.DrukUstawieniaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DrukPodgladToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DrukToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ExportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RaportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.WyslijToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.ImportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SlownikiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TypyTowarowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SerweryPocztoweToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UzytkownicyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WykresyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.KategorieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RazemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.KategorieRazemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oProgramieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PageSetupDialog = new System.Windows.Forms.PageSetupDialog();
            this.PrintDocument = new System.Drawing.Printing.PrintDocument();
            this.PrintDialog = new System.Windows.Forms.PrintDialog();
            this.PrintPreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.NotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.TextBoxWydatkow = new System.Windows.Forms.TextBox();
            this.LabelWydatkow = new System.Windows.Forms.Label();
            this.LabelPrzychodow = new System.Windows.Forms.Label();
            this.TextBoxPrzychodow = new System.Windows.Forms.TextBox();
            this.LabelSaldo = new System.Windows.Forms.Label();
            this.TextBoxSaldo = new System.Windows.Forms.TextBox();
            this.PodsumowaniaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaldaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StatystykiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TabControlOperacje.SuspendLayout();
            this.MenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabControlOperacje
            // 
            this.TabControlOperacje.AllowDrop = true;
            this.TabControlOperacje.Controls.Add(this.TabPageOperacjeGlowna);
            this.TabControlOperacje.HotTrack = true;
            this.TabControlOperacje.Location = new System.Drawing.Point(12, 34);
            this.TabControlOperacje.Name = "TabControlOperacje";
            this.TabControlOperacje.SelectedIndex = 0;
            this.TabControlOperacje.Size = new System.Drawing.Size(792, 453);
            this.TabControlOperacje.TabIndex = 1;
            this.TabControlOperacje.Selected += new System.Windows.Forms.TabControlEventHandler(this.TabControlOperacje_Selected);
            this.TabControlOperacje.DragDrop += new System.Windows.Forms.DragEventHandler(this.TabControlOperacje_DragDrop);
            this.TabControlOperacje.DragEnter += new System.Windows.Forms.DragEventHandler(this.TabControlOperacje_DragEnter);
            // 
            // TabPageOperacjeGlowna
            // 
            this.TabPageOperacjeGlowna.AllowDrop = true;
            this.TabPageOperacjeGlowna.Location = new System.Drawing.Point(4, 22);
            this.TabPageOperacjeGlowna.Name = "TabPageOperacjeGlowna";
            this.TabPageOperacjeGlowna.Padding = new System.Windows.Forms.Padding(3);
            this.TabPageOperacjeGlowna.Size = new System.Drawing.Size(784, 427);
            this.TabPageOperacjeGlowna.TabIndex = 0;
            this.TabPageOperacjeGlowna.Text = "Główna";
            this.TabPageOperacjeGlowna.UseVisualStyleBackColor = true;
            // 
            // ButtonOperacjaDodaj
            // 
            this.ButtonOperacjaDodaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ButtonOperacjaDodaj.Location = new System.Drawing.Point(841, 80);
            this.ButtonOperacjaDodaj.Name = "ButtonOperacjaDodaj";
            this.ButtonOperacjaDodaj.Size = new System.Drawing.Size(131, 40);
            this.ButtonOperacjaDodaj.TabIndex = 2;
            this.ButtonOperacjaDodaj.Text = "Zarejestruj operację";
            this.ButtonOperacjaDodaj.UseVisualStyleBackColor = true;
            this.ButtonOperacjaDodaj.Click += new System.EventHandler(this.ButtonOperacjaDodaj_Click);
            // 
            // ButtonOperacjaZmien
            // 
            this.ButtonOperacjaZmien.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ButtonOperacjaZmien.Location = new System.Drawing.Point(841, 139);
            this.ButtonOperacjaZmien.Name = "ButtonOperacjaZmien";
            this.ButtonOperacjaZmien.Size = new System.Drawing.Size(131, 40);
            this.ButtonOperacjaZmien.TabIndex = 3;
            this.ButtonOperacjaZmien.Text = "Zmodyfikuj operację";
            this.ButtonOperacjaZmien.UseVisualStyleBackColor = true;
            this.ButtonOperacjaZmien.Click += new System.EventHandler(this.ButtonOperacjaZmien_Click);
            // 
            // ButtonOperacjaSkasuj
            // 
            this.ButtonOperacjaSkasuj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ButtonOperacjaSkasuj.Location = new System.Drawing.Point(841, 197);
            this.ButtonOperacjaSkasuj.Name = "ButtonOperacjaSkasuj";
            this.ButtonOperacjaSkasuj.Size = new System.Drawing.Size(131, 40);
            this.ButtonOperacjaSkasuj.TabIndex = 4;
            this.ButtonOperacjaSkasuj.Text = "Usuń operację";
            this.ButtonOperacjaSkasuj.UseVisualStyleBackColor = true;
            this.ButtonOperacjaSkasuj.Click += new System.EventHandler(this.ButtonOperacjaSkasuj_Click);
            // 
            // MenuStrip
            // 
            this.MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plikToolStripMenuItem,
            this.SlownikiToolStripMenuItem,
            this.WykresyToolStripMenuItem,
            this.PodsumowaniaToolStripMenuItem,
            this.oProgramieToolStripMenuItem});
            this.MenuStrip.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip.Name = "MenuStrip";
            this.MenuStrip.Size = new System.Drawing.Size(1011, 24);
            this.MenuStrip.TabIndex = 5;
            this.MenuStrip.Text = "menuStrip";
            // 
            // plikToolStripMenuItem
            // 
            this.plikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ParametryToolStripMenuItem,
            this.toolStripSeparator2,
            this.DrukUstawieniaToolStripMenuItem,
            this.DrukPodgladToolStripMenuItem,
            this.DrukToolStripMenuItem,
            this.toolStripSeparator1,
            this.ExportToolStripMenuItem,
            this.RaportToolStripMenuItem,
            this.toolStripSeparator3,
            this.WyslijToolStripMenuItem,
            this.toolStripSeparator4,
            this.ImportToolStripMenuItem});
            this.plikToolStripMenuItem.Name = "plikToolStripMenuItem";
            this.plikToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.plikToolStripMenuItem.Text = "Plik";
            // 
            // ParametryToolStripMenuItem
            // 
            this.ParametryToolStripMenuItem.Name = "ParametryToolStripMenuItem";
            this.ParametryToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.ParametryToolStripMenuItem.Text = "Parametry";
            this.ParametryToolStripMenuItem.Click += new System.EventHandler(this.ParametryToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(248, 6);
            // 
            // DrukUstawieniaToolStripMenuItem
            // 
            this.DrukUstawieniaToolStripMenuItem.Name = "DrukUstawieniaToolStripMenuItem";
            this.DrukUstawieniaToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.DrukUstawieniaToolStripMenuItem.Text = "Ustawienia wydruku";
            this.DrukUstawieniaToolStripMenuItem.Click += new System.EventHandler(this.DrukUstawieniaToolStripMenuItem_Click);
            // 
            // DrukPodgladToolStripMenuItem
            // 
            this.DrukPodgladToolStripMenuItem.Name = "DrukPodgladToolStripMenuItem";
            this.DrukPodgladToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.DrukPodgladToolStripMenuItem.Text = "Podgląd wydruku";
            this.DrukPodgladToolStripMenuItem.Click += new System.EventHandler(this.DrukPodgladToolStripMenuItem_Click);
            // 
            // DrukToolStripMenuItem
            // 
            this.DrukToolStripMenuItem.Name = "DrukToolStripMenuItem";
            this.DrukToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.DrukToolStripMenuItem.Text = "Drukuj dane z bieżącego miesiąca";
            this.DrukToolStripMenuItem.Click += new System.EventHandler(this.DrukToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(248, 6);
            // 
            // ExportToolStripMenuItem
            // 
            this.ExportToolStripMenuItem.Name = "ExportToolStripMenuItem";
            this.ExportToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.ExportToolStripMenuItem.Text = "Export do formatu XLSX";
            this.ExportToolStripMenuItem.Click += new System.EventHandler(this.ExportToolStripMenuItem_Click);
            // 
            // RaportToolStripMenuItem
            // 
            this.RaportToolStripMenuItem.Name = "RaportToolStripMenuItem";
            this.RaportToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.RaportToolStripMenuItem.Text = "Raport w formacie PDF";
            this.RaportToolStripMenuItem.Click += new System.EventHandler(this.RaportToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(248, 6);
            // 
            // WyslijToolStripMenuItem
            // 
            this.WyslijToolStripMenuItem.Name = "WyslijToolStripMenuItem";
            this.WyslijToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.WyslijToolStripMenuItem.Text = "Wyślij pliki pocztą";
            this.WyslijToolStripMenuItem.Click += new System.EventHandler(this.WyslijToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(248, 6);
            // 
            // ImportToolStripMenuItem
            // 
            this.ImportToolStripMenuItem.Name = "ImportToolStripMenuItem";
            this.ImportToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.ImportToolStripMenuItem.Text = "Import danych z pliku CSV";
            this.ImportToolStripMenuItem.Click += new System.EventHandler(this.ImportToolStripMenuItem_Click);
            // 
            // SlownikiToolStripMenuItem
            // 
            this.SlownikiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TypyTowarowToolStripMenuItem,
            this.SerweryPocztoweToolStripMenuItem,
            this.UzytkownicyToolStripMenuItem});
            this.SlownikiToolStripMenuItem.Name = "SlownikiToolStripMenuItem";
            this.SlownikiToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.SlownikiToolStripMenuItem.Text = "Słowniki";
            // 
            // TypyTowarowToolStripMenuItem
            // 
            this.TypyTowarowToolStripMenuItem.Name = "TypyTowarowToolStripMenuItem";
            this.TypyTowarowToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.TypyTowarowToolStripMenuItem.Text = "Typy towarów";
            this.TypyTowarowToolStripMenuItem.Click += new System.EventHandler(this.TypyTowarowToolStripMenuItem_Click);
            // 
            // SerweryPocztoweToolStripMenuItem
            // 
            this.SerweryPocztoweToolStripMenuItem.Name = "SerweryPocztoweToolStripMenuItem";
            this.SerweryPocztoweToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.SerweryPocztoweToolStripMenuItem.Text = "Serwery pocztowe";
            this.SerweryPocztoweToolStripMenuItem.Click += new System.EventHandler(this.SerweryPocztoweToolStripMenuItem_Click);
            // 
            // UzytkownicyToolStripMenuItem
            // 
            this.UzytkownicyToolStripMenuItem.Name = "UzytkownicyToolStripMenuItem";
            this.UzytkownicyToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.UzytkownicyToolStripMenuItem.Text = "Użytkownicy";
            this.UzytkownicyToolStripMenuItem.Click += new System.EventHandler(this.UzytkownicyToolStripMenuItem_Click);
            // 
            // WykresyToolStripMenuItem
            // 
            this.WykresyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.KategorieToolStripMenuItem,
            this.RazemToolStripMenuItem,
            this.KategorieRazemToolStripMenuItem});
            this.WykresyToolStripMenuItem.Name = "WykresyToolStripMenuItem";
            this.WykresyToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.WykresyToolStripMenuItem.Text = "Wykresy";
            // 
            // KategorieToolStripMenuItem
            // 
            this.KategorieToolStripMenuItem.Name = "KategorieToolStripMenuItem";
            this.KategorieToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.KategorieToolStripMenuItem.Text = "Suma kwot częściowych / miesiące";
            this.KategorieToolStripMenuItem.Click += new System.EventHandler(this.KategorieToolStripMenuItem_Click);
            // 
            // RazemToolStripMenuItem
            // 
            this.RazemToolStripMenuItem.Name = "RazemToolStripMenuItem";
            this.RazemToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.RazemToolStripMenuItem.Text = "Suma kwot / miesiące";
            this.RazemToolStripMenuItem.Click += new System.EventHandler(this.RazemToolStripMenuItem_Click);
            // 
            // KategorieRazemToolStripMenuItem
            // 
            this.KategorieRazemToolStripMenuItem.Name = "KategorieRazemToolStripMenuItem";
            this.KategorieRazemToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.KategorieRazemToolStripMenuItem.Text = "Suma kwot / razem";
            this.KategorieRazemToolStripMenuItem.Click += new System.EventHandler(this.KategorieRazemToolStripMenuItem_Click);
            // 
            // oProgramieToolStripMenuItem
            // 
            this.oProgramieToolStripMenuItem.Name = "oProgramieToolStripMenuItem";
            this.oProgramieToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.oProgramieToolStripMenuItem.Text = "O programie...";
            this.oProgramieToolStripMenuItem.Click += new System.EventHandler(this.oProgramieToolStripMenuItem_Click);
            // 
            // PageSetupDialog
            // 
            this.PageSetupDialog.Document = this.PrintDocument;
            // 
            // PrintDocument
            // 
            this.PrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument_PrintPage);
            // 
            // PrintDialog
            // 
            this.PrintDialog.UseEXDialog = true;
            // 
            // PrintPreviewDialog
            // 
            this.PrintPreviewDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.PrintPreviewDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.PrintPreviewDialog.ClientSize = new System.Drawing.Size(400, 300);
            this.PrintPreviewDialog.Document = this.PrintDocument;
            this.PrintPreviewDialog.Enabled = true;
            this.PrintPreviewDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("PrintPreviewDialog.Icon")));
            this.PrintPreviewDialog.Name = "PrintPreviewDialog";
            this.PrintPreviewDialog.Visible = false;
            // 
            // NotifyIcon
            // 
            this.NotifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("NotifyIcon.Icon")));
            this.NotifyIcon.Text = "EWO";
            this.NotifyIcon.Click += new System.EventHandler(this.NotifyIcon_Click);
            // 
            // TextBoxWydatkow
            // 
            this.TextBoxWydatkow.BackColor = System.Drawing.SystemColors.Control;
            this.TextBoxWydatkow.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxWydatkow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TextBoxWydatkow.ForeColor = System.Drawing.Color.Red;
            this.TextBoxWydatkow.Location = new System.Drawing.Point(841, 358);
            this.TextBoxWydatkow.Name = "TextBoxWydatkow";
            this.TextBoxWydatkow.ReadOnly = true;
            this.TextBoxWydatkow.Size = new System.Drawing.Size(131, 15);
            this.TextBoxWydatkow.TabIndex = 6;
            this.TextBoxWydatkow.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LabelWydatkow
            // 
            this.LabelWydatkow.AutoSize = true;
            this.LabelWydatkow.Location = new System.Drawing.Point(838, 342);
            this.LabelWydatkow.Name = "LabelWydatkow";
            this.LabelWydatkow.Size = new System.Drawing.Size(85, 13);
            this.LabelWydatkow.TabIndex = 7;
            this.LabelWydatkow.Text = "Suma wydatków";
            // 
            // LabelPrzychodow
            // 
            this.LabelPrzychodow.AutoSize = true;
            this.LabelPrzychodow.Location = new System.Drawing.Point(838, 279);
            this.LabelPrzychodow.Name = "LabelPrzychodow";
            this.LabelPrzychodow.Size = new System.Drawing.Size(94, 13);
            this.LabelPrzychodow.TabIndex = 8;
            this.LabelPrzychodow.Text = "Suma przychodów";
            // 
            // TextBoxPrzychodow
            // 
            this.TextBoxPrzychodow.BackColor = System.Drawing.SystemColors.Control;
            this.TextBoxPrzychodow.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxPrzychodow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TextBoxPrzychodow.ForeColor = System.Drawing.Color.Green;
            this.TextBoxPrzychodow.Location = new System.Drawing.Point(841, 295);
            this.TextBoxPrzychodow.Name = "TextBoxPrzychodow";
            this.TextBoxPrzychodow.ReadOnly = true;
            this.TextBoxPrzychodow.Size = new System.Drawing.Size(131, 15);
            this.TextBoxPrzychodow.TabIndex = 9;
            this.TextBoxPrzychodow.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LabelSaldo
            // 
            this.LabelSaldo.AutoSize = true;
            this.LabelSaldo.Location = new System.Drawing.Point(838, 402);
            this.LabelSaldo.Name = "LabelSaldo";
            this.LabelSaldo.Size = new System.Drawing.Size(34, 13);
            this.LabelSaldo.TabIndex = 10;
            this.LabelSaldo.Text = "Saldo";
            // 
            // TextBoxSaldo
            // 
            this.TextBoxSaldo.BackColor = System.Drawing.SystemColors.Control;
            this.TextBoxSaldo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxSaldo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TextBoxSaldo.ForeColor = System.Drawing.Color.Blue;
            this.TextBoxSaldo.Location = new System.Drawing.Point(841, 418);
            this.TextBoxSaldo.Name = "TextBoxSaldo";
            this.TextBoxSaldo.ReadOnly = true;
            this.TextBoxSaldo.Size = new System.Drawing.Size(131, 15);
            this.TextBoxSaldo.TabIndex = 11;
            this.TextBoxSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // PodsumowaniaToolStripMenuItem
            // 
            this.PodsumowaniaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaldaToolStripMenuItem,
            this.StatystykiToolStripMenuItem});
            this.PodsumowaniaToolStripMenuItem.Name = "PodsumowaniaToolStripMenuItem";
            this.PodsumowaniaToolStripMenuItem.Size = new System.Drawing.Size(101, 20);
            this.PodsumowaniaToolStripMenuItem.Text = "Podsumowania";
            // 
            // SaldaToolStripMenuItem
            // 
            this.SaldaToolStripMenuItem.Name = "SaldaToolStripMenuItem";
            this.SaldaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.SaldaToolStripMenuItem.Text = "Salda";
            this.SaldaToolStripMenuItem.Click += new System.EventHandler(this.SaldaToolStripMenuItem_Click);
            // 
            // StatystykiToolStripMenuItem
            // 
            this.StatystykiToolStripMenuItem.Name = "StatystykiToolStripMenuItem";
            this.StatystykiToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.StatystykiToolStripMenuItem.Text = "Statystyki";
            this.StatystykiToolStripMenuItem.Click += new System.EventHandler(this.StatystykiToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 509);
            this.Controls.Add(this.TextBoxSaldo);
            this.Controls.Add(this.LabelSaldo);
            this.Controls.Add(this.TextBoxPrzychodow);
            this.Controls.Add(this.LabelPrzychodow);
            this.Controls.Add(this.LabelWydatkow);
            this.Controls.Add(this.TextBoxWydatkow);
            this.Controls.Add(this.ButtonOperacjaSkasuj);
            this.Controls.Add(this.ButtonOperacjaZmien);
            this.Controls.Add(this.ButtonOperacjaDodaj);
            this.Controls.Add(this.TabControlOperacje);
            this.Controls.Add(this.MenuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MenuStrip;
            this.MinimumSize = new System.Drawing.Size(890, 537);
            this.Name = "Form1";
            this.Text = "EWO";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.TabControlOperacje.ResumeLayout(false);
            this.MenuStrip.ResumeLayout(false);
            this.MenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl TabControlOperacje;
        private System.Windows.Forms.Button ButtonOperacjaDodaj;
        private System.Windows.Forms.Button ButtonOperacjaZmien;
        private System.Windows.Forms.Button ButtonOperacjaSkasuj;
        private System.Windows.Forms.MenuStrip MenuStrip;
        private System.Windows.Forms.ToolStripMenuItem SlownikiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TypyTowarowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oProgramieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DrukUstawieniaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DrukPodgladToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DrukToolStripMenuItem;
        private System.Windows.Forms.PageSetupDialog PageSetupDialog;
        private System.Windows.Forms.PrintDialog PrintDialog;
        private System.Drawing.Printing.PrintDocument PrintDocument;
        private System.Windows.Forms.PrintPreviewDialog PrintPreviewDialog;
        private System.Windows.Forms.ToolStripMenuItem WykresyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem KategorieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RazemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem KategorieRazemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RaportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ParametryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem WyslijToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.NotifyIcon NotifyIcon;
        private System.Windows.Forms.TextBox TextBoxWydatkow;
        private System.Windows.Forms.Label LabelWydatkow;
        private System.Windows.Forms.Label LabelPrzychodow;
        private System.Windows.Forms.TextBox TextBoxPrzychodow;
        private System.Windows.Forms.Label LabelSaldo;
        private System.Windows.Forms.TextBox TextBoxSaldo;
        private System.Windows.Forms.ToolStripMenuItem ImportToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.TabPage TabPageOperacjeGlowna;
        private System.Windows.Forms.ToolStripMenuItem SerweryPocztoweToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem UzytkownicyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PodsumowaniaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaldaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem StatystykiToolStripMenuItem;

    }
}

