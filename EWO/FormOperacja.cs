﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace EWO
{
    public partial class FormOperacja : Form
    {
        bool OperacjaEdycja = false;
        int OperacjaWybor;
        string TypDzialaniaWybor;
        int TowarWybor;

        public FormOperacja()
        {
            InitializeComponent();

            for (int i = 0; i < EWO.TypyDzialanIlosc(); i++)
                ComboBoxOperacjaDzialanie.Items.Add(EWO.DzialanieOdczyt(i));

            ComboBoxOperacjaDzialanie.Text = EWO.DzialanieOdczyt(0);

            if (EWO.AktualnyRokMsc == EWO.WybranyRokMsc)
                DatePickerOperacjaData.Text = DateTime.Today.ToString();
            else
                DatePickerOperacjaData.Text = EWO.WybranyRokMsc+"-01";

            TypDzialaniaWybor = ComboBoxOperacjaDzialanie.Text;
        }

        public FormOperacja(int id, string dzialanie, string towar, double kwota, string wariant,string data)
        {
            InitializeComponent();

            for (int i = 0; i < EWO.TypyDzialanIlosc(); i++)
                ComboBoxOperacjaDzialanie.Items.Add(EWO.DzialanieOdczyt(i));

            OperacjaWybor = id;
            ComboBoxOperacjaDzialanie.Text = dzialanie;
            TextBoxOperacjaTyp.Text = wariant;
            TextBoxOperacjaKwota.Text = OperacjaT.KwotaSprawdz(kwota.ToString());
            ComboBoxOperacjaTowar.Text = towar;
            DatePickerOperacjaData.Text = data;
            OperacjaEdycja = true;
            GroupBoxOperacja.Text = "Modyfikacja operacji";
            ButtonOperacja.Text = "Zmodyfikuj operację";
        }

        private void FormOperacja_Load(object sender, EventArgs e)
        {
            this.Icon = EWO.Ikonka;
        }

        private void ButtonOperacja_Click(object sender, EventArgs e)
        {

            if (TextBoxOperacjaTyp.Text != "" && TextBoxOperacjaKwota.Text != "" && ComboBoxOperacjaTowar.Text != "" && DatePickerOperacjaData.Text != "")
            {
                if (double.Parse(TextBoxOperacjaKwota.Text.ToString()) > 0)
                {
                    if (OperacjaEdycja == false)
                    {
                        try
                        {
                            EWO.OperacjaDodaj(ComboBoxOperacjaDzialanie.Text, double.Parse(TextBoxOperacjaKwota.Text), ComboBoxOperacjaTowar.Text, DatePickerOperacjaData.Text);
                            MessageBox.Show("Operacja zarejestrowana...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }

                        catch
                        {
                            MessageBox.Show("Błąd w czasie rejestrowania operacji!", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        try
                        {
                            EWO.OperacjaZmien(EWO.OperacjaOdczyt(OperacjaWybor).Id, ComboBoxOperacjaDzialanie.Text, double.Parse(TextBoxOperacjaKwota.Text), ComboBoxOperacjaTowar.Text, DatePickerOperacjaData.Text, OperacjaWybor);
                            MessageBox.Show("Operacja zmodyfikowana...", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }

                        catch
                        {
                            MessageBox.Show("Błąd w czasie modyfikowania operacji!", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                }

                else
                    MessageBox.Show("Nieprawidłowa kwota < 0!", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else
                MessageBox.Show("Proszę wypełnić wszystkie pola!", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ComboBoxOperacjaDzialanie_SelectedIndexChanged(object sender, EventArgs e)
        {

            ComboBoxOperacjaTowar.Items.Clear();

            TypyDzialanT wczytaj = new TypyDzialanT(ComboBoxOperacjaDzialanie.Text);

            EWO.TowaryWczytaj(wczytaj);

            for (int i = 0; i < EWO.TowaryIlosc(); i++)
            {
                ComboBoxOperacjaTowar.Items.Add(EWO.TowarOdczyt(i).Towar);
            }

            ComboBoxOperacjaTowar.Text = EWO.TowarOdczyt(0).Towar;
            TextBoxOperacjaTyp.Text = EWO.TowarOdczyt(0).TypTowaru;

        }

        private void TextBoxOperacjaKwota_Leave(object sender, EventArgs e)
        {
            try
            {
                TextBoxOperacjaKwota.Text = OperacjaT.KwotaSprawdz(TextBoxOperacjaKwota.Text);
                TextBoxOperacjaKwota.BackColor = Color.White;

                if (double.Parse(TextBoxOperacjaKwota.Text.ToString()) < 0)
                {
                    MessageBox.Show("Nieprawidłowa kwota < 0!", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    TextBoxOperacjaKwota.BackColor = Color.Red;
                }
            }
            catch
            {
                MessageBox.Show("Nieprawidłowa kwota!", "EWO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                TextBoxOperacjaKwota.BackColor = Color.Red;
            }


        }

        private void ComboBoxOperacjaTowar_SelectedIndexChanged(object sender, EventArgs e)
        {
            TowarWybor = ComboBoxOperacjaTowar.SelectedIndex;
            TextBoxOperacjaTyp.Text = EWO.TowarOdczyt(TowarWybor).TypTowaru;
        }
    }
}
